# ICA-GEN Website Project

This website is built by [Jekyll](https://jekyllrb.com) on the destination server at [generalisation.icaci.org](http://generalisation.icaci.org)

The repository is being pulled to the server through a [webhook](https://gitlab.vgiscience.de/ica-gen/ica-gen/settings/integrations) on push events. The build is then triggered through the `post-merge` hook in the destination `.git` directory:

For questions please refer to @ml