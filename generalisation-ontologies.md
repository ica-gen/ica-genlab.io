---
  title: Generalisation Ontologies
  layout: page
  order: 10
---

The ontology called "GeneProcessOnto" is an attempt to collaboratively build an ontology for the definition of a shared vocabulary regarding generalisation processes, operations and algorithms. It is not exhaustive at all, and some modelling choices are still to be debated. The ontology is available on the web Protégé server (<http://webprotege.stanford.edu>) but you have to be registered to access. When signed up, please send your login to Guillaume Touya (firstname.lastname[at]ign.fr) requiring an access to view the ontology, and you will be able to view it in the list of the available ontologies, and then browse it or download it. If you wish to be part of the collaborative edition of the ontology, you can be added as an editor.

![](/images/images/Events/GenOntoGroup2015part.jpg){: .right}




Platforms
---------

*   Web: <http://webprotege.stanford.edu>  
    ("Sign In" and search for "GeneProcessOnto")
*   Login request: Guillaume Touya (firstname.lastname[at]ign.fr)
*   [Workshop summary](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/ThemWorkshop/ThematicOntologyOnDemand_Paris2015.pdf)



Further reading
---------------

Gould, N. and Mackaness, W. (2015). [From taxonomies to ontologies: formalizing generalization knowledge for on-demand mapping.](http://www.tandfonline.com/doi/full/10.1080/15230406.2015.1072737) Cartography and Geographic Information Science.
