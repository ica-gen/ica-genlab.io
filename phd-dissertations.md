---
  title: PhD Dissertations
  layout: page
  order: 12
---



PhD dissertations
=================

*(page under construction)*

This page aims to welcome any PhD dissertation related to the field of the commission.

If you wish to see your dissertation here, please send an Email to Guillaume Touya (firstname.lastname(at)ign.fr) with the following information:

-   Title of the dissertation (+ title in English if necessary)
-   Name of the author
-   University (PhD's affiliation)
-   Year
-   Language of the dissertation
-   A set of key words
-   A link to download it (pdf preferred, please no big document by email).

You may also send us the dissertation of one colleague/student, but please be sure they agree with it and there are no privacy/copyright issue.

--------------------------------------------------------------------------------

2023
----

**Exploring the potential of deep learning for map generalization** [![pdf](images/icons/pdf.jpg)](https://theses.hal.science/tel-04089883v1/document)

| **Author**   | Azelle Courtial, IGN, Université Gustave Eiffel, France |
| **Language** | English |
| **Keywords** | deep learning, generative adversarial network, map generalisation, machine learning, mountain roads |



2018
----

**Généralisation de représentations intermédiaires dans une carte topographique multi-échelle pour faciliter la navigation de l'utilisateur** [![pdf](images/icons/pdf.jpg)](https://tel.archives-ouvertes.fr/tel-01935783/document)

| **Author**   | Marion Dumont, IGN, Université Paris Est, France |
| **Language** | French |
| **Keywords** | Multi-scale maps, smooth zooming, map generalisation, abstraction, user survey |


2016
----

**Interactions entre niveaux dans un modèle orienté agent de généralisation cartographique : Le modèle DIOGEN** [![pdf](images/icons/pdf.jpg)](https://tel.archives-ouvertes.fr/tel-01539595/document)

| **Author**   | Adrien Maudet, IGN, Université Paris Est, France |
| **Language** | French |
| **Keywords** | map generalisation, multi-agent systems, multi-level, artificial intelligence, spatial relation |


2014
----

**Quadtree-based Real-time Point Generalisation for Web and Mobile Mapping** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/Bereuter-2014-PhD-Thesis.pdf)

| **Author**   | Pia Bereuter, University of Zurich, Switzerland |
| **Language** | English |
| **Keywords** | map generalisation, web mapping, point generalisation, real time |


2011
----

**A solution to the problem of the generalization of the Italian geographical databases from large to medium scale: approach definition, process design and operators implementation** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/publications/savino.pdf)

| **Author**   | Sandro Savino, University of Padova, Italy |
| **Language** | English |
| **Keywords** | Model generalization, medium large scales, data enrichment, selection, typification, simplification |


2010
----

**Web-based architecture for on-demand maps: integrating meaningful generalization processing** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/foerster.pdf)


| **Author** | Theodor Foerster, ITC, Faculty of University of Twente, Netherlands
| **Language** | English |
| **Keywords** | Generalization operators, formalization, base maps, on-demand, user profiles, Web Processing Service, Generalization-enabled WMS, Clarity |


2008
----

**Aggregation in Map Generalization by Combinatorial Optimization** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/HaunertDissertation.pdf)


| **Author** | Jan-Henrik Haunert, University of Hannover, Germany
| **Language** | English |
| **Keywords** | area aggregation, land use, optimisation, mixed-integer programming |


2007
----

**MODELLING GEOGRAPHIC PHENOMENA AT MULTIPLE LEVELS OF DETAIL: A MODEL GENERALISATION APPROACH BASED ON AGGREGATION** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/Chaudhry_Thesis_07.pdf)


| **Author** | Omair Z Chaudhry, University of Edinburgh, United Kingdom
| **Language** | English |
| **Keywords** | partonomy, spatial relation, data enrichment, city, forest, mountain range |


**Data Enrichment for Adaptive Map Generalization Using Web Services** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/phd_moritz_neun_2007.pdf)


| **Author** | Moritz Neun, University of Zurich, Switzerland
| **Language** | English |
| **Keywords** | Generalization operators, Web Processing Service, WebGen, orchestration |


**Enabling Pattern-Aware Automated Map Generalization** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/thesis_stefan_steiniger.pdf)


| **Author** | Stefan Steiniger, University of Zurich, Switzerland
| **Language** | English |
| **Keywords** | spatial relations, patterns, data enrichment, map generalisation |


2006
----

**A MULTI-AGENT SYSTEM FOR ON-THE-FLY WEB MAP GENERATION AND SPATIAL CONFLICT RESOLUTION** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/these_jabeur.pdf)


| **Author** | Nafaâ Jabeur, Laval University, Canada
| **Language** | English |
| **Keywords** | map generalisation, multi-agent systems, web mapping, on-the-fly |


2001
----

**Energy Minimization Methods for Feature Displacement in Map Generalization** [![pdf](images/icons/pdf.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/phd/bader_phd.pdf)


| **Author** | Matthias Bader, University of Zurich, Switzerland
| **Language** | English |
| **Keywords** | map generalisation, elastic beams, snakes, displacement, roads |
