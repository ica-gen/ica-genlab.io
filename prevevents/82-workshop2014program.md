---
  layout: page
---

17th ICA Workshop - Program
---------------------------

### Tuesday, 23. September

(download archive with all submissions [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/submissions2014.zip))

08:30 – 09:00 Registration

09:00 – 09:15 Welcome (and fast forward session)

09:15 – 10:45 Session 1: Generalisation of User Generated & Multi-Source Spatial Content

William Mackaness and Nick Gould ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/GeoginAutomatedCartography17thICA-21Sept2014v2.pdf))
[*The Role of Geography in Automated Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_2.pdf)

Cecile Duchêne ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/2014_ICAGeneWorkshop_duchene.pdf))
[*Making a map from “thematically multi-sourced data”: the potential of making inter-layers spatial relations explicit*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_14.pdf)

Nick Gould and William Mackaness ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/nickgould_vienna_2014.pdf))
[*Collaboration on an Ontology for Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_1.pdf)

Ehsan Abdolmajidi, Julian Will, Lars Harrie and Ali Mansourian ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/ICAPresentation_-_20140917.pdf))
[*Comparison of matching methods of user generated and authoritative geographic data*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_4.pdf)

Guillaume Touya and Jean-François Girres ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/touya_workshop_2014.pdf))*
[Generalising Unusual Map Themes from OpenStreetMap](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_6.pdf)*

10:45 – 11:15 Coffee break

11:15 – 12:30 Session 2: Generalisation at NMAs for NMAs

Nicolas Regnauld
[*1Generalise: 1Spatial’s new automatic generalisation platform*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_10.pdf)

Blanca Baella, Dan Lee, Anna Lleopart and Maria Pla ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/ICGC_Esri_Vienna_20140923.pdf))
*[ICGC MRDB for topographic data: first steps in the implementation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_8.pdf) *

Jantien Stoter, Vincent van Altena, Jan Bakermans, Peter Lentjes, Ron Nijhuis, Marc Post and Marcel Reuvers ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/20140923_ICA2014Wenen.pdf))
*[Generalisation of a 1:10k map from municipal data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_9.pdf) *

Sheng Zhou
[*Towards a Multi-Representation Database Approach to On-Demand Mapping and Continuous Zooming*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_5.pdf)

12:30 – 14:00 Lunch

14:00 – 14:45 Break-out session

14:45 – 15:15 Reports from break-out session ([Generalisation Ontology](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/BreakoutOntologyandGeographyMapping.pdf)) ([Thematic Mapping](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/notes-BreakOut.pdf)) ([Conflation & Matching](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Conflation__Matching.pdf))

15:15 – 15:45 Coffee break

15:45 – 17:00 Session 3: Vector generalisation algorithms & raster data generalisation

Radan Suba, Martijn Meijers, Lina Huang and Peter van Oosterom ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Radan_Suba.pdf)) ([demo](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Radan_Suba_demo.wmv))
*[Continuous Road Network Generalization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_7.pdf) *

Sandro Savino ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Savino_-_2014_-_Vienna_-_ICA.pdf))
[*Generalization of braided streams*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_11.pdf)

Andreas Reimer and Christian Kempf ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Reimer_Kempf.pdf))
*[Efficient derivation and caricature of urban settlement boundaries for 1:250k](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_12.pdf) *

Paulo Raposo and Timofey Samsonov* *([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/slides/Towards_General_Theory_ofRaster_Generalization_full.pdf))*
*[*Towards general theory of raster data generalization*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2014/genemr2014_submission_13.pdf)

17:00 – 17:30 Wrap up, next steps
