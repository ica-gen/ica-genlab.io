---
  layout: page
---


1st workshop on CartoAI: AI for cartography
--------------------------------------------------------------

Leeds, United Kingdom (on site and online)

**September 12, 2023 (9:00 - 12:30)**



Following the recent evolutions of cartography, and a successful workshop in 2022 on Computational Cartography and Map Generalisation with Deep Learning, this CartoAI workshop seeks to gather the researchers working on artificial intelligence techniques applied to cartography. For this workshop, contributions on all AI techniques are welcome including, e.g., machine learning, optimization, and multi-agent systems.
The workshop will give the opportunity to the participants to give short presentations of recent and on-going research. It will also include a keynote and time for discussions.

The following topics are welcome:
* Novel machine learning techniques applied to maps or to cartographic data
* AI techniques for map generalisation
* AI techniques for map schematization
* AI techniques for text placement
* Pattern recognition in maps
* Cartographic style transfer
* AI techniques for map vectorization and map label interpretation
* Use of deep learning in cartography
* Methods of unsupervised machine learning, including clustering and dimensionality reduction
* Evaluation of AI techniques and results
* Review of AI studies in cartography


## Program

* **09:00 - 09:05** Welcome and Introduction

* **09:05 - 09:30** Keynote by *Robert Weibel (University of Zurich, Switzerland)* - Digging deep in history: Can we learn from the past to inform present and future AI models of map generalization?

* **09:30 - 10:50** Presentation Session 1: Cartography in the AI era (7 min + 5 min Q&A)
 * Azelle Courtial, Jérémy Kalsron, Bérénice Le Mao, Quentin Potié, Guillaume Touya and Laura Wenclik. [Text-to-map generation: a review of current potentials](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/02_CR_CartoAI_Courtial.pdf).
 * Quentin Potié, Guillaume Touya and William Mackaness. [Experiments in the automatic segmentation of anchors using deep learning techniques](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/01_CR_CartoAI_Potie.pdf).
 * Lawrence Stanislawski, Nattapon Jaroenchai, Shaowen Wang, Ethan Shavers, Alexander Duffy, Phillip Thiem, Zhe Jiang and Adam Camerer. [Transferring deep learning models for hydrographic feature extraction from IfSAR data in Alaska](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/10_CR_CartoAI_Stanislawski.pdf).
 * Yuhao Kang, Song Gao and Robert Roth. [Artificial Intelligence Studies in Cartography: A Review and Synthesis of Methods, Applications, and Ethics](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/12_CR_CartoAI_Kang.pdf).
 * Rachid Oucheikh and Lars Harrie. (Video presentation). [Design, implementation and evaluation of generative deep learning models for map labeling](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/09_CR_CartoAI_Oucheikh.pdf).
 * Lingrui Yan, Tinghua Ai, Aji Gao and Junbo Yu. (Video presentation). [Relief Shading Generation under Generative Adversarial Nets Considering Artistic Style Transfer](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/05_CR_CartoAI_Yan.pdf).
* **10:50 - 11:05** Break
* **11:05 - 12:25** Session 2: Map Generalization with AI (7 min + 5 min Q&A)
  * Nicolas Beglinger, Zhiyong Zhou, Cheng Fu and Robert Weibel. [Vector road generalization using deep learning: An empirical study](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/07_CR_CartoAI_Beglinger.pdf).
  * Yu Feng. [Prompt-aided Map Generalization with Diffusion Models](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/08_CR_CartoAI_Feng.pdf).
  * Eric Lafon, Quentin Potié and Guillaume Touya. [Salient building detection using multimodal deep learning](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/03_CR_CartoAI_Lafon.pdf).
  * Iga Ajdacka and Izabela Karsznia. [The use of machine learning to automate the selection of rivers for small-scale maps](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/06_CR_CartoAI_Ajdacka.pdf).
  * Barry Kronenfeld, Lawrence Stanislawski, Barbara Buttenfield and Ethan Shavers. (Video presentation). [Generalization quality metrics to support multiscale mapping: Hausdorff and average distance between polylines](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/04_CR_CartoAI_Kronenfeld.pdf).
  * Aji Gao, Tinghua Ai, Haosheng Huang, Junbo Yu and Huafei Yu. (Video presentation). [A Deep Learning Approach to 3D Building Map Generalization Based on Graph Convolutional Networks](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/cartoai2023/11_CR_CartoAI_Gao.pdf).
* **12:25 - 12:30**  Closing


## Important dates

* Submission deadline: 			14 July 2023,
submitted to EasyChair.
* Notification of acceptance: 		31 July 2023
* Workshop:  			12 September 2023

## Organizing Committee

* Yu Feng, Technical University of Munich, Germany
* Cheng Fu, University of Zurich, Switzerland
* Jan-Henrik Haunert, University of Bonn, Germany
* Rachid Oucheikh, Lund University, Sweden
* Guillaume Touya, IGN, Univ. Gustave Eiffel, France
* Zhiyong Zhou, University of Zurich, Switzerland
