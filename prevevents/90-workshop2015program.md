---
  layout: page
---

18th ICA Workshop - Program
---------------------------

### Friday, 21st August

(download archive with all submissions [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submissions.zip))

08:30 – 09:00 Registration

09:00 – 09:15 Welcome (and fast forward session)

09:15 – 10:30 Session 1: Generalisation of Map Series & Conflation

Marion Dumont, Guillaume Touya and Cécile Duchêne ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/Dumont_Gene&MR_210815.pdf))
[*Automated Generalisation of Intermediate Levels in Multi-Scales Pyramid*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_6.pdf)

Dan Lee ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/DanLee_ICAGenMR_ISPRS_Workshop2015.pdf))
[*Using Conflation for Keeping Data Harmonized and Up-to-date*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_8.pdf)

Anna Vetter, Mark Wigley, Dominik Käuferle and Georg Gartner ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/Vetter_ICC_Workshop_Presentation.pdf))
[*The automatic generalisation of building polygons with ArcGIS standard tools based on the 1:50\`000 Swiss National Map Series*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_2.pdf)

10:30 – 11:00 Coffee break

11:15 – 12:30 Session 2: Network Generalisation

Radan Suba, Martijn Meijers and Peter Van Oosterom ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/ICA_VarioScaleRoad_Aug2015.pdf))
[*Large scale road network generalization for vario-scale map*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_7.pdf)

Ling Zhang and Eric Guilbert ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/lingzhang2015_ICA_workshop.20150821.pdf))
*[Drainage tree construction based on patterns in a river network](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_5.pdf) *

Lawrence Stanislawski, Barbara Buttenfield and Paulo Raposo ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/2015ICA_Workshop_SynopticMetrics.pdf))
[*Synoptic Evaluation of Scale-Dependent Metrics for Hydrographic Line Feature Geometry*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_9.pdf)

12:15 – 13:30 Lunch

13:30 – 14:30 Break-out session

14:30 – 15:00 Reports from break-out session

15:00 – 15:30 Coffee break

15:30 – 16:45 Session 3: Ontologies, Homogenization & Multimedia Summarization

Eric Guilbert and Bernard Moulin ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/landformp.pdf))
[*Towards a contextual representation of landforms *](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_3.pdf)

Richard Guercke and Monika Sester
[*Homogenization of Facade Structures*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_4.pdf)

Guillaume Touya ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/Touya_ICAworkshop_2015_presentation.pdf))
*[Lessons Learned From Research on Multimedia Summarization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/genemr2015_submission_1.pdf) *

16:45 – 17:00 Wrap up, next steps
