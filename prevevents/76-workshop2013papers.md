---
  layout: page
---

16th ICA Workshop - Papers, program and presentations
-----------------------------------------------------

### Friday, 23. August

08:30 – 09:00 Registration

09:00 – 09:30 Welcome and fast forward session

09:30 – 10:40 Session 1: Generalisation for Topographic Map Production

C. Duchêne, J. Stoter and D. Burghardt ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/2013_WSICA_DucheneEtAl.pdf))
*Feedback NMA Workshop in Barcelona*

P. De Mayer and M. Jobst
*Activities of the Commission on Map Production and Geobusiness with focus on Service Oriented Mapping*

P. Erauw and A. Féchir ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/2013_WSICA_Eraw.pdf))
[*Implementation of Automatic Generalisation in the Production Process of the 1:50.000 Map Series*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_7.pdf)

V. van Altena, M. Post, R. Nijhuis and J. Stoter ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/2013_WSICA_VanAltenaEtAl.pdf))
[*Generalisation of 1:50k roads centrelines from 1:10k road polygons in an automated workflow*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_18.pdf)

10:40 – 11:10 Coffee break

11:10 – 12:30 Session 2: Relation modelling and contextual generalisation

H. Yan, T. Liu, W. Yang and J. Li
[*Spatial Similarity Relations in Multi-scale Map Spaces: Basic Concepts and Potential Research Issues*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_9.pdf)

A. Maudet, G. Touya, C. Duchêne and S. Picault ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ica_workshop_maudet.pdf))
[*Improving multi-level interactions modelling in a multi-agent generalisation model: first thoughts*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_2.pdf)

C. Stern and M. Sester ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ICA_Workshop_Presentation_Stern_Sester_final.pdf))
[*Extending an approach to derive constraints for the integration and generalization of detailed environmental spatial data in DLM or maps of small scales*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_21.pdf)

N. Gould and J. Cheng ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/presentation_nick_gould.pdf))
[*A prototype for ontology driven on-demand mapping of urban traffic accidents*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_8.pdf)

12:30 – 14:00 Lunch

14:00 – 15:00 Session 3: Point selection and Dynamic Mapping

N. Schwartges, D. Allerkamp, J.-H. Haunert and A. Wolff ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/slides-schwartges.pdf))
[*Optimizing Active Ranges for Point Selection in Dynamic Maps*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_5.pdf)

P. Bereuter and R. Weibel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ICA_GENL_2013_Bereuter_Weibel.pdf))
[*Assessing Real-Time Generalisation of Point Data*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_13.pdf)

P. Raposo, C.A. Brewer and L.V. Stanislawski ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/RaposoBrewerStanislawski_point_enrichment_and_gen.pdf))
[*Label and Attribute-Based Topographic Point Thinning*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_16.pdf)

15:00 – 16:00 Break-out session

16:00 – 16:30 Coffee break

16:30 – 17:15 Reports from break-out session

### Saturday, 24. August

08:30 – 9:15 Session 4: Vendors presenting recent generalisation software developments

D. Watkins - ESRI ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Esri_Generalization_ICC_Commission_Workshop.pdf))

A. Mathur - Axes Systems AG ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ICC_Presentation_Mo_26-08-2013_for_PDF.pdf))
[*Automatic Production and Updating of Topographic Maps - A Case Study Using the German AAA Data Model*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Automatic_Production_and_Updating_of_Topographic_Maps_v2.0.pdf)

N. Regnauld - 1Spatial ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Regnauld_ICA_Gene_Workshop2013.pdf))

09:30 – 10:30 Session 5: Selection of Road Networks and Hydrographic Networks

S.A. Benz and R. Weibel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ICA_Workshop_Presentation_Benz_Weibel_2013.pdf))
[*Road Network Selection Using an Extended Stroke-Mesh Combination Algorithm*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_11.pdf)

R. Weiss and R. Weibel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Weiss_Weibel_ICAWorkshop_Presentation_2013.pdf))
[*Road Network Selection for Small-Scale Maps Using an Improved Centrality Approach*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_14.pdf)

B.P. Buttenfield, L.V. Stanislawski, C. Anderson-Tarver and M.J. Gleason ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Buttenfield_Braids7.pdf))
[*Alternate Methods for Automatic Selection of Primary Paths Through Braided Hydrographic Networks*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_15.pdf)

10:30 – 11:00 Coffee break

11:00 – 12:00 Computer Demo / Posters / Maps

12:00 – 13:30 Lunch

13:30 – 14:30 Session 6: Continuous zooming, Morphing and Collapse Operator

R. Suba, M. Meijers and P. van Oosterom
[*2D vario-scale representations based on real 3D structure*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_4.pdf)

D. Peng, J.H. Haunert, A. Wolff and C. Hurter
[*Morphing Polylines Based on Least-Squares Adjustment*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_6.pdf)

S. Szombara ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ICA_WoGaMP_Szombara_a.pdf))
[*Unambiguous Collapse Operator of Digital Cartographic Generalisation Process*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_20.pdf)

14:30 – 15:50 Session 7: Contour lines and line simplification

R.Y. Peters, H. Ledoux and M. Meijers ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/ica2013_ravi-peters2.pdf))
[*Generation and generalization of safe depth-contours for hydrographic charts using a surface-based approach*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_3.pdf)

W. Socha and J. Stoter ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Automatic_Generalisation_of_Electronic_Navigational_Charts.pdf))
[*First attempts to automatize generalisation of electronic navigational charts - specifying requirements and methods*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_12.pdf)

Z. Ungvári, N. Agárdi and L. Zentai ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/Map_gen_ungari_2013.pdf))
[*A comparison of methods for automatic generalization of contour lines generated from digital elevation models*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/orkshop2013/genemappro2013_submission_1.pdf)

R. Olszewski, A. Fiedukowicz and A. Pillich-Kolipinska ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/slides/RO_AF_APK_modified_WEA.pdf))
[*Utilisation of computational intelligence for simplification of linear objects using extended WEA algorithm*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2013/genemappro2013_submission_17.pdf)

15:50 – 16:20 Coffee break

16:20 – 17:00 Wrap up, next steps

Normal 0 21 false false false DE X-NONE X-NONE *First Attempts to automatize generalisation of electronic navigational charts – specifying requirements and methods*
