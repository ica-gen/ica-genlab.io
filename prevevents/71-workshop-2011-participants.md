---
  layout: page
---

14th ICA Workshop - Participants
--------------------------------

39 people from 12 countries participated to this workshop:

![group2011part](prevevents/images/images/Events/group2011part.jpg)

Dogan Altundag, Kadaster, Netherlands
Serdar Aslan, General Command Mapping, Turkey
Sandrine Balley, Ordnance Survey, UK
Cynthia Brewer, Pennsylvania State University, USA
Marc-Olivier Briat, ESRI, USA
Dirk Burghardt, TU Dresden, Germany
Barabara Buttenfield, University of Colorado, USA
Tobias Dahinden, University of Hannover, Germany
Cécile Duchêne, IGN/COGIT, France
Pieter Erauw, IGN-NGI, Belgium
Nick Gould, University of Manchester, UK
Richard Guercke, University of Hannover, Germany
Lars Harrie, University of Lund, Sweden
Kusay Jaara, IGN/COGIT, France
William Mackaness, University of Edinburgh, UK
Martijn Meijers, TU Delft, Netherlands
Wouter Meulemans, TU Eindhoven, Netherlands
Sébastien Mustière, IGN/COGIT, France
Byron Nakos, University of Athens, Greece
Ron Nijhuis, Kadaster, Netherlands
Jesus Palomar-Vazquez, Polytechnical University of Valencia, Spain
Woojin Park, Seoul National University, South Korea
Karsten Pippig, TU Dresden, Germany
Maria Pla, Institut Cartogràfic de Catalunya, Spain
Paolo Raposo, Pennsylvania State University, USA
Andreas Reimer, GFZ German Research Centre For Geosciences, Germany
Jérémy Renard, IGN/COGIT, France
Patrick Revell, Ordnance Survey, UK
Sandro Savino, University of Padova, Italy
Monika Sester, University of Hannover, Germany
Özlem Simav, General Command Mapping, Turkey
Vince Smith, Intergraph, USA
Larry Stanislawski, USGS-CEGIS, USA
Christian Stern, Karlsruhe University of Applied Sciences, Germany
Jantien Stöter, Kadaster & TU Delft, Netherlands
Guillaume Touya, IGN/COGIT, France
Peter van Oosterom, TU Delft, Netherlands
David Watkins, ESRI, USA
