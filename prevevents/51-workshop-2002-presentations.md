---
  layout: page
---

6th ICA Workshop - Presentations
--------------------------------

### List of Abstracts / Working Paper and Presentation

[TABLE]

 

### Short Summaries of the Outcome of the Discussion Groups:

#### Rapporteurs & Topics:

1.  Christian Heipke: [images, vector and surface generalization - how can it be linked?](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2002/report_heipke.pdf)
2.  Geoff Dutton: semantic abstraction
3.  Rupert Brooks: [success of generalization software - suggestions for the future](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2002/Generalization_Software_notes_from_session.pdf)
4.  Martin Galanda: [Generalization for mobile displays / services](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2002/generalization_for_mobile_devices.pdf)
5.  Anne Ruas: [MRDB and generalization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2002/MRDB-gene.pdf)
6.  Geoff Edwards: the role of cognition and perception
7.  Stephan Nebiker: [3D-generalization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2002/Session_Protocol_3D_Generalisation.pdf)
