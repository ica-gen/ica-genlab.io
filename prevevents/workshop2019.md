---
  layout: page
---


Abstractions, Scales, and Perception, 22nd ICA Workshop
--------------------------------------------------------------

International Cartographic Association Pre-Conference Joint Commission Workshop (Commission on Cognitive Issues in Geographic Information Visualization, and Commission on Map Generalisation and Multiple Representation)
Location: Tokyo, Japan (same venue as [ICC'2019][1]).
Date: Monday 15th July 2019  (one-day workshop)

**Paper deadline: 22nd February 2019**

See more details and the submission procedure on the [workshop website][3].

## Program and papers

9:00-9:20 Introduction
9:20-10:20 First Talk session – 10′ presentation +10’Q/A per talk
* Lucille Alonso, Florent Renard – [Spatial comparison of the perception of urban heat islands and thermal comfort zones with participative in situ measurements and remote sensing data](/downloads/abs2019/Abs2019_paper_3.pdf) – University of Lyon – UMR CNRS Environment City Society, France.
* Barry Kronenfeld, Barbara Buttenfield, Lawrence Stanislawski and Jiaxin Deng – [Alternate definitions of linear and areal displacement between polylines – The plot thickens](/downloads/abs2019/Abs2019_paper_8.pdf) – Eastern Illinois University & University of Colorado – Boulder, USA
* Mathias Gröbe, Dirk Burghardt – [Verification of Multi-Scale Map Design](/downloads/abs2019/Abs2019_paper_7.pdf) – TU Dresden, Germany.
10:20-10:40 Coffee Break (Miraikan Cafe 5F)
10:40-12:00 Second Talk Session – 10′ présentation + 10′ Q/A per talk
* Florian Hruby, Rainer Ressl – [Scale in Immersive Virtual Environments](/downloads/abs2019/Abs2019_paper_2.pdf) – National Commission for Knowledge and Use of Biodiversity (CONABIO), Mexico & University of Vienna, Austria
* Arzu Cöltekin – [What contributes to the complexity of visuospatial displays?](/downloads/abs2019/Abs2019_paper_6.pdf) – University of Applied Sciences & Arts Northwestern Switzerland.
* Larry Stanislawski, Barbara Buttenfield, Barry Kronenfeld and Ethan Shavers – [Scale-specific metrics for adaptive generalization and geomorphic classification of stream features](/downloads/abs2019/Abs2019_paper_9.pdf) – U.S. Geological Survey-CEGIS & University of Colorado – Boulder, USA.
* Guillaume Touya – [Finding the Oasis in the Desert Fog? Understanding Multi-Scale Map Reading](/downloads/abs2019/Abs2019_paper_5.pdf) – IGN ENSG, LaSTIG GEOVIS, France.
12:00-14:00 Lunch time
14:00-15:00 Demo & Coffee time
15:00-16:00 Brainstorming time
16:00-16:30 Brainstorming reports and concluding remarks

## Purpose
The ICA Commissions on Cognitive Visualization, and on Generalisation and Multiple Representations, are pleased to organize a joint one-day workshop dedicated to advances, works in progress, and position statements about the perception of geospatial abstractions or the perception of scales in maps and geovisualisations.

Abstraction is the main process for moving from a geographic space or phenomenon to its representation as spatial information. Abstractions may be conceptual, geometric, semantic, graphic, visual, or cognitive. Examples include techniques to highlight, enhance, or simplify salient characteristics or properties, in order to support visual communication, recognition, understanding of spatial features and inferring knowledge about spaces. If different aspects of abstraction are not managed well, across different scales, mostly when navigating through scales in geoportals for instance, it can lead to perceptual difficulties in reading the map. Generalisation is the process of deliberately transforming existing geospatial data or their symbolisation into more abstract representations, and multiple representations involves creating a series of such generalisations, often distributed through map scale.

The workshop will feature research presentations and open-ended brainstorming sessions, and will focus on identifying open research gaps and the elaboration of a shared research agenda. We encourage submissions from any practitioner of abstraction or generalisation: those in academia, industry, government, among other sectors, are welcome.

## Topics of interest include but are not limited to:
- multi-scale, multi-source and multi-view graphic representation
- massive data (“Big Data”) generalization
- continuous visualization (across scales)  and fluid interaction with graphic representation
- (semi) automatic approaches for map design and geovisualization
- generalisation, schematization, and stylization techniques
- visual perception measures and experimental approaches to assess visual perception
- thematic applications: statistical and socio-economic data, spatio-temporal data and phenomena, urban and environmental dynamics, etc.
- technical applications, adaptation to visualization devices, (i.e. smartphones, tablets, VR/AR) and use contexts (e.g.,  emergency and crisis management, individual mobility, industrial purposes, etc.).

## Scientific workshop organisers:
ICA Commissions on Cognitive Visualization & Generalisation and Multiple Representation:
Pia Bereuter,
Sidonie Christophe,
Amy Griffin,
Paulo Raposo,
Guillaume Touya

## Photos of the workshop (courtesy of Timofey Samsonov)

![](images/images/Events/abs2019/ABS2019_03.jpg)

![](images/images/Events/abs2019/ABS2019_04.jpg)

![](images/images/Events/abs2019/ABS2019_51.jpg)

[1]: http://www.icc2019.org/
[2]: http://www.icc2019.org/data/ica-abstract_teplate.docx
[3]: https://icc2019abstraction.wordpress.com/
