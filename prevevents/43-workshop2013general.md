---
  layout: page
---

16th ICA Workshop - General information
---------------------------------------

*![ica logo](prevevents/images/ica_logo.png)*

 

*In the tradition of pre-conference workshops in association with the International Cartographic Conference (ICC) the **ICA Commission on Generalisation and Multiple Representation** and the **ICA Commission on Map Production and Geo-Business** will jointly organise a workshop in*

* 
*

*Dresden, Germany*, 2*3-24 August, 2013*

 

*For the ICA Commission on Generalisation and Multiple Representation it will be the **16******th**** **ICA Generalisation Workshop**.
*

**Key dates:**

-   *****8 April: deadline for submission of short papers (limit 3000 words, and 8 pages including figures)*
-   *30 April: notification of acceptance of paper for presentation (& invitation)*
-   15 August: deadline for submission of revised papers (participants will have around two weeks to read papers prior to the workshop)
-   23 and 24 August: Workshop

*Submission and review of short papers will be managed through EasyChair
(an online conference management system): <https://www.easychair.org/conferences/?conf=genemappro2013>*

** **

**Place and local organising committee:**
*The workshop will be held at Technical University of Dresden. A revised version of the accepted papers is expected to take into account the reviewers’ comments, not exceeding 10 pages. Participants are expected to read the papers before the workshop so presentations will be relatively short (10-15’) to leave place for questions and discussions. Time will also be kept for discussions in small groups and open problem solving. *

*A word template is available   [ICA\_Workshop\_Template\_2013.doc](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/ICA_Workshop_Template_2013.doc)*

**Topics of interest:**

*Besides submissions related to research interests of NMAs, vendors and practitioners, the organisers particularly welcome papers that address the topics listed below, which cover research themes that the organising Commissions are is particularly interested in exploring further. *Please note that for any of these topics, on top of paper submissions the organisers welcome demonstrations that illustrate the utility of systems, or that demonstrate the handling of large volumes of geographic data.

**User Specification**

Methods for capturing user needs that include ‘translation’ into a map specification (particularly in the context of web based requests – that require simple interactions); a* specification that includes not just the content and geographic extent, but the generalisation operations required to cartographically render the map in a way that meets the specific needs of the user. *In particular rapid prototyping approaches to map generalisation or user driven map generalisation rather than ‘data centric’ solutions.

***Task Oriented Map Specifications***

*In more customised environments it should be possible to provide thematic maps tailored to the needs of the user (rather than traditional data centric approaches), *e.g. geological mapping and 3D mapping, demographic, epidemiology, temporal maps. Furthermore p*apers are welcomed concerned with changing contexts of use, such as ‘spoken maps’, maps over mobile location aware devices, where generalisation is used to control variable delivery of geographic information at different scales.*

**Generalisation in support of Data integration**

Increasingly NMAs seek to provide their data as a basis for third party integration. Generalisation is required in order to support integration of data sourced at different scales. Such work includes techniques for making explicit relationships and qualities that exist between phenomena in order that databases can better support map generalisation processes, interrogation, and conflation. The issue of data integration also extends into ontological modelling and schema ‘alignment’ or schema integration.

**Map Production**

Distributed networks enable new methods and paradigm of map production, which may work more efficient and economic. Various international experiences with these Service-Oriented Architectures give an insight into the power of a SOA-enabled map production. With reference to spatial-data-services, e.g. generalisation services and “edge-matching” services, reports, experiences and further possibilities for map production and geobusiness should be discussed.

**Characterising and Reasoning about Geographic Phenomena**

Detection and representation of structures relevant for later generalisation and abstraction. Ontological modelling as a basis for reasoning about geographic information and as a basis for semantic modelling. Modelling and visualising different characteristics of geographic phenomena, e.g. connectivity, flow, sinuosity, alignment, gestalt properties.

**Web Services**

Web services as a mechanism for sharing map generalisation algorithms and more broadly open source solutions to map generalisation.

**Service-Oriented Mapping**

Availability, consistence and reliance are main keywords for data and service exchange. How can these aspects best described and supported by new (IT-) architectural methods? This and many other questions occur if sustainability and a productive realisation of SOMAP experiments should be achieved.

**Quality**

Measures and procedures to quantify and ensure quality of the generalisation process as well as within Service-Oriented map production; methods to visualise this quality are needed.

**3D-Generalisation**

Generalisation of individual 3D-objects, groups of 3D-objects, and terrain.
