---
  layout: page
---

19th ICA Workshop - Program, papers and presentations
-----------------------------------------------------

Automated generalisation for on-demand mapping
----------------------------------------------

### Tuesday, 14th June

(download archive with all submissions [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/genemr2016_submissions.zip))

08:30 – 09:00 Registration

09:00 – 09:10 Welcome (and fast forward session)

09:10 – 10:30 Session 1: On-demand mapping, generalisation ontologies and folksonomies

William Mackaness and Phil Bartie ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Mackaness_HelsinkiWorkshop.pdf))
[*Automatic Detection of Ports For Map Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_01.pdf)

Weiming Huang, Ali Mansourian and Lars Harrie ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Huang_et_al_ICA_Workshop.pdf))
[*On-demand mapping and integration of thematic data*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_02.pdf)

Meysam Aliakbarian and Robert Weibel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Aliakbarian_Weibel.pdf))
[*Integration of folksonomies into the process of map generalization*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_03.pdf)

Radek Augustýn ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Augustym_CZE_ICA_Helsinki.pdf))
[*CZE State Mapping Generalization Efforts*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_04.pdf)

10:30 – 11:00 Coffee break

11:00 – 12:20 Session 2: Multiscale Representations

Marion Dumont, Guillaume Touya and Cecile Duchene ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Agile2016_ppt_MarionDumont.pdf))
[*Assessing the Variation of Visual Complexity in Multi-Scale Maps with Clutter Measures*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_05.pdf)

Haowen Yan, Weifang Yang and Tao Liu ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Haowen_Yan.pdf))
[*Approach to calculating spatial similarity degree using map scale change of road networks in multi-scale map spaces*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_06.pdf)

Larry Stanislawski, Yan Liu, Barbara Buttenfield, Kornelijus Survila and Jeffrey Wendel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/AGILE_ICA_2016_Stanislawski_etal.pdf))
[*High Performance Computing to Support Multiscale Representation of Hydrography for the Conterminous United States*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_07.pdf)

Radan Suba, Mattijs Driel, Martijn Meijers, Elmar Eisemann and Peter van Oosterom ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Radan_presentation.pdf) + [demo](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Radan_demo_task.avi))
[*Usability test plan for truly vario-scale maps*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_08.pdf)

12:20 – 14:00 Lunch

14:00 – 15:00 Break-out session

Topic 1: Semantic similarity measures ([notes](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/SemSim.pdf))
Topic 2: Managing relationships between background data and thematic data ([notes](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/BreakoutRelationsThematicBackground.pdf))
Topic 3: Transitions of scale and clutter measures ([notes](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/TransitionsOfScaleAndClutterMeasures.pdf))

15:00 – 15:30 Reports from break-out session 

15:30 – 16:00 Coffee break

16:00 – 17:20 Session 3: Generalisation algorithms and production workflows

Nicolas Regnauld ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Generalise_ICA_worshop2016.pdf))
[Automatic Generalisation for production](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_09.pdf)

Azimjon Sayidov and Robert Weibel ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Sayidov_GEO_UZH.pdf))
[*Constraint-based approach in geological map generalization*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_10.pdf)

Martijn Meijers ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/icagen2016_meijers.pdf))
[*Building simplification using offset curves obtained from the straight skeleton*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_11.pdf)

Timofey Samsonov and Olga Yakimova ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/Samsonov_Yakimova_2016.pdf))
*[Geometric Simplification of Administrative Borders With Mixture of Irregular and Orthogonal Segments](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/genemr2016_paper_12.pdf) *

17:20 – 17:30 Wrap up, next steps

 

Normal 0 21 false false false DE X-NONE X-NONE *First Attempts to automatize generalisation of electronic navigational charts – specifying requirements and methods*
