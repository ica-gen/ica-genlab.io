---
  layout: page
---


ICA Workshop on Multiscale Cartography and Sustainability
--------------------------------------------------------------

Vancouver, Canada (and possibly online)

**16th August, 2025 from 8:00 to 11:30 PST (UTC-8)**

A detailed description of the workshop coming soon.

 The submissions should cover one the following topics:

*	Multiscale cartography of towards achieving the United Nations Sustainable Development Goals (SDGs)
*	Mapping service accessibility and gaps
*	Green multiscale cartography
*	Modifiable Aerial Unit Problem
*	Automated metric assessment of generalized geospatial data quality, such as (but not limited to):
  *	Legibility and legibility conflicts
  *	Horizontal or vertical positional displacement
  *	Feature density pattern retention or change
  *	Sinuosity preservation
  *	AI methods to assess generalization
  *	Perceptual quality
  *	Retention of partonomic (hierarchical, compositional, etc.) relations between scales
  *	Maintaining contextual relations between scales
*	AI or machine learning applications related to the above topics
*	Cartographic generalization   in application to address sustainability and/or SDGs



## Program

To be announced...

## Registration

Registration is done via the ICC conference registration system. Please visit the ICC 2025 [website][4]

If you only plan to attend online, you just need to use the link above, no need for registration.

## Submissions

You are invited to submit a 2-page paper, on ongoing research or position papers, following the general guidelines of the [ICA conference abstracts][2].

The proceedings of the workshop will be published online with the CC-BY licence, on this page. Papers should be submitted to [EasyChair][1].

As the workshop promotes open science, we strongly encourage that the datasets used in the presented research are made available, as well as the code. The code can be deposited in a platform such as Github, while the datasets can be uploaded to [Zenodo.org][3] or similar repositories, and the DOI should be mentioned in the paper.

The proceedings will be published openly on this website (CC-BY licence).

The workshop will be open to researchers and practitioners interested in cartography for sustainability, map generalisation and multiple representation, regardless of submission or acceptance of an abstract.

## Important dates

* Submission deadline: 			15th April 2025,
submitted to EasyChair.
* Notification of acceptance: 		by 9th May, 2025
* Workshop:  				Tuesday, 13th June, 2023

## Organizing and Scientific Committee

* Carolyn Fish, University of Oregon, USA
* Izabela Karsznia, University of Warsaw, Poland
* Nicolas Regnauld, Esri, France
*	Britta Ricker, Utrecht University, Netherlands
* Timofey Samsonov, Lomonosov Moscow State University, Russia
* Lawrence V. Stanislawski, USGS, USA
* Guillaume Touya, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.




[1]: https://easychair.org/conferences?conf=icamultisustain25
[2]: https://www.ica-conference-publications.net/ica-abstracts_word_template.docx
[3]: https://zenodo.org/
[4]: https://icc2025.com/
