---
  layout: page
---

15th ICA Workshop - Papers and program
--------------------------------------

### Thursday, 13. September

(download archive with all submissions [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions.zip))

8.30 – 9.00 Registration

9:00 – 9.30 Welcome and fast forward session

9:30 – 10:45 Session 1: Process modelling and shared semantics for on‐demand mapping

Sandrine Balley, Kusay Jaara and Nicolas Regnauld
[*Towards a Prototype for Deriving Custom Maps from Multisource Data*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session1_sub1.pdf)

Nick Gould and Omair Chaudhry
[*An Ontological approach to On‐demand Mapping*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session1_sub2.pdf)

Guillaume Touya, Sandrine Balley, Cecile Duchene, Kusay Jaara, Nicolas Regnauld and Nicholas Gould
[*Towards an Ontology of Generalisation Constraints and Spatial Relations*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session1_sub3.pdf)

10:45 – 11:15 Coffee break

11:15 – 12:30 Session 2: Data structures and handling of user generated content

Matthias Müller and Stefan Wiemann
[*A framework for building multi‐representation layers from OpenStreetMap data*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session2_sub1.pdf)

Ralf Klammer and Dirk Burghardt
[*Approaches for enhancing tile‐based mapping with cartographic generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session2_sub2.pdf)

Martijn Meijers, Jantien Stoter and Peter Van Oosterom.
*[Comparing the vario‐scale approach with a discrete multi‐representation based approach for automated generalisation of topographic data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session2_sub3.pdf)*

12:30 – 14:00 Lunch

14:00 – 14:50 Session 3: Generalisation of schematic maps

Andreas Reimer and Christian Volk
[*An approach for using cubic Bézier curves for schematizations of categorical maps*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session3_sub1.pdf)

Patrick Lüscher, Patrick Stoop, Jürg Fehlmann and Jörg Sparenborg
*[Automatic generation of network schematics from cadastral databases: Applications for an electric utility company](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session3_sub2.pdf)*

15:00 – 16:00 Break‐out session

16:00 – 16:30 Coffee break

16:30 – 17:15 Reports from break‐out session

### Friday, 14. September

9:00 – 10:40 Session 4: Workflow and production systems for topographic data

Sandro Savino
[*New perspectives in the generalization of medium‐large scale databases in Italy*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session4_sub1.pdf)

Nicolas Regnauld, Mark Plews and Paul Martin
[*An Enterprise System for Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session4_sub2.pdf)

Osman Nuri Çobankaya and Necla Uluğtekin
[*Updating the Multiple Representation Database*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session4_sub3.pdf)

Blanca Baella, Anna Lleopart and Maria Pla
[*ICC Topographic Databases: Design of a MRDB for data management optimization*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session4_sub4.pdf)

10:40 – 11:30 Coffee break and poster session

11:30 – 12:30 Break‐out session

12:30 – 14:00 Lunch

14:00 – 15:15 Session 5: Generalisation algorithms

Lawrence Stanislawski, Mark‐Olivier Briat, Edie Punt, Michael Howard, Cindy Brewer and
Barbara Buttenfield
*[Density Stratified Thinning of Road Networks to Support Automated Generalization for The National Map](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session5_sub1.pdf)*

Serdar Aslan, İ.Öztuğ Bildirici, Özlem Simav and Bülent Çetinkaya
[*An Incremental Displacement Approach Applied to Building Objects in Topographic Mapping*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session5_sub2.pdf)

Junqiao Zhao, Jantien Stoter, Hugo Ledoux and Qing Zhu
[*Repair and generalization of hand‐made 3D building models*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/submissions2012/genemr2012_session5_sub3.pdf)

15:15 – 15:45 Coffee break

15:45 – 16:30 Reports from break‐out session

16:30 – 17:00 Wrap up, next steps
