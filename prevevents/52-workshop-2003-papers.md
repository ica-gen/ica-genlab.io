---
  layout: page
---

7th ICA Workshop - Papers
-------------------------

### Discussions

Database issues: multiple representation, incremental update and topological modelling
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/discuss_db_issues.pdf)

Visualisation on small displays
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/discuss_small.pdf)

### Papers and presentations

Karl-Heinrich Anders (U Hannover)
*A Hierarchical Graph-Clustering Approach to find Groups of Objects*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/anders_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/anders.pdf)

Blanca Baella and Maria Pla (ICC)
*An example of database generalization workflow: the Topographic Database of Catalonia at 1:25 000*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/baella_et_al_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/baella_et_al.pdf)

Sylvain Bard (COGIT)
*Evaluation of generalisation quality*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/bard_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/bard.pdf)

Matthias Bobzien and Dieter Morgenstern (U Bonn)
*Abstracting and Formalizing Model Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/bobzien_et_al_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/bobzien_et_al.pdf)

Dirk Burghardt and Alessandro Cecconi (U Zurich)
*Mesh Simplification for Building Selection*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/burghardt_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/burghardt_et_al.pdf)

Cécile Duchêne (COGIT)
*Coordinative agents for automated generalisation of rural areas*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/duchene_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/duchene.pdf)

Cécile Duchêne, Sylvain Bard, Xavier Barillot, Anne Ruas, Jenny Trévisan and Florence Holzapfel (COGIT)
*Quantitative and qualitative description of building orientation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/duchene_et_al_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/duchene_et_al.pdf)

Birgit Elias (U Hannover)
*Determination of Landmarks and Reliability Criteria for Landmarks*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/elias_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/elias.pdf)

Andrea Forberg and Helmut Mayer (BU Munich)
*Squaring and Scale-Space Based Generalization of 3D Building Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/forberg_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/forberg_et_al.pdf)

Martin Galanda (U Zurich)
*Modelling constraints for polygon generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/galanda_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/galanda.pdf)

Wenxiu Gao, Jianya Gong and Zhilin Li (U Wuhan, PU Hong Kong)
*Knowledge-based Generalization on Land-use Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/gao_et_al_v0.pdf)

Paul Hardy, Melanie Hayles and Patrick Revell (Laser-Scan)
*Clarity - a new Environment for Generalisation using AGENTS, JAVA, XML and Topology*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/hardy_et_al_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/hardy_et_al.pdf)

Frédéric Hubert and Anne Ruas (COGIT)
*A method based on samples to capture user needs for generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/hubert_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/hubert_et_al.pdf)

Bin Jang and Lars Harrie (U Gävle, U Lund)
*Cartographic Selection Using Self-Organizing Maps*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/jiang_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/jiang_et_al.pdf)

Novit Kreiter (swisstopo)
*Multirepresentation Databases and Need for Generalization at swisstopo*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/kreiter.pdf)

Jagdish Lal and Liqiu Meng (TU Munich)
*Aggregation on the Basis of Structure Recognition*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lal_et_al_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lal_et_al.pdf)

Dan Lee (ESRI)
*Recent Generalization Development and road ahead*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lee_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lee.pdf)

Cécile Lemarié (IGN France)
*Generalisation process for Top100: research in generalisation brought to fruition*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lemarie_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/lemarie.pdf)

Robert McMaster (U of Minnesota)
*The Creation of Multiple Scale Databases in the NHGIS*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/mcmaster.pdf)

Byron Nakos and Vassilis Mitropoulos (TU Athen)
*Local length ratio as a measure of critical points detection for line simplification*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/nakos_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/nakos_et_al.pdf)

Nicolas Regnauld, (OS)
*Algorithms for the Amalgamation of Topographic Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/regnauld_v0.pdf)

Vince Smith (Intergraph)
*Generalization for Medium-Scale Mapping: Results and Statistics from One Production Implementation*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/smith.pdf)

Lichun Sui, (TU Munich)
*Acquisition of Generalization Knowledge using Matching Methods*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/sui_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/sui.pdf)

J. Mark Ware, N. Thomas and Christopher Jones (U Glamorgan, U Cardiff)
*Resolving Graphic Conflict in Scale Reduced Maps: Refining the Simulated Annealing Technique*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/ware_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/ware_et_al.pdf)

Ian D. Wilson, J. Mark Ware and J. Andrew Ware (U Glamorgan)
*Reducing Graphic Conflict In Scale Reduced Maps Using A Genetic Algorithm*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/wilson_et_al_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2003/wilson_et_al.pdf)
