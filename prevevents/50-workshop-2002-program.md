---
  layout: page
---

6th ICA Workshop - Program
--------------------------

### Hosted by:

**CCRS – Canada Centre for Remote Sensing**
588 Booth Street
Ottawa, Ontario, Canada

[Internet: www.ccrs.nrcan.gc.ca](http://www.ccrs.nrcan.gc.ca)
### Sunday, July 7th, 2002

[TABLE]

### Monday, July 8th, 2002

[TABLE]

### General Comments:

The workshop – like its preceding ones – is organized on an informal and highly interactive basis. All participants are invited to actively contribute to the success of the event. This shall be achieved by

1.  the working papers of the presentations can be downloaded from the workshop web-site. **All participants are expected to read the papers**, so that the focus of the presentation can be less introductory and focus on the essential and hard problems.
2.  There are break-out sessions, where the whole group splits and discusses in depth given specific topics with a small number of people. The results of the discussions are reported back to the whole group. Tentative topics are given in the program, however, this is still open to be finalized during the workshop.

### Organizing Committee:

Dianne Richardson, Canada

Robert Weibel, Switzerland

Tiina Sarjakoski, Finland

Volker Walter, Germany

Monika Sester, Germany
