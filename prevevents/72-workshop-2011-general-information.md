---
  layout: page
---

14th ICA Workshop - General information
---------------------------------------

[TABLE]

The Chairs of the ICA commission on Map Generalisation and Multiple Representation and the ISPRS Commission II/2 Working group on Multiscale Representation of Spatial Data warmly encourage the submission of research papers to the 14th Generalisation Workshop. This time the workshop will explore in particular multi scale on demand mapping and the changing context of map use.

This two days workshop will assume reading of papers prior to the workshop, comprise short presentations, break out sessions, demonstrations, posters, open problems and discussions. Participation and attendance at the workshop requires submission of research papers or position papers.

### Key dates

The workshop will be held in Paris, 30 June and 1st July 2011 (immediately prior to the ICA Conference 4-8th July 2011). Topics of interest are detailed below. Key dates are:

-   28 February: deadline (extended) for submission of extended abstracts (around 2000 words)
-   15 April: notification of acceptance of paper for presentation (&amp; invitation)
-   1 June: deadline for submission of revised extended abstracts (participants will have 1 month in which to read papers prior to the workshop)
-   30th June and 1st July: Workshop
-   2nd July: [Tutorial](68-tutorial-2011-program-slides-and-pictures.md) on generalisation (associated event)

### Abstract submission

Through EasyChair system <https://www.easychair.org/conferences/?conf=genemr2011>

### Place, location and format:

The workshop will be held at IGN France in Paris, at the end of June and immediately prior to the ICA conference. Presentations will be of mixed duration, but relatively brief, with a focus on discussion groups and open problem solving.

### Registration

Registration to the workshop (on invitation only): more information soon.
Registration to the associated [International Cartographic Conference](http://www.icc2011.fr/) is separated from registration to the workshop and now open.

### Topics of interest:

Besides submissions related to research interests of NMAs, vendors and practitioners, the reviewers particularly welcome papers that respond to changing contexts of map use, and in response to developing technologies in data capture, and dissemination via the web and through web based services. The following headings cover research that the Commission is particularly interested in exploring further.

**Contexts of Use**

Papers concerned with changing contexts of use, such as ‘spoken maps’, maps over mobile location aware devices, where generalisation is used to control variable delivery of geographic information at different scales.

**User Specification**

Methods for capturing user needs that include ‘translation’ into a map specification (particularly in the context of web based requests – that require simple interactions); a specification that includes not just the content and geographic extent, but the generalisation operations required to cartographically render the map in a way that meets the specific needs of the user. In particular rapid prototyping approaches to map generalisation or user driven map generalisation rather than ‘data centric’ solutions.

**Task Oriented Map Specifications**

In more customised environments it should be possible to provide thematic maps tailored to the needs of the user (rather than traditional data centric approaches), e.g. geological mapping and 3D mapping, demographic, epidemiology, temporal maps.

**Generalisation in support of Data integration**

Increasingly NMAs seek to provide their data as a basis for third party integration. Generalisation is required in order to support integration of data sourced at different scales. Such work includes techniques for making explicit relationships and qualities that exist between phenomena in order that databases can better support map generalisation processes, interrogation, and conflation. The issue of data integration also extends into ontological modelling and schema ‘alignment’ or schema integration.

**Characterising and Reasoning about Geographic Phenomena**

Detection and representation of structures relevant for later generalisation and abstraction. Ontological modelling in support of multi scale data integration, or in user specification. Ontological modelling as a basis for reasoning about geographic information and as a basis for semantic modelling. Modelling and visualising different characteristics of geographic phenomena (both within and between) e.g. connectivity, flow, sinuosity, alignment, gestalt properties.

**Web Services**

Web services as a mechanism for sharing map generalisation algorithms and more broadly open source solutions to map generalisation.

**Quality**

Measures and procedures to quantify and ensure quality of the generalisation process; methods to visualise quality.

**3D-Generalisation**

Generalisation of individual 3D-objects, groups of 3D-objects, and terrain.

For any of these topics, the Commission welcomes demonstrations that illustrate the utility of systems, or that demonstrate the handling of large volumes of geographic data.

### Scientific committee

**Program & Workshop chairs**
Dirk Burghardt
Lars Harrie
William Mackaness
Monika Sester

**Scientific committee**
Blanca Baella
Cynthia Brewer
Dirk Burghardt
Barabara Buttenfield
Omair Chaudhry
Tobias Dahinden
Cécile Duchêne
Theodor Foerster
Julien Gaffuri
Lars Harrie
Jan-Henrik Haunert
Patrick Luescher
William Mackaness
Liqiu Meng
Sebastien Mustiere
Maria Pla
Nicolas Regnauld
Patrick Revell
L. Tiina Sarjakoski
Monika Sester
Bettina Speckmann
Larry Stanislawski
Stefan Steiniger
Jantien Stoter
Guillaume Touya
Peter van Oosterom
Mark Ware
Robert Weibel
Alexander Wolff
