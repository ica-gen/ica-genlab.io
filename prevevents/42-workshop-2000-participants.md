---
  layout: page
---

4th ICA Workshop - Participants
-------------------------------

![group2000part](prevevents/images/images/Events/group2000part.jpg)

Lorenzo García Asensio Instituto Geográfico Nacional, Spain
Blanca Baella Institut Cartogràfic de Catalunya, Catalonia
José Luis García Balboa Grupo de Investigación en Ingeniería Cartográfica. Universidad de Jaén, Spain
Marianne Bengston Kort-og Matrikelstyrelsen, Denmark
Joachim Bobrich Institute of Cartography University of Hanover, Germany
Christoph Brandenberger Institut für Kartographie ETH, Switzerland
Manuel Antonio Ureña Cámara Grupo de Investigación en Ingeniería Cartográfica. Universidad de Jaén, Spain
Javier Iribas Cardona Delegación del Gobiero en La Rioja/Instituto Geográfico Nacional, Spain
Josep Lluís Colomer Institut Cartogràfic de Catalunya, Catalonia
David H. Douglas University of Gävle, Sweden
Matthias Ekkehard Head of marketing and applications on geoinformation city and state of hamburg department for building and construction surveying authority, Germany
Martin Galanda Department of Geography, University of Zurich, Switzerland
Miguel Garriga American Automobile Association, USA
Juan Francisco Reinoso Gordo Grupo de Investigación en Ingeniería Cartográfica.Universidad de Jaén, Spain
Kelvin Haire Laser-Scan Ltd, United Kingdom
Roland Hansson ESRI, USA
Paul Hardy Laser-Scan Ltd, United Kingdom
Erkki-Sakari Harju Chief Cartographer Geodata Oy, Finland
Peter Højholt Kort-og Matrikelstyrelsen, Denmark
Jeff Hyatt Intergraph Corp., USA
Dursun Er Ilgin General Command of Mapping Dept. of Cartography, Turkey
Henry Jackson Sr. System Consultant Intergraph Mapping and Information Systems, USA
YoleneJahard Institut Geographique National, France
Antti Jakobsson National Land Survey of Finland Development Centre , Finland
Francisco Javier Ariza López Grupo de Investigación en Ingeniería Cartográfica. Universidad de Jaén, Spain
Carles Karsunke Intergraph (España) S.A., Spain
François Lecordix Institut Geographique National, France
Dan Lee ESRI, USA
Liqiu Meng Lehrstuhl für Kartographie. Technical University of Munich, Germany
Jaume Miranda Institut Cartogràfic de Catalunya, Catalonia
Mark Pendlington Research Unit - Room C530 Ordnance Survey GB, United Kingdom
Rufino Pérez E.U.Ing.Técnica Topográfica. Universidad Politécnica de Madrid, Spain
Maria Pla Institut Cartogràfic de Catalunya, Catalonia
Manuel Gallego Priego Xunta de Galicia, Spain
Ari Purhonen Geodata Oy, Finland
M'tivier Romain Ministère des Ressources Naturelles, Canada
Joan Romeu Institut Cartogràfic de Catalunya, Catalonia
Anne Ruas Institut Geographique National, France
Santi Sànchez Institut Cartogràfic de Catalunya, Catalonia
Scott Seeley Intergraph Corp., USA
Vince Smith Intergraph Corp., USA
Jon Thies Intergraph Corp., USA
Isabel Ticó Institut Cartogràfic de Catalunya, Catalonia
Lysandros Tsoulos Assist. Professor. National Technical University of Athens, Greece
Sjef van der Sten ITC, The Netherlands
Jesús Palomar Vázquez Dpto. Ingeniería Cartográfica, Geodesia y Fotogrametría. Universidad Politécnica de Valencia, Spain
Torcuato Rivas Vega Instituto Geográfico Nacional, Spain
Mahes Visvalingam Department of Computer Science, University of Hull, England
Robert Weibel Department of Geography, University of Zurich, Switzerland
