---
  layout: page
---

13th ICA Workshop - Participants
--------------------------------

28 persons from 11 countries particpated to this workshop:

![group2010part](prevevents/images/images/Events/group2010part.jpg)

Pia Bereuter, Univ. of Zurich, Switzerland
Dirk Burghardt, Dresden University of Technology, Germany
Barbara Buttenfield, Univ. Of Colorado-Boulder, USA
Cécile Duchêne, IGN / COGIT, France
Hongchao Fan, Techinische Universität München, Germany
Julien Gaffuri, EU - Joint Research Center, Italy
Stefan Hahmann, Dresden University of Technology, Germany
Lars Harrie, Lund University, Sweden
Shen Jie Nanjing, University, China
Dominik Käuferle, SwissTopo, Switzerland
Patrick Lüscher, Univ. of Zurich, Switzerland
Bo Mao, Geoinformatics, KTH, Sweden
Daniel Pilon, Natural Resources Canada, Canada
Edith Punt, ESRI, USA
Andreas Reimer, HU Berlin/GFZ Potsdam, Germany
Jeremy Renard, IGN / COGIT, France
Patrick Revell, Ordnance Survey, UK
Urs-Jakob Rüetschi, ESRI, Switzerland
Sandro Savino, Univ. of Padova, Italy
Özlem Simav, General Command of Mapping, Turkey
Larry Stanislawki, USGS, USA
Jantien Stoter, Kadaster & OTB, TU Delft, Netherlands
Guillaume Touya, IGN / COGIT, France
Ramya Venkateswaran, Univ. of Zurich, Switzerland
Mark Ware, University of Glamorgan, UK
David Watkins, ESRI, USA
Robert Weibel, Univ. of Zurich, Switzerland
Luan Xuechen, Wuhan University, China
