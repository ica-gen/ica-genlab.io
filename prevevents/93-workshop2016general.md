---
  layout: page
---

19th ICA Workshop - General information
---------------------------------------

![ica logo commission](prevevents/images/logos/ica_logo_commission.png)![Agile](prevevents/images/logos/Agile.png)

Automated generalisation for on-demand mapping
----------------------------------------------

The next workshop of the ICA Commission on Generalisation and Multiple Representation will be carried out as one day pre-conference workshop prior to the AGILE conference

 

*Helsinki, Finland, 14 June, 2016*

 

The workshop will explore new challenges and solutions in the domain of automated generalisation for on demand mapping and the changing context of map use. Many tasks require us to visualise geographic information at a number of different scales, in a variety of environments, over different devices. Therefore research is concerned with automated methods that enable the creation and display of such geographic information at multiple levels of detail, across a range of technologies. The challenges of this research draws upon researchers and practitioners alike, working in the fields of on-demand mapping, multiple representations, data integration and generalisation of geographic information.

For the ICA Commission on Generalisation and Multiple Representation it will be the 19th ICA Generalisation Workshop. Participants of the workshop are invited to submit research papers or position papers.

### Key dates:

-   10 April: deadline for submission of short papers (limit 3000 words, and 8 pages including figures)
-   30 April: notification of acceptance of paper for presentation
-   31 May: deadline for submission of revised papers
    (participants will have around two weeks to read papers prior to the workshop)
-   14 June: Workshop

Submission and review of short papers will be managed through EasyChair
(an online conference management system):

<https://www.easychair.org/conferences/?conf=genemr2016>[](https://www.easychair.org/conferences/?conf=genemr2016)

### Location and registration:

The workshop will be held at University of Helsinki, address: Fabianinkatu 33, Helsinki.
[Workshop rooms and floor plan](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2016/AGILE2016_Workshops.pdf)[](https://agile-online.org/Conference_Paper/images/conference_2016/AGILE2016_Workshops.pdf)

###  Topics of interest:

-   Map specifications and user requirements for on-demand mapping
-   Modelling geographic relationships in automated environments
-   Generalisation ontologies for on-demand mapping
-   Development of generalisation algorithms
-   Generalisation process modelling and orchestration for on-demand mapping
-   Generalisation web services
-   Data integration and harmonisation of user generated or multi-source spatial content
-   Multi-scale data matching, change detection and updating
-   3D-Generalisation
-   Continuous generalisation
-   Evaluation and quality in generalisation

#####  Scientific workshop organisers & program committee:

Dirk Burghardt, Cecile Duchene

Barbara Buttenfield
Hongchao Fan
Nick Gould
Lars Harrie
Jan-Henrik Haunert
Dan Lee
William Mackaness
Martijn Meijers
Marc Post
Paulo Raposo
Nicolas Regnauld
Timofey Samsonov
Sandro Savino
Monika Sester
Lawrence Stanislawski
Stefan Steiniger
Christian Stern
Jantien Stoter
Guillaume Touya
Vincent van Altena
Peter Van Oosterom
Xiang Zhang
