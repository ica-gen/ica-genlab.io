---
  layout: page
---


25th ICA Workshop on Map Generalisation and Multiple Representation
--------------------------------------------------------------

Delft, The Netherlands (and online)

**13th June, 2023 from 13:30 to 17:00 CET**

**[Teams link to attend the workshop online][5]**

Though cartography has drastically changed in the past years with online interactive maps quickly replacing paper maps at one single scale, the need for map generalisation has not disappeared with the possibility to zoom in maps. Some old challenges stand still while others emerge, or become prominent. We want this workshop to be the meeting point of the traditional map generalisation practitioners and academics, as well as researchers interested in the abstraction and the multi-scale interactive visualisation of various types of spatial data.

The 25th ICA Workshop on Map Generalisation and Multiple Representation will be held prior to the AGILE 2023 conference. Participants of the workshop are invited to submit papers on ongoing research or position papers. Papers relating map generalisation or multiple representations to the theme of the AGILE conference, i.e. “spatial data for design” are highly encouraged. The submissions should cover one the following topics:

* map feature geometry simplification, aggregation, typification, etc.;
* map generalization for emerging technologies such as augmented or virtual reality;
* abstraction or sense-making of ‘Big Data’;
* semantic generalization;
* continuous generalization;
* multi-scale interactions for visualisation and analysis;
* computational geometric methods;
* map label selection;
* edge bundling in trail or flow maps;
* evaluation and quality in generalization;
* perception and cognition of multi-scale visualisations
* map generalisation and multiple representations at national mapping agencies


## Program

* **13h30-13h45** Introduction of the workshop
* **13h45-15h10** 1st Presentation session (10’ presentation + 10’ questions)
  * Jagadish Boodala, Onkar Dikshit and Nagarajan Balasubramanian. *Topological characterization of point data using graphlets* ([Download paper][11])
  * Bérénice Le Mao and Guillaume Touya. *Progressive river network selection for pan-scalar maps* ([Download paper][8])
  * Pyry Kettunen, Anssi Jussila and Christian Koski. *Compositions of generalised built areas for topographic maps* ([Download paper][10])     
  * Zhiyong Zhou, Cheng Fu and Robert Weibel. *Automatic labeling for data-driven building simplification in vector map generalization* ([Download paper][9])
* **15h10-15h30** Coffee break
* **15h30-16h10** 2nd Presentation session (10’ presentation + 10’ questions)
  * Guillaume Touya, Ridley Campbell and Laura Wenclik. *Generalisation Constraint Monitors to Assess Tactile Maps* ([Download paper][7])
  * Guillaume Touya, Justin Berli and Azelle Courtial. *Cartagen4py, an open source Python library for map generalisation* ([Download paper][6])
* **16h10-16h50** Panel discussion session with Robert Weibel (Univ. Zurich), Pyry Kettunen (FGI, NLS), and Nicolas Regnauld (ESRI) - Chair: Guillaume Touya
* **16h50-17h00** Conclusion of the workshop

## Registration

Registration is done via the AGILE conference registration system. Please visit the AGILE [website][4]

If you only plan to attend online, you just need to use the link above, no need for registration.

## Submissions

You are invited to submit a 2-page paper, on ongoing research or position papers, following the general guidelines of the [ICA conference abstracts][2].

The proceedings of the workshop will be published online with the CC-BY licence, on this page. Papers should be submitted to [EasyChair][1].

As the workshop promotes open science, we strongly encourage that the datasets used in the presented research are made available, as well as the code. The code can be deposited in a platform such as Github, while the datasets can be uploaded to [Zenodo.org][3], and the DOI should be mentioned in the paper.

The workshop will be open to researchers and practitioners interested in map generalisation and multiple representation, regardless of submission or acceptance of an abstract.

## Important dates

* Submission deadline: 			28th April 2023,
submitted to EasyChair.
* Notification of acceptance: 		by Monday, 22th May, 2023
* Workshop:  				Tuesday, 13th June, 2023

## Organizing Committee

* Pia Bereuter, FHNW University of Applied Sciences of Northwestern Switzerland.
* Izabela Karsznia, University of Warsaw, Poland
* Martijn Meijers, TU Delft, The Netherlands.
* Nicolas Regnauld, Esri, France
* Timofey Samsonov, Lomonosov Moscow State University, Russia
* Lawrence V. Stanislawski, USGS, USA
* Guillaume Touya, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.


## Scientific Committee

* Pia Bereuter, FHNW University of Applied Sciences of Northwestern Switzerland.
* Azelle Courtial, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.
* Izabela Karsznia, University of Warsaw, Poland.
* Martijn Meijers, TU Delft, The Netherlands.
* Paulo Raposo, Twente University, The Netherlands.
* Nicolas Regnauld, Esri, France.
* Timofey Samsonov, Lomonosov Moscow State University, Russia.
* Lawrence V. Stanislawski, USGS, USA.
* Guillaume Touya, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.



[1]: https://easychair.org/conferences/?conf=icamapgen23
[2]: http://www.icc2019.org/data/ica-abstract_teplate.docx
[3]: https://zenodo.org/
[4]: https://agile-online.org/conference-2023
[5]: https://teams.microsoft.com/l/meetup-join/19%3ameeting_N2NkODM4Y2MtODg0MS00OGMxLWFjYWYtN2VjYTUwODcxOWVh%40thread.v2/0?context=%7b%22Tid%22%3a%223876b373-7e2c-4857-b024-53326b5b4bb2%22%2c%22Oid%22%3a%2284f74161-e3d4-4994-b462-2678bc7ab225%22%7d
[6]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_1.pdf
[7]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_2.pdf
[8]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_3.pdf
[9]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_4.pdf
[10]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_5.pdf
[11]: https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2023/icamapgen23_paper_6.pdf
