---
  layout: page
---

9th ICA Workshop - Papers and program
-------------------------------------

Find program as PDF file [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/program2005.pdf).

### Thursday, 7th, July

9h00-9h15 - Welcome

9h15-11h00 - Session 1 : National mapping Agencies - Results, Projects, Requirements

B. Baella, M. Pla (ICC, Spain), [*Reorganizing the Topographic Databases of the Institut Cartogràphic de Catalunya Applying Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Baella_Pla.pdf)

A. Féchir, J. De Waele (IGN, Belgium), [*The Future Production of Generalised Maps at IGN Belgium*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Fechir_DeWaele.pdf)

F. Lecordix, Y. Jahard, C. Lemarié, E. Hauboin (IGN, France), [*The end of Carto 2001 Project: Top100 based on BDCarto® database*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Lecordix_Jahard_et_al_.pdf) (warning 35 MB)

F. Lecordix (IGN,France) , N. Regnauld (OS,UK), M. Meyer (KMS,Denmark), A. Féchir (IGN,Belgium), [*MAGNET Consortium*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Lecordix_Regnauld_et_al.pdf)

J. Stöter (ITC, Netherlands), [*Generalisation: the gap between research and practice*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Stoter.pdf)

11h00-11h30 - Coffee break
11h30-12h30 - Discussions in small groups
12h30-13h00 - Synthesis of discussions in small groups
13h00-14h30 - LUNCH

14h30-16h15 – Session 2 : Platform, Architecture, Modelling

M. Sester (University of Hannover) <span style="color:red">Invited presentation</span>, *Overview of the GiMoDig Project*

E. Bernier, Y. Bédard, F. Hubert (CRG, Université Laval, Québec), [*UMapIT: An On-Demand Web Mapping Tool Based on a Multiple Representation Database*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Bernier_et_al.pdf)

M. Bozkurt, R. Groth, B. Hansson, L. Harrie. P. Ringberg, H. Stigmar, K. Torpel (Lund university and
SonyEricsson, Sweden), [*Towards Extending Web Map Services for Mobile Applications*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Bozkurt_et_al.pdf)

M. Neun, D. Burghardt (Univ. of Zurich, Switzerland), [*Web Services for an Open Generalisation Research Platform*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Neun_Burghardt.pdf)

C. Duchêne, M. Dadou, A. Ruas (IGN, France), [*Helping the Capture and Analysis of Expert Knowledge to support Generalisation*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Duchene_et_al.pdf)

16h15-16h45 - Coffee break

16h45-17h30 – Session 3 : GIS vendors

V. Smith (Intergraph, USA), [*Demonstrable Interoperability of Agent-based Generalization with Open, Geospatial Clients*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Smith.pdf)

P. Hardy, D. Lee (ESRI, USA), [*Multiple Representations with Overrides, and their relationship to DLM/DCM Generalization*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Hardy_Lee.pdf)

17h30-18h30 – Brainstorming in small groups: semantic network of knowledge related to
generalisation

18h30-19h00 – Synthesis and analysis of brainstorming

### Friday, 8th, July

9h30-10h00 – ICA Commission: presentation of activities

10h00-11h10 – Session 4: Generalisation processes, operations and algorithms (1)

J.T. Bjørke (FFI and UMB, Norway), <span style="color:red">Invited presentation</span>, *Information Theory for Selection*

S. Thom (OS, UK), [*A Strategy for Collapsing OS Integrated Transport Network™ dual carriageways*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Thom.pdf)

M. Galanda, R. Koehnen, J. Schroeder, R. McMaster (Univ. of Minnesota, USA), [*Automated Generalization of Historical U.S. Census Unit*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Galanda_et_al.pdf)

11h10-11h40 - Coffee break

11h40-12h40 – Session 5: Generalisation processes, operations and algorithms (2)

O. Chaudhry, W. Mackaness (Univ. of Edinburgh, Scotland), [*Visualisation of Settlements Over Large Changes in Scale*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Chaudhry_Mackaness.pdf)

P. Revell (OS, UK), [*Seeing the Wood from the Trees: Generalising OS MasterMap® Tree Coverage Polygons to Woodland at 1:50 000 Scale*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Revell.pdf)

K. Zaksek, T. Podobnikar (Institute for Anthropological and Spatial Studies, Slovenia), [*An Effective DEM Generalization with Basic GIS Operations*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Zaksek_Podobnikar.pdf)

12h40-14h30 - LUNCH

14h30-16h15 – Session 6: Data Analysis, Enrichment and Matching

J.-H.Haunert (Univ. of Hannover, Germany), [*Link based Conflation of Geographic Datasets*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Haunert.pdf)

M. Zhang, W. Shi, L. Meng (Univ. of Munich, Germany), [*A generic matching algorithm for line networks of different resolutions*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Zhang_et_al.pdf)

A. Edwardes (Univ. of Zurich, Switzerland), [*Modelling space for cartometric analysis: a grid-based approach*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Edwardes.pdf)

J. Gaffuri (IGN, France), [*Toward a taken into account of the “background themes” in a multi-agent generalisation process*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Gaffuri.pdf)

S. Steiniger, R. Weibel (Univ. of Zurich, Switzerland), [*Relations and Structures in Categorical Maps*](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2005/Steiniger_Weibel.pdf)

16h15-16h45 - Coffee break
16h45-17h45 – Discussions in small groups
17h45-18h15 – Synthesis of discussions
18h15-18h45 – Synthesis of workshop, future events…
