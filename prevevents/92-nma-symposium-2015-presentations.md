---
  layout: page
---

NMA Symposium 2015 - Program and presentations
----------------------------------------------

[TABLE]

Day 1
-----

09:00-09:30 Introduction session: welcome and introduction of participants

09:30 Swisstopo, Switzerland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/20151203_ext_abstract_swisstopo.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/20151203_ICA-EuroSDR-NMA_swisstopo.pdf)[](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/20151203_ext_abstract_swisstopo.pdf)
09:50 CZE, Czech Republic [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/czech_republic.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/CZE_National_Mapping.pdf)
10:10 NLS, Finland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Finland.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/EuroSDR_NTDB_generalisation_Finland.pdf)

**10:30-11:00 Coffee break**

11:00 IGN, France [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/France_workshop_ICA_Amsterdam.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Amsterdam_NMA_Meeting_IGNFrance.pdf)
11:20 Ordnance Survey, UK [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/OSGB_-_Presentation_Abstract.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/OSGB_-_2nd_ICA_EuroSDR_NMA_Symposium_20151119.pdf)
11:40 Kadaster, Netherlands [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Netherlands.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ICA_EuroSDR_final_Kadaster.pdf)
12:00 ICG, Catalonia [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ICGC_Generalization_Amsterdam2015_Abstract.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ICGC_Generalization_Amsterdam_20151126.pdf)
12:20 GST (KMS), Denmark [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/DenmarkAbstract-DanishGeodataAgency.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Denmark.pdf)

**12:40-13:30 Lunch**

13:30-15:00 Break out session [\[break out 1\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/BreakoutSession1.pdf) [\[break out 2\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/20151203_1502421.jpg) [\[break out 3\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Breakout3.pdf)

**15:00- 15:30 Coffee break**

15:30 GUGiK, Poland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Poland_abstractOfpresentation_-_GUGiK.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ICAEuroSDR_NMA_workshop_34_december_2015_Amsterdam_1_Poland.pdf)
15:50 LM, Sweden [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Sweden_Abstract_NMA_Workshop_Amsterdam_Dec_2015.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Presentation_Sweden_Amsterdam.pdf)
16:10 ELF [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ELF_MultiscaleApproach.pdf) [\[link\]](http://www.elfproject.eu/)
16:30 Ordnance Survey, Ireland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/OrdnanceSurveyIreland_summary.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/MRDS_EroSdr_meeting31015_Final.pdf)

Day 2
-----

8:30-9:00 Recap – Summary of day 1

9:00   Survey of Israel [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/israel.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/slideshow_Israel.pdf)
9:15   HGK, Turkey \[abstract\] [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/ICA_EuroSDR_NMASyposium2015_Turkey.pdf)
9:30   Kartverket, Norway [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Norway.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/EuroSDR_Amsterdam_Norway.pdf)
9:45   IGN, Belgium [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Belgium.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/MRDB2015_Belgium.pdf)
10:00 AdV, Germany \[abstract\] [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/LGL_Baden_Wuerttemberg.pdf)

10:15 ESRI [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Esri.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/Esri_201412_ArcGIS_MultiScale.pdf)
10:30 1Spatial [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/1Spatial_Introduction.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2015/1Spatial_1Generalise_EuroSDR_ICA_Dec2015.pdf)

**10:45-11:15 Coffee break**

11:15 What’s next?
11:30-12:00 Prestentation of map products
