---
  layout: page
---

12th ICA Workshop - Papers and presentations
--------------------------------------------

### Industry platforms and production lines

[Practical Research in Generalization of European National Framework Data From 1:10k to 1:50k, Exercising and Extending an Industry-Standard GIS](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/19_Hardy_et_al.pdf)
P. Hardy (ESRI), D. Lee, J. van Smaalen - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/19pres.pdf)

[Applied Generalization & MRDB for Mapping Agencies using Open, Geospatial Clients](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/30_Smith.pdf) 
V. Smith (Intergraph) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/30pres.pdf)

[Advances of 1Spatial in generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/31_Warner.pdf) 
G. Stickler (1Spatial) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/31pres.pdf)

[A Review of the Clarity Generalisation Platform and the Customisations Developed at Ordnance Survey Research](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/17_Revell.pdf)
P. Revell (Ordnance Survey) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/17pres.pdf)

### Generalisation of networks

[User Centric Mapping for Car Navigation Systems](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/02_Dogru_et_al.pdf)
A.O. Dogru (Istambul Technical University), C. Duchêne, S. Mustière, N.N. Ulugtekin - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/02pres.pdf)

[Development of a Knowledge-Based Network Pruning Strategy for Automated Generalisation of the United States National Hydrography Dataset](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/23_Stanislawski.pdf)
L. Stanislawski (USGS) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/23pres.pdf)

### Orchestration of the generalisation process

[Object-field relationships modelling in an agent-based generalisation model](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/16_Gaffuri_et_al.pdf)
J. Gaffuri (IGN France), C. Duchêne, A. Ruas - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/16pres.pdf)

[Partitioning Techniques to Make Manageable the Generalisation of National Spatial Datasets](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/21_Chaudhry_Mackaness.pdf)
O.Z. Chaudry, W.A. Mackaness(University of Edinburgh) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/21pres.pdf)

[First Thoughts for the Orchestration of Generalisation Methods on Heterogeneous Landscapes](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/01_Touya.pdf)
G. Touya (IGN France) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/01pres.pdf)

[Towards an Interoperable Web Generalisation Services Framework – Current Work in Progress](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/12_foerster_et_al.pdf)
T. Foerster (ITC), D. Burghardt, M. Neun, N. Regnauld, J. Swan, R. Weibel - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/12pres.pdf)

### Managing multiple representations

[Towards a Data Model for Update Propagation in MR-DLM](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/07_Zhou_et_al.pdf)
S. Zhou (Ordnance Survey), N. Regnauld, C. Roensdorf - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/07pres.pdf)

[Using the constrained tGAP for generalisation of IMGeo to Top10NL model](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/27_Hofman_et_al.pdf)
A. Hofman (Logica Nederland), P. Dilo, P. van Oosterom, N. Borkens - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/27pres.pdf)

### International surveys

[A study on the state-of-the-art in automated map generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/13_Stoter_et_al.pdf)
J. Stoter, C. Duchêne, G. Touya, B. Baella, M. Pla, P.Rosenstand, N. Regnauld (Ordnance Survey), H. Uitermark, D. Burghardt, S. Schmid, K.-H. Anders, F. Dávila - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/13pres.pdf)

[Generalisation operators for practice - a survey at national mapping agencies](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/11_foerster_stoter.pdf)
T. Foerster (ITC), J. Stoter - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/11pres.pdf)

### Evaluation and data enrichment

[Methodologies for the evaluation of generalised data derived with commercial available generalisation systems](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/28_Burghardt_et_al.pdf)
D. Burghardt (University of Zurich), S. Schmid, C. Duchene, J. Stoter, B. Baelaa, N. Regnauld, G. Touya - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/28pres.pdf)

[Usability Testing of Legibility Constraints](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/03_Stigmar_Harrie.pdf)
H. Stigmar (Lund University, NLS), L. Harrie - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/03pres.pdf)

[Machine-based Interpretation of Map Requirements for Automated Evaluation of Generalized Data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/24_Zhang_et_al.pdf)
X. Zhang (ITC, Wuhan University), J. Stoter, T. Ai - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/24pres.pdf)

[Alternative options of using processing knowledge to populate ontologies for the recognition of urban concepts](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/22_Luescher.pdf)
P. Luscher (University of Zurich), R. Weibel, D. Burghardt - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/22pres.pdf)

### Generalisation operators

[A transition from simplification to generalisation of natural occurring lines](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/25_Nakos_et_al.pdf)
B. Nakos (NTU Athen), J. Gaffuri, S. Mustière - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/25pres.pdf)

[High Quality Building Generalization by Extending the Morphological Operators](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/04_Damen_et_al.pdf)
J. Damen, M. van Kreveld (Utrecht University), B. Spaan - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/04pres.pdf)

[Automated Cell Based Generalization of Virtual 3D City Models with Dynamic Landmark Highlighting](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/29_Glander_Dollner.pdf)
T. Glander (University of Potsdam), J. Dollner - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2008/29pres.pdf)
