---
  layout: page
---

ICA / EuroSDR NMA Symposium 2013
================================

[TABLE]

 

 

 

 

 

A NMA Symposium, co-organised with the EuroSDR Commission on Data Specifications, is planned on the topic **“Designing MRDB and multi-scale DCMs: sharing experience between governmental mapping agencies”**.

The symposium will take place from

 

*21-22 March 2013, in Barcelona, Spain *

**hosted by the** Institut Cartografic de Catalunya**

 

During the symposium participants will get the opportunity to present production workflows and solutions based on the utilisation of MRDB. Besides the prepared presentations of the participants, time will be kept for formal and informal discussions around the current practices and used tools/software products.

In addition common needs and challenges will be identified that could be passed to industrials on the one hand, researchers on the other hand. Finally the role of INSPIRE will be discussed with respect to the topic of MRDB and multi-scale DCMs.

 

Besides other the following governmental mapping agencies/cartographic institutions will be present:

-   ADV Project Germany [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/ADV_project.docx)
-   ICC
-   IGN-Belgium [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Belgium_MRDB_Abstract_IGNB.docx)
-   IGN-France [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Abstract-workshopICA-Barcelona-IGNFrance.pdf)
-   IGN-Spain [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/IGN_Madrid.docx)
-   JRC
-   Kadaster NL [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/NL_DLMDCM.docx)
-   KMS-Denmark
-   NLS-Finland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Finland.docx)
-   OSGB [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/OS_Presentation-outline-Barcelona2013.docx)
-   OSI-Ireland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/OSI.pptx)
-   swisstopo [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Swisstopo.docx)
-   USGS [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/ICA_EuroSDR_Workshop_StanislawskiBrewerButtenfield_fin.docx)

  (Download all documents as zip file: [symposium2013.zip](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/symposium2013.zip))

Organising Committee

Cecile Duchene, Jantien Stoter, Maria Pla & Blanca Baella
