---
  layout: page
---

10th ICA Workshop - Participants
--------------------------------

![group2006part](prevevents/images/images/Events/group2006part.jpg)

Brewer Cynthia (University of Pennsylvania -- USA)
Buttenfield Barbara (University of Colorado -- USA)
Cetinkaya Bulent (NMA : GCM -- Turkey)
Chaudhry Omair (University of Edinburgh -- UK)
Dutton Geoff (Industry -- USA)
Foerster Theodor (University : ITC -- NL)
Gaffuri, Julien (Research lab : IGN-COGIT -- France)
Gotlib Dariusz (University of Varsovia -- PL)
Hardy Paul (Industry : ESRI -- USA)
Lecordix Francois (NMA : IGN -- France)
Lee Dan (Industry : ESRI -- USA)
Mackaness William (University of Edinburgh -- UK)
Olszewski Robert (University of Varsovia -- PL)
von Oosterom Peter (University of Delft -- NL)
Petzold Ingo (University : GIUZ/Zurich -- Switzerland)
Revell Patrick (NMA : OS -- UK)
Rosenstand Peter (NMA: KMS -- Denmark)
Ruas Anne (Research lab : IGN-COGIT -- France)
Stanek Karel (University : LGC-Brno -- Czeck Republic)
Valdeperez Pastor Magali (NMA : IGN -- SPAIN)
Varanka Dalia (NMA : USGS -- USA)
