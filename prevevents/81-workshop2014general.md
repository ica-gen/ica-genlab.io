---
  layout: page
---

17th ICA Workshop - General information
---------------------------------------

### with special focus on
Integration, Filtering and Abstraction of User Generated or Multi-Source Spatial Content

The availability of user generated spatial-temporal content has greatly increased within the past years through the popularity of web based technologies, mobile applications and social networks. This offers great potential for a large number of spatial-temporal applications such as crisis mapping, monitoring of epidemics, mobility management, emotional mapping, thematic navigation, landscape planning, etc. The integration of multi-source VGI data as well as the combination with authoritative data provides promising opportunities but also poses new challenges due to data streaming, heterogeneity, differences in spatial granularity and semantic description.

The ICA Commission on Generalisation and Multiple Representation will organise the one day workshop in

***« Vienna, Austria, 23rd September 2014 »***

in advance of GIScience Conference 2014. The workshop will be hosted by the Austrian Federal Office for Metrology and Surveying.

We encourage participant to submit short research or position papers in advance. Depending on the quality of the paper it is planned to publish the revised and extended versions in a common journal issue of Journal of Spatial Information Science (JOSIS).

##### **Key dates:**

-   16 May 2014 deadline for submission of short papers (1500 words) to a review panel
-   27 June acceptance / rejection notification
-   **1 Sep deadline for revised submission of paper (up to 3000 words)**

&nbsp;
-   16 Oct - following the workshop, invitation to submit to special issue
-   16 Jan 2015 - deadline for 6000 word papers.

Submission and review of extended abstract will be managed through EasyChair
(an online conference management system):
https://www.easychair.org/conferences/?conf=genemr2014

##### **Location:**

The workshop will be held at

Austrian Federal Office for Metrology and Surveying
Schiffamtsgasse 1-3
A-1020 Vienna

##### How to get there:

from Vienna International Airport
S-Bahn Train to Wien Mitte (3,60 EUR), [www.oebb.at](http://www.oebb.at/en/index.jsp)
City Airport Train CAT to Wien Mitte (9,00 EUR), [www.cityairporttrain.at](http://www.cityairporttrain.com/default.aspx?lang=en-US)
Vienna AirportLines Bus to Wien Südbahnhof (6,00 EUR), [www.viennaairportlines.at](http://www.postbus.at/en/index.jsp)
Taxi (starting at 25,00 EUR)

from Wien Mitte/Landstraße
Metro U4 (direction Heiligenstadt) to Schottenring (Exit: Herminengasse)

from Wien Westbahnhof train station
Metro U3 (direction Simmering) to Volkstheater change to
Metro U2 (direction Aspern) to Schottenring (Exit: Herminengasse)

from Wien Südbahnhof (Ost) train station
Tram line D (direction Nussdorf) to Schottentor change to Metro U2 (direction Aspern) to Schottenring (Exit: Herminengasse)

Public Transport in Vienna: [www.wienerlinien.at](http://www.wienerlinien.at/eportal2/ep/tab.do?tabId=0)

##### Format:

Presentations will be relatively short (10-15') to leave place for questions and discussions.
Time will also be kept for discussions in small groups and open problem solving.

The aim of the workshop is to present and discuss approaches on multi-source data integration, information filtering and clutter reduction as well as automated generalisation within the context of on-demand and multi-scale mapping.

##### **Topics of interest:**

-   Integration of spatial and temporal information from different VGI platforms
-   Real-time generalisation and visualisation of data streams
-   Multi-scale modelling and representation of user generated content
-   Continuous zooming and vario-scale representations
-   Generalisation for on-demand mapping
-   Clutter reduction methods
-   Development and utilisation of Web Processing Services and Web Generalisation Services
-   Automated generalisation of OpenStreetMap data
-   Derivation of schematised maps
-   Real-time label placement and labeling through scale

##### **Scientific workshop organisers & p**rogram committee**:**

Dirk Burghardt, Cecile Duchêne, William Mackaness

Blanca Baella, Sandrine Balley, Dirk Burghardt, Cecile Duchene, Hongchao Fanfan, Julien Gaffuri, Lars Harrie, Jan Haunert, William Mackaness, Martijn Meijers, Liqiu Meng, Sebastien Mustiere, Robert Olszewski, Maria Pla, Nicolas Regnauld, Sandro Savino, Monika Sester, Lawrence Stanislawski, Jantien Stoter, Guillaume Touya, Peter Van Oosterom, Xiang Zhang
