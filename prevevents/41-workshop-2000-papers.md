---
  layout: page
---

4th ICA Workshop - Papers
-------------------------

### Discussions

Single Detailed Database VS Multiple Scale Databases for Map Production, Database Update VS Map Update ([Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/garriga.pdf "Report"))
On demand mapping ([Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/hyatt.pdf "Report"))
Maps for navigation ([Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/douglas_seeley.pdf "Report"))

### Papers and presentations

Robert Weibel, University of Zurich, Switzerland
*Challenges for Generalization and Web Mapping*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/weibel.pdf)

Paul Hardy and Kelvin Haire, Laser-Scan Ltd., Cambridge UK
*Generalization, Web Mapping and Data Delivery over the Internet*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/hardy.pdf)

Dan Lee and Roland Hansson, ESRI Redlands, USA
*Integration of Generalization and Text Placement in ArcGIS*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/lee.pdf)

Henry L. Jackson, Intergraph, Huntsville, USA
*Creating and Generalizing Linear Networks*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/jackson.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/jackson.pdf)

Sjef van der Steen, ITC Enschede, The Netherlands
*Publishing on Demand, Technical Possibilities and Limitations*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/steen.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/steen.pdf)

Joan Romeu, ICC
*Technical Advances in Digital Cartography at the Institut Cartogràfic de Catalunya*

Liqiu Meng, Technical University of Munich, Germany
*ATKIS - Model Generalization and On-demand Cartographic Visualization*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/meng.pdf)

Anne Ruas, Institut Géographique National, St-Mandé, France
*Tests of Generalization Software by the OEEPE Working Group on Generalization*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/ruas_op.pdf)

Peter Højholt, KMS, København, Denmark
*Integration of Generalization into Map Production: Putting it All Together*

François Lecordix, Institut Géographique National, St-Mandé, France
*Project Carto2001, A Cartographic Space Odyssey*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/lecordix.pdf)

Matthias Ekkehard, City of Hamburg, Germany
*Maps as an Important Part of Internet Presentation of a Large City*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/ekkehard.pdf)

Anne Ruas, Institut Géographique National, St-Mandé, France
*Project AGENT: Overview and Results of a European R and D Project in Map Generalization*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2000/presentations/ruas_agent.pdf)
