---
  layout: page
---

12th ICA Workshop - General information
---------------------------------------

[TABLE]

This workshop has been organised jointly between the [International Cartographic Association](http://www.icaci.org/), [EuroSDR](http://www.eurosdr.net/) and the [Dutch program RGI](http://www.rgi.nl/). It has been hosted at [LIRMM](http://www.lirmm.fr/), in Montpellier, France, on 2008, 20-21 June.

### Presentations

Presentations have been organised in 7 sessions: 

-   Industry platforms and generalisation production lines, 
-   Generalisation of networks, 
-   Evaluation and data enrichment,
-   Large-Scale International Projects,
-   Managing multiple-representation,
-   Orchestration of the generalisation process,
-   Generalisation algorithms,

Papers and presentations can be downloaded from [here](64-workshop-2008-papers-and-presentations.md)

### Discussions

6 discussion sessions have been organsied around the following topics: 
- Generalisation Web Services (chair: Nico Regnauld) 
- Formalising map specifications / defining user requirements (chair: William Mackaness) 
- Generalisation for 3D man made objects (chair: Tassilo Glander) 
- Updating and generalisation / Updating and Multi-Rep (chair: Peter van Oosterom) 
- Generalisation in thematic mapping (chair: Cécile Duchêne) 
- Ways forward in contextual generalisation (chair: Robert Weibel) 

### Attendees

Many thanks to all the 38 attendees for this very dynamic event.
The full list of attendees can be seen [here](63-workshop-2008-participants.md)

### Committees

Organising committee: William Mackaness, Sebastien Mustiere, Peter van Oosterom, Jantien Stoter

Reviewing committee: Karl-Henrich Anders, Cindy Brewer, Dirk Burghardt, Cécile Duchêne, Jan-Henrik Haunert, Liqiu Meng, Keith Murray, Nicolas Regnauld, Anne Ruas, Tiina Sarjakoski, Monika Sester, Katalin Toth, Christelle Vangenot, Robert Weibel.
