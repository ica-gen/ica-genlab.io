---
  layout: page
---

21th ICA Workshop - General information
---------------------------------------

![ica logo commission](/images/logos/ica_logo_commission.png)![Agile](/images/logos/Agile.png)![GDR MAGIS](/images/logos/logo-magis-xs.png)![CNRS](/images/logos/cnrs.png)

Map Generalisation Practice with Volunteered Geographic Information
----------------------------------------------

The 21th workshop of the ICA Commission on Generalisation and Multiple Representation was carried out as one day pre-conference workshop prior to the AGILE conference. It was jointly organized with French [GDR MAGIS CNRS][1].

 

*Lund, Sweden, 12 June, 2018*


Map generalisation research mostly focused on the needs of national mapping agencies (NMAs): making maps at small scales from high resolution geographic databases. Given the popularity of independent Volunteered Geographic Information (VGI) platforms such as OpenStreetMap, as well as the use of crowdsourced data within NMAs, a focus on the generalisation of VGI is needed. Unlike NMA datasets, VGI can be very diverse and heterogeneous, and thus poses real, novel challenges for data management and processing.
The workshop is co-organized with a French research group on Volunteered Geographic Information chaired by Cyril de Runz (University of Reims).
This will be a hackathon-like workshop: two datasets from OSM and FlickR will be provided, and participants will be asked to submit how they successfully (or unsuccessfully) generalise part of (or the whole) dataset. Participants will be allowed to use other VGI datasets around Lund if they prefer. The submission topics can be:
- methods/algorithms to generalise VGI
- software to generalise VGI
- theoretical findings on VGI generalisation
- scale/level of detail issues in VGI
- identification of specific data types that require new approaches to generalisation

##### Program

| Time           | Event                              |
| -------------- | ---------------------------------- |
| 9:00  | Workshop Introduction                       |
| 9:15  | Presentation Session - *10 min talk & 10 min questions per talk*   |
|       |  1. *Generalizing OpenRailwayMap to 1:10k and 1:50k* - J. Gaffuri [map](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/railway_gene_tests_gisco.zip), [paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/ICA_Workshop_2018_railway_gaffuri.pdf)|
|       |  2. *A Template Matching Method for Enhancing and Generalizing OpenStreetMap Building Data* - X. Yan [map and paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/ai_et_al_apper_and_map.pdf)|
|       |  3. *The Spatial Distribution of Photo Locations of Locals and Tourists in Lund* - T. Grundemann  [map](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/Map_Grundemann.pdf), [paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/Description_Grundemann.pdf)   |
|       |  4. *User Interest in Lund using VGI Data Density Analysis* - X. Wang [map](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/Lundmap_wang.jpeg),  [paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/lund.osm) |
| 11:00 | Coffee Break with other AGILE Workshops                         |
| 11:30 | Demo Session & Discussion                                       |
| 12:30 | **Keynote Presentation** by **Lars Harrie** of Lund University  |
| 12:55 | Concluding Remarks                                              |
| 13:00 | Lunch                                                           |


On the afternoon, all the participants were invited to the [VGI-Alive 2018 workshop][2] organized by Peter Mooney.


<img src="/images/images/Events/group2018.jpg" width="700" height="350" />



##### Submission
This workshop is not a usual one and the main item of the submission will be **a map, generalized from the provided datasets**. The map can be submitted as an image or a link to a web map. The map should be submitted with a short description of the content and the process to derive the map.

It will be possible to submit a **two-page abstract** that describes the generalization method used and the remaining problems to improve the map. The abstracts should use the following template ([download template](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/workshop2017/ICA_Workshop_Template_2017.docx)).

It will also be possible to submit **the generalized dataset** as a benchmark. It will be made available with a DOI, following the standards of open science.
Finally, the accepted submissions will be presented during the workshop.

##### Important Dates
- submission deadline : ~~23 March 2018~~ extended to ~~6 April 2018~~ 23 April 2018
- Notification acceptance : ~~5~~ 9 April 2018
- AGILE Early registration ends: 15 April 2018
- Workshop date : 12 June 2018


#####  Datasets:

The test area covers a region around Lund with the following Lat/Lon coordinates: top=55.7519 left=13.0878 bottom=55.6543 right=13.3144

The OpenStreetMap dataset is available with two formats: [raw .osm file](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/lund.osm) and [shapefiles](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/lund_osm_shp.zip)

The FlickR dataset from the same region is available in .csv and shapefiles in [this zip archive](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2018/LUND_Flickr_Data_CSV+SHP.zip).

#####  Scientific workshop organisers & program committee:

Pia Bereuter, Cyril de Runz, Paulo Raposo, Guillaume Touya

[1]: http://apvgi-magis.ign.fr/
[2]: http://www.cs.nuim.ie/~pmooney/vgi-alive2018/
