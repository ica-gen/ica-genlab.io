---
layout: page
---

20th ICA Workshop on Generalisation and Multiple Representation
===============================================================

**8.30 -  9.00 Registration**

**9.00 -  9.10 Opening and welcome**

**9:10 - 10:30 Session I: Generalisation of heterogeneous data and conflation**

*   Cécile Duchêne, Sébastien Mustière, Sandrine Gomes, Mathilde Kremp, Lucille Billon and Romain Sordello: [Mapping heterogeneous data: a case study on the French Green Infrastructure](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_2.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Duchene_WashingtonWorkshop.pdf))
*   Nicolas Regnauld and Derek Howland: [Rule based quality control for automated generalisation and conflation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_8.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Regnauld_WashingtonWorkshop.pdf))
*   Rose Nyberg: [Automatic placement of text for objects that do not have a clear geographic definition](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_6.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Nyberg_WashingtonWorkshop.pdf))
*   Tomaž Podobnikar: [Semantic generalization of the multi-source digital elevation model (DEM)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_3.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Podobnikar_WashingtonWorkshop.pdf))

**10:30 - 11:00 Coffee break**

**11:00 - 12:00 Session II: Pattern aware generalisation**

*   Guillaume Touya and Marion Dumont: [Progressive Block Graying and Landmarks Enhancing as Intermediate Representations between Buildings and Urban Areas](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_1.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Touya_WashingtonWorkshop.pdf))
*   Xiao Wang and Dirk Burghardt: [A Stroke-based Approach to Detect Building Pattern for Generalization Purposes](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_5.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Wang_WashingtonWorkshop.pdf))
*   Lawrence Stanislawski and Barbara Buttenfield: [Preserving Line Sinuosity in Hydrographic Feature Simplification](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_9.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Stanislawski_WashingtonWorkshop.pdf))

**12:00 - 13:30 Lunch**

**13:30 - 15:00 Demos, Map products, Poster presentation and Break-out session**

**15:00 - 15:30 Coffee break**

**15:30 - 16:30 Session III: Generalisation applied to social media data andmaps on mobile devices**

*   Lucas Godfrey and William Mackaness: [Continuous cartography: generalisation for mixed-scale and mixed-space map views](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_10.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Godfrey_WashingtonWorkshop.pdf))
*   Dirk Burghardt, Alexander Dunkel and Mathias Gröbe: [Generalisation and Multiple Representation of Location-Based Social Media Data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_7.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Burghardt_WashingtonWorkshop.pdf))
*   Barry Kronenfeld: [Twitter Wars! Social Media Mapping in the Classroom](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/genemr2017_paper_4.pdf) ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Kronfeld_WashingtonWorkshop.pdf))

**16:30 – 17:00 Reports from [Break-out-Session](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2017/Break-out-session.pdf), next steps, wrap up**
