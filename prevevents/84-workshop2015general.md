---
  layout: page
---

18th ICA Workshop - General information
---------------------------------------

 

![isprs logo](prevevents/images/logos/isprs_logo.gif)![ica logo commission](prevevents/images/logos/ica_logo_commission.png)

*ICA-ISPRS Workshop on
Generalisation and Multiple Representation*
-------------------------------------------

*In the tradition of pre-conference workshops in association with the International Cartographic Conference (ICC) the ICA Commission on Generalisation and Multiple Representation and the ISPRS WG II/2 "Multi n-dimensional Spatial Data Representations, Data Structures and Algorithms" will jointly organise a workshop in*

* 
*

*Rio de Janeiro, Brazil*, *21 August, 2015*

 

The workshop will be supported by the Cartographic Engineering Department of State University of Rio de Janeiro (UERJ). For the ICA Commission on Generalisation and Multiple Representation it will be the 18th ICA Generalisation Workshop. Participants of the workshop are invited to submit research papers or position papers.

### Key dates:

-   15 April: deadline for submission of short papers (limit 3000 words, and 8 pages including figures)
-   12 May: notification of acceptance of paper for presentation (& invitation)
-   31 July: deadline for submission of revised papers (participants will have around three weeks to read papers prior to the workshop)
-   21 August: Workshop

Submission and review of short papers will be managed through EasyChair (an online conference management system):

<https://www.easychair.org/conferences/?conf=genemr2015>

### Location and registration:

The workshop will be held at State University of Rio de Janeiro - UERJ (near Maracanã Arena, via Metro: Line 2 - Station "Maracanã").
Room for presentations: Auditorium 53 - Building F (Bloco F) - Fifth floor.

[![UERJ Campus](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2015/UERJ_Campus.jpg)](https://www.google.de/maps/place/Universidade+do+Estado+do+Rio+de+Janeiro/@-22.9111615,-43.2338655,1114m/data=!3m1!1e3!4m2!3m1!1s0x0000000000000000:0x874ed5a98cf472d1!6m1!1e1)
(Click on the map for navigation via Google Maps)

Participants must register by e-mail to dirk.burghardt(at)tu-dresden.de. Participation fee will be 40 $ (~ 140 BRL) and has to be paid at the beginning of the workshop.

### Topics of interest: 

Besides submissions related to research interests of NMAs, vendors and practitioners, the organisers particularly welcome papers that address the topics listed below, which cover research themes that the organising Commissions are is particularly interested in exploring further. Please note that for any of these topics, on top of paper submissions the organisers welcome demonstrations that illustrate the utility of systems, or that demonstrate the handling of large volumes of geographic data.

-   Map Specifications and User Requirements
-   Modelling Geographic Relationships in Automated Environments
-   Generalisation Ontologies
-   Development of Generalisation Algorithms
-   Generalisation Process Modelling
-   Generalisation in Support of Data Integration in particular of User Generated or Multi-Source Spatial Content
-   Terrain Generalisation
-   3D-Generalisation
-   Continuous Generalisation
-   Evaluation and Quality in Generalisation
-   Generalisation in Practice

##### **Scientific workshop organisers & p**rogram committee**:**

Dirk Burghardt, Eric Guilbert

Barbara Buttenfield
Cecile Duchene
Hongchao Fanfan
Nicholas Gould
Lars Harrie
William Mackaness
Martijn Meijers
Marc Post
Nicolas Regnauld
Andreas Reimer
Sandro Savino
Monika Sester
Stefan Steiniger
Jantien Stoter
Radan Suba
Vincent van Altena
Peter van Oosterom

 

Eric Guilbert
