---
  layout: page
---

2nd ICA Workshop - Papers and program
-------------------------------------

### S1: Map Production Systems -- Critique of the State of the Art

Dan Lee
*Establish the Rules, Integrate the Tools, and Approach the Reality of Generalization*

María Pla and Blanca Baella
*Use of Generalization Tools at the Institut Cartogràfic de Catalunya*

Nico J. Bakker
*Generalization in Practice with Topographical Databases in the Netherlands*

### S2: Knowledge Acquisition and Encoding

Geoffrey Edwards
*Geocognostics and Map Generalisation*

Barbara Buttenfield and Ming-Hsiang Tsu
*Encapsulated Operators for Processing Geographic Information*

Jan Neumann
*Reengineering Generalization: A Tool Solving its Cognitive Function Crisis*

Zhen Tian
*Cartographic Generalization: Its Way of Thinking and Practice*

Agnes Gryl and Geoffrey Edwards
*Description in Natural Language and Cartographic Generalization: Similar Functionalities?*

### S3: Modelling Semantics and Non-Spatial Structure

John van Smaalen
*Class Aggregation in Database Generalization*

Jan Terje Bjørke
*Positional Entropy Based Map Generalization*

Francis Harvey
*A Semantical Framework for the Geometric Matching of Geographic Information*

Dietrich Schürer
*Model Generalization in Digital Systems*

Ingrid Jaquemotte
*Methods of Raster Graphics for Partial Tasks of Map Generalization \[abstract only\]*

### S4: Modelling Geometric Spatial Structure

Allan Brown
*Spatial Data Generalization by means of Georeferenced Square Tiling*

Zhilin Li
*Philosophical, Conceptual and Algorithmic Issues in Automated Map Generalization*

Geoffrey Dutton
*Using a Global Hierarchical Coordinate System for Generalization of Vector Map Data*

Dianne E. Richardson
*Current Development Acitivities and Interests in Automated and Semi-Automated Generalization at CCRS*

Bo Su
*A Methodology for Building Models for Generalization Operations Using Mathematical Morphology \[abstract only\]*

Weiping Yang
*Spatial Database and Dynamic Object Generalization Coupled with a Map Agent \[abstract only\]*

### S5: Quality -- Assessment and Constraints

Thomas Wilke
*Quality Measures for Generalized Terrain Models*

Byron Nakos
*Fractal Geometry Theory in Performing Automated Map Generalization Operations*

Olli Jaakkola
*Automatic Generalization of Categorial Coverages*

Robert B. McMaster and Howard Veregin
*Visualizing Cartographic Generalization*

Jean-Philippe Lagrange
*Analysis of Constraints and of Their Relationship with Generalization Process Management*

Frank Brazile
*A Proposal for Quality Evaluation in Generalization \[abstract only\]*

### S6: Techniques in Conflict Detection and Resolution

Peter Højholt
*Solving Local and Global Space Conflicts Using a Finite Element Method Adapted from Structural Mechanics*

Dirk Burghardt
*Automated Displacement by Energy Minimization*

Francois Lecordix
*Conflict Detection in PlaGe*

J. Mark Ware and Chris Jones
*Conflict Resolution Strategies Based on Object Displacement and Trial Positions*

Lifan Fei and Dietmar Grünreich
*Symbol Displacement in Cartographic Generalization on the Basis of a Hybrid Data Structure*

### S7: Synthesis -- Integration, Strategies and Control

Anne Ruas
*Generalization of an Urban Situation by Means of Road Selection and Building Aggregation, Typification and Displacement*

William A. Mackaness
*Automatic Strategies of Cartographic Design*

Corinne Plazanet
*Line Generalization Mechanisms and Caricature Algorithms*

Darka Mioc
*Spatio-Temporal Map Generalizations in the Dynamic Voroni Data Model*
