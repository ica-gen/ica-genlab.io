---
  layout: page
---

8th ICA Workshop - Papers and program
-------------------------------------

### Friday 20

Presentation of EuroSDR activites - Peter Woodsford
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/euroSDR1_ICA_WS04-oral.pdf)

#### Session 1: Generalisation in NMA

Patrick Revell (UK,NMA)
*Building on Past Achievements: Generalising OS MasterMap Rural Buildings to 1:50 000*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Revell-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/revell_ICA_WS04-oral.pdf)

Serdar Aslan, Cetinkaya B., Er Ilgin D.; Yildirim A. (Turkey, NMA)
*Some intermediate results of KartoGen generalization project in HGK*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/aslan-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/aslan_ICA_WS04-oral.pdf)

Adam Iwaniak, Chybicka I, Ostrowski, W (PL,NMA)
*Generalization of the topographic database to the vector map level 2*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Chybicka-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/iwaniak_ICA_WS04-oral.pdf)

Jantien Stoter, Kraak M J Knippers R A (NL / ITC)
*Generalisation of Framework Data: A Research Agenda*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/stoter-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/stoter_ICA_WS04-oral.pdf)

#### Session 2: GIS vendors proposals

Dan Lee (USA / ESRI)
*Geographic and cartographic contexts in generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Lee-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/lee_ICA_WS04-oral.pdf)

Dieter Neuffer, T. Hopewell, P. Woodsford (UK / Laser-Scan)
*Integration of Agent-based generalisation with mainstream technologies and other system components*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Neuffer-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/neuffer_ICA_WS04-oral.pdf)

Vince Smith (USA / Intergraph) P. Watson (UK / Laser-Scan)
*Interoperability of Agent-based generalisation with open, geospatial clients*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Smith-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/smith_ICA_WS04-oral.pdf)

Paul Hardy, Briat, Eicher, Kressmann (USA / ESRI)
*Database driven cartography from a digital landscape model with multiple representations and overrides*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Hardy-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/hardy_ICA_WS04-oral.pdf)

#### Session 3: MRDB

Christelle Vangenot (CH / EPFL)
*Multi-representation in spatial databases using the MADS conceptual model*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Vangenot-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/vangenot_ICA_WS04-oral.pdf)

Karl-Heinrich Anders (G / ikg) J Bobrich (G / bkg)
*MRDB Approach for Automatic Incremental Update*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Anders-v1-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/anders_ICA_WS04-oral.pdf)

Hanna Stigmar (SE / NMA)
*Merging route data and cartographic data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Stigmar-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/stigmar_ICA_WS04-oral.pdf)

Jenny Trevisan (FR / COGIT)
*From DLM to multi representation DCM - Modelling an application on buildings*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Trevisan-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/trevisan_ICA_WS04-oral.pdf)

Dariusz Gotlib, M. Lebiecki, R. Olszewski (PL / IPC)
*Investigating possibilities to develop the BDT in Poland as a MRDB type database*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Gotlib-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/gotlib_ICA_WS04-oral.pdf)

#### Session 4: Brain Storming in two groups

G1 : Challenges for NMAs?
Chairs : Cécile Duchêne / Colin Bray

G2 : Buildings a MRDB
Introduction and Chair: Peter Hojholt

[Brain storming summary](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/brainstormings.pdf)

Synthesis of the day
Future events of EuroSDR commission 4 and 5 - Peter Woodsford and Colin Bray
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/euroSDR2_ICA_WS04-oral.pdf)

### Saturday 21

Presentation of the ICA Commission activities - Anne Ruas / William Mackaness
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/ICA-activities.pdf)

#### Session 5: Algorithms

Jan-Henrik Haunert, M. Sester (G / ikg)
*Using the straight skeleton for generalisation in a multiple representation environment*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Haunert-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/haunert_ICA_WS04-oral.pdf)

Cécile Duchêne (FR / COGIT)
*The CartACom model: a generalisation model for taking relational constraints into account*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/duchene-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/duchene_ICA_WS04-oral.pdf)

Stefan Steiniger (CH / GIUZ) S. Meier (D / ipg )
*Snakes: a technique for line smoothing and displacement in map generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/steiniger-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/steiniger_ICA_WS04-oral.pdf)

Lars Harrie (SE / NMA)
*Using simultaenous graphic generalisation in a system for real time maps*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Harrie-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/harrie_ICA_WS04-oral.pdf)

Monika Sester (D / ikg)
*Automatic Generalization of buildings for small scales using typification and Streaming generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Sester-v1-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/sester_ICA_WS04-oral.pdf)

#### Session 6: 3D and Mobility

Mark Hampe, Monika Sester (D / ikg)
*Generating and Using a Multi Resolution Database (MRDB) for Mobile Applications*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/hampe-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/hampe_ICA_WS04-oral.pdf)

Frank Thiemann, Monika Sester (D / ikg)
*Segmentation of buildings for 3D generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Thiemann-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/thiemann_ICA_WS04-oral.pdf)

Andrea Forberg (D / ipk )
*Simplification of the 3D Building Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Forberg-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/forberg_ICA_WS04-oral.pdf)

Dirk Burghardt (CH / GIUZ ) Mathur Ajay (D, Axes-Systems
*Derivation of digital vector models - project DRIVE*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Burghardt-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/burghardt_ICA_WS04-oral.pdf)

#### Session 7: Data Enrichment

Julien Gaffuri, J. Trevisan (FR / COGIT)
*Role of urban patterns for building generalisation: An application of AGENT*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Gaffuri-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/gaffuri_ICA_WS04-oral.pdf)

Moritz Neun, R. Weibel, D. Burghardt (CH / GIUZ)
*Data enrichment for adaptive generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Neun-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/neun_ICA_WS04-oral.pdf)

Bin Jiang (SE / hig)
*Spatial clustering for mining knowledge in support of generalization processes in GIS*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Bin-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/jiang_ICA_WS04-oral.pdf)

Qingnian Zhang (China-Sweden)
*Modeling Structure and Patterns in Road Network Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Zhang-v2-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/zhang_ICA_WS04-oral.pdf)

#### Session 8: Brain Storming in three groups

G3: Research Agenda
Introduction and Chair: Nicolas Regnauld

G4: Changes coming from the web (open sources, access...)
Chair : Menno-Jan Kraak

G5: Building an Open Generalisation Platform
Chair : Robert Weibel

[brain storming summary](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/brainstormings.pdf)

Synthesis of the workshop - William Mackaness and Anne Ruas

### Sunday 22

#### Football match

[photo .. before the match](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/aci-wg-leicester-footbefore.JPG)

[photo .. after the match](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/aci-wg-leicester-footafter2.jpg)

### Papers not presented

Tiina Sarjakoski, Tapani Sarjakoski (FI / NMA)
A use case based mobile GI service with embedded map generalisation
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Sarjakoski-v2-ICAWorkshop.pdf)

Jagdish Lal - Liqiu Meng (D / ipk)
3D Building recognition using artificial neural network
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2004/Lal-v2-ICAWorkshop.pdf)
