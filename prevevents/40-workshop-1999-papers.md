---
  layout: page
---

3rd ICA Workshop - Papers
-------------------------

Blanca Baella and Maria Plà (Institut Cartographic de Catalunya)
*Map Generalization to Obtain the Topographic Map of Catalonia 1:10,000*

Zhilin Li and Chengming Li (The Hong Kong Polytechnic University)
*Algorithms for Objective Representation of DTM at Its Natural Scales*

Dietrich Schürer (University of Bonn)
*Requirements of Model Generalization for GIS and Implementation on SupportGIS*

Paul Hardy (Laser-Scan Ltd., Cambridge UK)
*Active Object Techniques for Production of Multiple Map and Geodata Products from a Spatial Database*

Jan Terje Bjørke (Norwegian Defence Research Establishment)
*Map Generalization: A Fuzzy Logic Approach*

Peter Højholt (National Survey and Cadastre, Denmark)
*From 1:10.000 to 1:50.000 at 300 km2/day*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/hojholt.pdf)

Byron Nakos (National Technical University of Athens)
*Comparison of Manual Versus Digital Line Simplification*

Michael E. Lonergan and Christopher B. Jones (University of Glamorgan)
*The Deformation and Displacement of Areal Objects During Automated Map Generalisation: An Approach to Conflict Resolution Through Maximising Nearest Neighbour Distances*

Anne Ruas (Institut Géographique National)
*The Role of Meso Level for Urban Generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/ruas/aci-ws-99-ruas)

Annabelle Boffet (Institut Géographique National)
*A Framework for Automated Spatial Analysis Based on Spatialization Principles*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/boffet/ACI-GB3-html)

Nicolas Regnauld, Alistair Edwardes and Mathieu Barrault (University of Edinburgh and University of Zurich)
*Strategies in Building Generalisation: Modelling the Sequence, Constraining the Choice*

Mahes Visvalingam (The University of Hull)
*Aspects of (Line) Generalisation*

Alistair Edwardes and William Mackaness (University of Edinburgh)
*Modelling Knowledge For Automated Generalisation of Categorical Maps - A Constraint Based Approach*

Barbara P. Buttenfield (University of Colorado, Boulder USA)
*Progressive Transmission of Vector Data on the Internet: A Cartographic Solution*

André Skupin (University of New Orleans)
*Revisiting Töpfer: Implications of the Radical Law for Scalable Spatialization*

Lars Harrie and Anna-Karin Hellström (University of Lund, Sweden)
*A prototype system for propagating updates between cartographic data sets*

Joachim Bobrich (University of Hannover)
*Solution of Cartographic Conflicts by Context Dependent Optimization*

Mats Dunkars and Anna Bergman (VBB VIAK and University College Gävle)
*Object Oriented Modelling of Geographical Information at Multiple Scales*

Peter van der Poorten and Chris Jones (University of Glamorgan)
*Customisable Line Generalisation using Delaunay Triangulation*

Dan Lee (ESRI Inc., Redlands USA)
*Practical Solutions for Specific Generalization Tasks*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/dlee.pdf)

Christopher Gold, David Thibault and Zhonghai Liu (Université Laval, Quebec, Canada)
*Map Generalization by Skeleton Retraction*

Tapani Sarjakoski and Tiina Kilpeläinen (Finnish Geodetic Institute)
*Least Squares Adjustment for Generalization of Large Data Sets*

Frank Brazile and Alistair Edwardes (University of Zurich and University of Edinburgh)
*Map Space Partitioning for Agent Process Organization*

Beat Peter and Robert Weibel (University of Zurich)
*Using Vector and Raster-Based Techniques in Categorical Map Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/peter_weibel.pdf)

Marc van Kreveld (University of Utrecht)
*Twelve Computational Geometry Problems from Cartographic Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop1999/kreveld.pdf)

Geoffrey H. Dutton (Spatial Effects, Belmont MA, USA)
*Visualizing Map Generalization Solutions on the World Wide Web*

Francis Harvey (University of Kentucky)
*Pessimistic Generalization: Limits and Lessons for Semantic Interoperability*

William A. Mackaness and Elizabeth Glover (University of Edinburgh)
*The Application of Dynamic Generalisation to Virtual Map Design*

Margarita Kokla and Marinos Kavouras (National Technical University of Athens)
*Spatial Concept Lattices: An Integration Method in Model Generalization*
