---
  layout: page
---

13th ICA Workshop - General information
---------------------------------------

[TABLE]

The chairs of the ICA commission on Map Generalisation and Multiple Representation warmly encourage the submission of research papers to a workshop that will explore multi scale, on demand mapping.

This two days workshop will assume reading of papers prior to the workshop, comprise short presentations, break out sessions, open problems and discussions. Participation and attendance at the workshop requires submission of research papers or position papers.

### Program and papers

Program and papers can be seen [here](67-workshop-2010-papers-and-presentations.md)
Program in printable form [here](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/program2010.pdf)

### Venue

Preliminary practical info for attendees:

-   Attendance is on invitation only.
-   The workshop will be held in the University of Zurich.
-   Attendees will only be charged for meals (approximately 110CHF, including Sunday lunch and dinner as well as Monday lunch). 
-   For booking accomodations, this [hotel booking site](http://www.hotelscombined.com/City/Zurich.htm) may help you, as well as the [GIScience website](http://www.giscience2010.org/index.php?page=hotels)

### Submission

Submissions are closed. 18 papers have been accepted.

### Key dates

The workshop will be held in Zurich, 12-13 September 2010, immediately prior to GIScience. 
Topics of interest are detailed below.
Key dates are:

-   15 April: deadline for submission of extended abstracts (2000 words maximum).
-   1 June: notification of acceptance of paper for presentation (& invitation).
-   1 August: deadline for submission of revised extended abstracts (Participants will have 1 month in which to read papers prior to the workshop).
-   12-13 September: Workshop

### Place, location and Format

The workshop will be held in Zurich, in September of 2010 immediately prior to the GIScience conference. It will be held in the Geography Department of the University of Zurich. The GIScience conference will be held 14-17 September (14 Sept is set aside for workshops). Presentations will be 10 mins with 10 mins for discussion. The workshop will have a limited set of presentations, with a focus on discussion groups and open problem solving.

### Topics of interest

Traditionally submissions have tended to reflect the research interests of NMAs, vendors and practitioners. Whilst such submissions will continue to be considered by the review panel, the reviewers particularly welcome papers that respond to changing contexts of map use, and in response to developing technologies in data capture, and dissemination via the web and through web based services. The following headings cover research that the Commission is particularly interested in exploring further.

#### Contexts of Use

Papers concerned with changing contexts of use, such as ‘spoken maps’, maps over mobile location aware devices, where generalisation is used to control variable delivery of geographic information at different scales.

#### Task Oriented Map Specifications

In more customised environments it should be possible to provide thematic maps tailored to the needs of the user. Papers that explore how a user’s request can be ‘translated’ into a specification that includes not just the content and geographic extent, but the generalisation operations required to cartographically render the map in a way that meets the specific needs of the user.

#### Evaluation Methodologies

Papers concerned with evaluation of generalised output – either in comparison with existing product, or in response to new contexts of use. Such papers include benchmarking of systems developed by National Mapping Agencies, or commercial off the shelf generalisation systems.

#### Frameworks for Map Generalisation

Various techniques have been developed for bringing together mixtures of generalisation operators – constraint based techniques, simulated annealing, satisficing and optimisation techniques. Papers exploring how we can model the process of ‘design’ – exploring deeper issues of a language of scale, and the semantics of map generalisation are most welcome.

#### Generalisation in support of Data integration

Increasingly NMAs seek to provide their data as a basis for third party integration. Generalisation is required in order to support integration of data sourced at different scales. The issue of data integration also extends into ontological modelling and schema ‘alignment’ or schema integration.

#### High Dimensional Generalisation

The application of generalisation in 3D, 4D (spatio temporal generalisation), and in the context of exploratory data analysis, the generalisation of high dimensional data.

For any of these topics, the Commission welcomes demonstrations that illustrate the utility of systems, or that demonstrate the handling of large volumes of geographic data.

### Scientific committee

**Workshop chairs** 
Cécile Duchêne
Robert Weibel

**Program chairs** 
William Mackaness 
Sebastien Mustiere 

**Scientific committee**
Karl-Heinrich Anders 
Melih Basaraner 
Dirk Burghardt 
Omair Chaudhry 
Cecile Duchene 
Sara Fabrikant 
Julien Gaffuri
Lars Harrie 
Jan-Henrik Haunert 
Patrick Luescher 
Martijn Meijers 
Liqiu Meng
Nicolas Regnauld 
Patrick Revell 
Anne Ruas 
L. Tiina Sarjakoski 
Monika Sester 
Larry Stanislawski 
Jantien Stoter 
Katalin Toth 
Necla Ulugtekin 
Peter van Oosterom 
Christelle Vangenot
Robert Weibel 
Stephan Winter 
Alexander Wolff
