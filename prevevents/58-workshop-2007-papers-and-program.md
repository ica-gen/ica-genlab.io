---
  layout: page
---

11th ICA Workshop - Papers and program
--------------------------------------

### Wednesday 1st

8pm : meet at the lobby of the COSMOS hotel (Conference Center) to eat jointly

### Thursday 2nd

9.00-9.30 Presentation of the Commission activites (2003-2007)

#### Session 1 : Modelling Multi Scale Networks

Stuart Thom (UK,NMA)
*Automatic Resolution of Road Network Conflicts using Displacement Algorithms Orchestrated by Software Agents*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Thom_ICA_Workshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/thom.pdf), [Image](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/image_thom.gif)

Guillaume Touya (Fr, COGIT)
*A Road Network Selection Process Based on Data. Enrichment and Structure Detection*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Touya-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/touya.pdf)

Discussion on Network
10.30-11 : Coffee Break

#### Session 2 : Generalisation in Production Environments

François Lecordix, J-M Le Gallic, L. Gondol, A. Braun (Fr / NMA)
*Development of a new generalisation flow line for topographic maps*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Lecordix_ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/lecordix.pdf)

Blanca Baella, M. Pla (Sp / NMA) J. Palomar-Vasquez, J Pardo-Pascual (Sp / Univ.)
*Deriving the relief of the Topographic Database of Catalonia at 1:25,000: spot heights generalization from the master database*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Blanca_ICAWorkshop2.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/baella.pdf)

Patrick Revell (UK / OS)
*Generic Tools For Generalising Ordnance Survey Base Scale Landcover Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Revell-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/revell.pdf)

Discussion on Production line
12.30-2pm : LUNCH

#### Session 3 : Web Based Services and generalisation on demand

Theodor Foerster, J Stoter, R. Lemmens (NL / ITC)
*Towards automatic web-based generalisation processing: a case study*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Foerster_ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/foerster.pdf)

Nicolas Regnauld (UK / OS)
*Evolving from automating existing map production systems to producing maps on demand automatically*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Regnauld-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/regnauld.pdf)

Discussion and Brain Storming on web generalisation services

4pm: end of the first day
4-6: EuroSDR Meeting for the EuroSDR Group. [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/jantien.pdf)
7.15pm: meet at the lobby of the COSMOS hotel (Conference Center) to eat jointly

### Friday 3rd

8:30-9:00 : Synthesis on Web generalisation Services

#### Session 4 : MRDB and Data Enrichment Methodologies

Katalin Toth (EU,JRC)
*Data consistency and multiple-representation in the ESDI*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Toth-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/toth.pdf)

Babs Buttenfield, E. Wolf (USA, Univ.)
*"The Road and the River Should Cross at the Bridge" Problem: Establishing Internal and Relative Topology in an MRDB*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Buttenfield_ICA_Workshop_2007.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/buttenfield.pdf)

Patrick Lüscher, Dirk Burghardt, Robert Weibel (Ch,Univ.)
*Ontology-driven Enrichment of Spatial Databases*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Luescher-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/luscher.pdf)

Discussion on MRDB and Data Enrichment

10.30-11: Coffee Break

#### Session 5 : Aggregating and Partitioning Geographic Information

Jan Haunert (G / IFK)
*Efficient area aggregation by combination of different techniques*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Haunert_ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/haunert.pdf)

Omair Chaudhry, W. Mackaness (UK / Univ)
*Utilising Membership Information in the Creation of Hierarchical Geographies*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Chaudhry-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/chaudhry.pdf)

Discussion on Aggregation
12.30-2pm: LUNCH

#### Session 6 : Formalising and Capturing of Cartographic Knowledge

J-L Monnot, Dan Lee, Paul Hardy (USA / ESRI)
*Topological constraints, actions, and reflexes for generalization by optimization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Monnot_Hardy_Lee_ICA_Workshop.pdf)

Patrick Taillandier (Fr, COGIT)
*Automatic Knowledge Revision of a Generalisation System*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Taillandier_ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/taillandier.pdf)

D. Burghardt, Stefan Schmid (CH, Univ.), Jantien Stoter (NL / ITC)
*Investigations on cartographic constraint formalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/Burghardt-ICAWorkshop.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/burghardt.pdf)

Discussion and Brain Storming on cartographic knowledge 
3.30-4pm: Coffee

#### Session 7 : Future of Generalisation and MRDB

Brain Storming: main challenges for the next 10 years.
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2007/final_session.pdf)
ICA Commission: Next challenges and agenda
5.30 : END OF THE WORKSHOP
