---
  layout: page
---

10th ICA Workshop - Papers and program
--------------------------------------

### Sunday 25th June 2006

Commission Annoucement
[Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Open.pdf)

### Session 1 : Semantic Modelling in Map Generalisation - Chair: J. Gaffuri

Barbara Buttenfield, C. Frye
*The Fallacy of the Golden Feature in MRDBs : Data Modeling Versus Integrating New Anchor Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Buttenfield.pdf)

Omair Chaudhry, W. Mackaness
*Indeterminate Boundaries in Higher order phenomenon*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-ChaudhryMackaness.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Chaudhry.pdf)

Geoff Dutton, A. Edwardes
*Ontological Modeling of GeographicalRelationship for Map Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Dutton-Edwardes.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Dutton-Edwardes.pdf)

### Session 2 : Map Generalisation Services - Chair: G. Dutton

Theodor Foerster , J. Stoter
*Establishing an OGC Web Processing Service for generalization processes*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-foerster_stoter.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Foerster2.pdf)

Ingo Petzold, D. Burghardt, M. Bobzien
*Workflow Management and Generalisation Services*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Petzold.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Petzold.pdf)

Peter Van Oosterom, M. de Vriers; M. Meijers
*Vario-scale data server in a web service context*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-VanOosterom2.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Oosterom.pdf)

Discussion Groups: Web Generalisation Services - Chair A. Ruas

### Session 3: Map Generalisation Methodologies - Chair B. Buttenfield

Julien Gaffuri
*How to merge optimization and agent-based generalization techniques in a single generalization model?*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Gaffuri.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Gaffuri.pdf)

Paul Hardy, J-L Monnot; D Lee
*An Optimization Approach To Constraint-Based Generalization In a Commodity GIS Framework*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Monnot.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Monnot-Hardy-Lee.pdf)

Bülent Cetinkaya, S. Aslan, Y. Sengun, O-N Cobankaya, D. Er Ilgin
*Contour Simplification with Defined Spatial Accuracies*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Cetinkaya.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Cetinkaya.pdf)

### Session 4 : Map Generalization in Production Environment - Chair: Peter van Oosterom

Patrick Revell, N. Regnauld, S. Thom
*Generalising and Symbolising Ordnance Survey Base Scale Data to Create a Prototype 1:50 000 Scale Vector Ma*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Revell.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Revell.pdf)

François Lecordix, J-M Le Gallic, L Gondol
*Clarity(TM) experimentation for cartographic generalisation in production*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Lecordix.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Lecordix.pdf)

Karel Stanek
*Design of a support system for cartographic generalization of a topographic reference base*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Stanek.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Stanek.pdf)

Dariusz Gotlib, Robert Olszewski
*Integration of the Topographic Database, VMap L2 Database and selected cadastral data - a step towards the integrated, MRDB reference database in Poland*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA2006-Gotlib_Olszewski.pdf), [Oral Presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2006/ICA-WS06-Gotlib.pdf)

Discussion Groups on Production Needs - Chair: W. Mackaness
