---
  layout: page
---

Program, slides and pictures - Paris 2011
=========================================

When: 2nd July 2011 (immediately prior to the ICA Conference 4-8th July 2011)

Where: IGN, Paris, France

Tutorial Abstract
=================

The tutorial was carried out to make practitioners from National Mapping Agencies, young researchers and scientists from GIScience related domains aware of the advances of the field of automated map generalisation and multiple representation. In particular we gave an overview of the current concepts and methods used in automated generalisation, such as generalisation operators, cartographic constraints, relation modelling and process orchestration. Selected results and experiences were presented from the EuroSDR project on the state-of-the-art in automated generalisation derived with commercial software. Furthermore the tutorial gave some insights of the “operational” usage of generalisation tools in map production during a demo session.

Tutorial Outline
================

The tutorial will be subdivided into two main parts:
1st Part 8:30 am - 12 am: Lectures on the basics 
2nd Part 1:30 pm - 4 pm: Generalisation in practice (NMA solutions and Demo of generalisation software tools)

Tutorial program
================

### Registration : 8:30 - 9:00 am

### Session 1: 9:00 am – 10:15 am

1. [Introduction](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/1-Intro_ICATutorial.pdf) (William Mackaness - 15 min)

-   Generalisation needs today
-   Activities of the ICA Commission
-   Brief overview of the tutorial structure

2. [Generalisation operators](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/2-Operators_ICATutorial.pdf) (Nico Regnauld - 30 min)

-   Frameworks for generalisation operators
-   Methods and approaches for 2D/3D vector based generalisation
-   Raster based generalisation methods

3. [Approaches to modelling the generalisation process](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/3-Modelling_ICATutorial.pdf) (Lars Harrie - 30 min)

-   Condition-action modelling
-   Human interaction modelling
-   Constraint-based /optimisation methods

### Session 2: 10:30 am – 12:00 am

4. [Constraints and evaluation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/4-Evaluation_ICATutorial.pdf) (Jantien Stoter - 30 min)

-   Definition of constraints and constraint typologies
-   Harmonised constraint within the EuroSDR project
-   Constrained based evaluation

5. [Relation modelling and MRDB](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/5-MRDB_ICATutorial.pdf) (Dirk Burghardt - 30 min)

-   Tools for spatial analysis and pattern detection
-   Triangulations and tree structures 
-   Vertical linkages and MRDB

6. [Multi-agents within automated generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/6-Agent_ICATutorial.pdf) (Cécile Duchêne - 30 min)

-   Multi-agent systems principles
-   Principles of agent modeling for generalisation
-   Overview of existing agent-based generalisation models 

### Lunch 12:00 am – 1:30 pm

### Session 3: 1:30 – 4:00 pm

Generalisation in Practice. Several demos, carried out in small groups, presented either the practical usage of generalisation software tools in a NMA production context or (last demo) current developments performed by industrials.

-   Ordnance Survey GB : building and coastline generalisation to create OS VectorMap District (P. Revell & N. Regnauld) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/demo_Revell_ICAtutorial.pdf)
-   IGN France : Generalisation in the production flowline of the new baseline map (X. Halbecq) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/demo_Halbecq_ICAtutorial.pdf)
-   LGL Baden-Württemberg : Map production of the DTK50 (S. Urbanke) - [slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/demo_Urbanke_ICAtutorial.pdf)
-   ESRI : Partitioning and parallelization for contextual generalization (D. Watkins) - paper presented at the workshop

Some pictures of the demo session
=================================

[![DemoOSGB](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoOSGB.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoOSGB.jpg)   [![DemoIGNF](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoIGNF.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoIGNF.jpg)   [![DemoLGLBW](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoLGLBW.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoLGLBW.jpg)   [![DemoESRI](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoESRI.jpg)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/tutorial2011/DemoESRI.jpg)
