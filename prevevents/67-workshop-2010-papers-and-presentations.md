---
  layout: page
---

13th ICA Workshop - Papers and presentations
--------------------------------------------

### Invited talks

[Generalisation in INSPIRE](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-Invited-Gaffuri.pdf) 
Julien Gaffuri.

[Outcomes from GDI’2010 (Generalisation and Data Integration Symposium in Boulder)](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-Invited-Buttenfield.pdf) 
Barbara Buttenflied.

[ICA Generalisation workshops (1995-2010), from past generalisation workshops to future works on the field](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-RoundTable-Burghardt.pdf) 
Dirk Burghardt.

### Session 1: Generalisation in Practice: Softwares, Map products…

[EuroSDR research on state-of-the-art of automated generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_4.pdf) 
Jantien Stoter, Blanca Baella , Connie Blok , Dirk Burghardt , Cecile Duchene , Maria Pla , Nicolas Regnauld Guillaume Touya. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-4.pdf))

[The Generalization of the Canadian Landmass: A Federal Perspective](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_15.pdf)
Daniel Pilon , Alexandre Beaulieu and Nouri Sabo. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-15.pdf))

[User-directed generalization of roads and buildings for multi-scale cartography](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_22.pdf)
Edith Punt and David Watkins. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-22.pdf))

### Session 2: Methods and constraints for generalisation

[A generic approach for simplification of buiding ground plan](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_16.pdf)
Hongchao Fan and Liqiu Meng. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-16.pdf))

[Ant Colony Optimization Applied to Map Generalization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_20.pdf)
Nigel Richards and Mark Ware. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-20.pdf))

[Towards constraint formulation for chorematic schematisation tasks - work in progress](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_14.pdf)
Andreas Reimer and Joachim Fohringer.

### Session 3: Evaluation of generalization quality

[Analytical estimation of map legibility](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_2.pdf)
Hanna Stigmar, Lars Harrie and Milan Djordjevic. ([slides]https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-2.pdf))

[City Model Generalization Similarity Measurement using Nested Structure of Earth Mover’s Distance](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_3.pdf)
Bo Mao, Hongchao Fan, Lars Harrie, Yifang Ban and Liqiu Meng. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-3.pdf))

[Generalization of Hydrographic Features and Automated Metric Assessment Through Bootstrapping](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_11.pdf)
Lawrence Stanislawski , Barbara Buttenfield and V.A. Samaranayake. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-11.pdf))

### Session 4: Generalisation in Practice: Softwares, Map products…

[Implementation of Comprehensive Modeling Techniques on KARTOGEN Generalization Software](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_1.pdf)
Özlem Simav , Serdar Aslan , Bülent Çetinkaya and Osman Nuri Çobankaya. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-1.pdf))

[Capitalisation problem in research - example of a new platform for generalisation : CartAGen](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_10.pdf)
Jérémy Renard , Julien Gaffuri and Cécile Duchêne. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-10.pdf))

### Session 5: Browsing semantics for generalisation

[Linked Data – A Multiple Representation Database at Web Scale?](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_12.pdf)
Stefan Hahmann and Dirk Burghardt. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-12.pdf))

[Semantics Matters: Cognitively Plausible Delineation of City Centres from Point of Interest Data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_24.pdf)
Patrick Lüscher and Robert Weibel.[
](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/slides/2010-ICAWSGene-24.pdf)

### Session 6: Data enrichment and partitioning for generalisation

[Relevant Space Partioning for Collaborative Generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_5.pdf)
Guillaume Touya. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-5.pdf))

[Generating Strokes of Road Networks Based on Pattern Recognition](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_8.pdf)
Xuechen Luan and Bisheng Yang. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-8.pdf))

[Data enrichment for road generalization through analysis of morphology in the CARGEN project](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_21.pdf)
Sandro Savino, Massimo Rumor, Matteo Zanon and Igor Lissandron. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-21.pdf))

### Session 7: Generalisation for mobile

[Generalisation of point data for mobile devices: A problem-oriented approach](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_25.pdf)
Pia Bereuter and Robert Weibel. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-25.pdf))

[Mobile map generalization approach considering user locational context](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/genemr2010_submission_13.pdf)
Jie Shen, Junfei Shi and Yi Long. ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2010/2010-ICAWSGene-13.pdf))
