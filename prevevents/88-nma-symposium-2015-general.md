---
  layout: page
---

2nd ICA / EuroSDR NMA Symposium - General information
-----------------------------------------------------

[TABLE]

The ICA Commission “Generalisation and Multiple Representation” and the EuroSDR commission “Data specifications” cordially invite national and regional mapping agencies to attend the:

**2nd ICA / EuroSDR NMA Symposium**

***Designing MRDB and multi-scale DCMs:
Sharing experiences between mapping agencies and the outside world***

**3, 4 December 2015, Kadaster, Amsterdam**

During the symposium participants will get the opportunity to present production workflows and solutions for multi-scale and multi-resolution databases and products. Besides the presentations of the participants, time will be kept for formal and informal discussions around the current practices and used tools/software products.

In March, 2013 the first symposium took place in Barcelona attended by 13 mapping agencies (presentations and abstracts can be found [here](73-nma-symposium-2013-presentations.md)). For the symposium of 2015 the presentations and abstracts are available [here](92-nma-symposium-2015-presentations.md).

The aim of the symposium is to see progress since then, to learn form each other’s experiences and to identify common needs and challenges that could be passed to industrials on the one hand and researchers on the other hand.

**Organising Committee:
**ICA: Cecile Duchene, Dirk Burghardt
EuroSDR: Jantien Stoter
Local organisation: Vincent van Altena, Marc Post and Jantien Stoter
