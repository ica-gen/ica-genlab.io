---
  layout: page
---

5th ICA Workshop - Papers
-------------------------

### Discussions

Semantic
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/semantic.pdf)

Data Update
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/update.jpg)

Common Platform
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/commonplatform.jpg)

Common Test Data and Benchmark
[Report](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/benchmarks.pdf)

### Papers and presentations

Mats Bader and Mathieu Barrault (U Zurich)
*Cartographic Displacement in Generalization: Introducing Elastic Beams*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/bader_barraultv1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/bader.pdf)

Blanca Baella and Maria Pla (ICC Barcelona)
*Map Names Generalization at the Institut Cartografic de Catalunya*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/baella-pla_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/baella.pdf)

Marianne Bengtson (KMS Denmark)
*Design and Implementing of Automatical Generalisation in a New Production Environment for Datasets in Scale 1:50.000 (- and 1:100.000)*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/bengtson_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/bengston.pdf)

Tao Cheng (University of Leicester)
*Quality Assessment of Model-Oriented Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/cheng_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/cheng.pdf)

Yuen Hang Choi and Zhilin Li (HK PolyU)
*Correlation of Generalisation Effects with Thematic Attributes of Cartographic Objects*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/choi_li_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/zhilinli.pdf)

Mats Dunkars (SWECO Position, Sweden)
*Automatic Generation of a View to a Geographical Database*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/dunkars_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Dunkars.pdf)

Martin Galanda (U Zurich)
*Optimization techniques for polygon generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/galanda_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/galanda.pdf)

Miguel Garriga and Geoff Baldwin (AAA)
*Generalization of Multiple Scale Maps from a Single Master Database*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/garriga_v2.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/garriga.pdf)

Qing Sheng Guo, Christoph Brandenberger and Lorenz Hurni, ETH (Zurich)
*A Progressive Line Simplification Algorithm*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/guo_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/guo.pdf)

Kelvin Haire (Laser-Scan Ltd, Cambridge)
*Active Object and Agent Based Approaches to Automated Generalisation (incl. Demo)*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/haire_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/haire.pdf)

Lars Harrie (U of Lund)
*Weight setting and Quality Assessment in Simultaneous Graphic Generalisation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/harrie_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Harrie.pdf)

Dan Lee (ESRI, Redlands)
*Moving Towards New Technology for Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/lee_v2.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/lee.pdf)

Sébastien Mustière and Cécile Duchêne (IGN France)
*Comparison of Different Approaches to Combine Road Generalisation Algorithms: GALBE, AGENT and CartoLearn*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/mustiere_duchene_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/mustiere.pdf)

Byron Nakos (Natl Tech U Athens)
*On the Assessment of Manual Line Simplification Based on Sliver Polygon Shape Analysis*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/nakos_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Nakos.pdf)

Beat Peter (U Zurich)
*Measures for the Generalization of Polygonal Maps with Categorical Data*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Peter_v2.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/peter.pdf)

Peter van der Poorten and Chris Jones (Cardiff U, UK)
*Feature Based Line Generalisation Using Delaunay Triangulation*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/poorten_jones_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/poorten.pdf)

Lilian Pun (HK Polytechnic University)
*Dynamic Representation of Vegetation Succession - A Preliminary Thought*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/pun_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/pun.pdf)

Anne Ruas (IGN France)
*Overview of Some Research Projects at IGN France*
[Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/ruas.pdf)

Tiina Sarjakoski and Lassi Letho (Finnish Geodetic Inst.)
*Challenges of Information Society for Map Generalization*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/sarjakoski_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Sarjakoski.pdf)

Monika Sester (University of Hannover)
*Kohonen Feature Nets for Typification*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/sester_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/sester.pdf)

Andriani Skopeliti and Lysandros Tsoulos (Natl Tech U Athens)
*A Methodology for the Assessment of Generalization Quality*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/skopeliti_tsoulos_v2.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/skopeliti.pdf)

Marc van Kreveld (University of Utrecht)
*Smooth Generalization for Continuous Zooming*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/van_kreveld_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/vankreveld.pdf)

Mark Ware, Chris Jones, Nathan Thomas (U of Glamorgan and Cardiff, UK)
*A Simulated Annealing Algorithm for Cartographic Map Generalization with Multiple Operators*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/jmware_v1.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/ware.pdf)

Sheng Zhou and Chris Jones(U of Cardiff, UK)
*Multi-Scale Spatial Database and Map Generalisation.*
[Paper](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/szhou_v0.pdf), [Oral presentation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2001/Zhou.pdf)
