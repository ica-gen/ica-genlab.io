---
  layout: page
---

12th ICA Workshop - Participants
--------------------------------

![group2008part](prevevents/images/images/Events/group2008part.jpg)

Blanca Baella, ICC, Spain
Pia Bereuter, University of Zurich, Switzerland
Joachim Bobrich, BKG, Germany
Dirk Burgardht, University of Zurich, Switzerland
Francoise de Blomac, SIG La Lettre, France
Ozgur Dogru, Istambul Technical University, Turkey
Cécile Duchêne, IGN COGIT, France
Anne Fechir, IGN Belgium
Theodor Foerster, ITC, Netherlands
Dávila Martínez Francisco Javier, IGN Spain
Julien Gaffuri, IGN COGIT, France
Tassilo Glander, University of Potsdam, Netherlands
Paul Hardy, ESRI, US
Arjen Hofman, TU Delft, Netherlands
Marc van Kreveld, Utrecht University, Netherlands
Thérèse Libourel, LIRMM, France
Patrick Luscher, University of Zurich, Switzerland
William Mackaness, University of Edinburgh, UK
Martijn Meijers, TU Delft, Netherlands
Sébastien Mustière, IGN COGIT, France
Byron Nakos, NTU Athen, Greece
Moritz Neun, ESRI Switzerland
Peter van Oosterom, TU Delft, Netherlands
Maria Pla, ICC, Spain
Nicolas Regnauld, Ordnance Survey, UK
Patrick Revell, Ordnance Survey, UK
Carsten Roensdorff, Ordnance Survey, UK
Yavuz Selim SENGUN, General Command of Mapping, Turkey
Vincent Smith, Intergraph, US
Bettina Speckmann, TU Eindhoven, Netherlands
Larry Stanislawski, USGS, US
Graham Stickler, 1Spatial, UK
Hanna Stigmar, National Land Survey - Lund University, Sweeden
Guillaume Touya, IGN COGIT, France
Robert Weibel, University of Zurich, Switzerland
Kees de Zeeuw, Dutch Kadaster, Netherlands
Xiang Zhang, Wuhan University and ITC, China
Sheng Zhou, Ordnance Survey, UK
