---
  layout: page
---

14th ICA Workshop - Papers and presentations
--------------------------------------------

Duration of each presentation: 15 minutes.

### Thursday, June 30st

08.30-09.30 Registration, welcome and fast forward session

09.30-11.00 Session 1: Generic Generalization Approaches

[Generating Multiple Scale Model for the Cadastral Map using Polygon Generalization Methodology](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Park.pdf)
Woojin Park and Kiyun Yu ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Park_slides.pdf))

[Parallelity in Chorematic Territorial Outlines](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Reimer.pdf)
Andreas Reimer and Wouter Meulemans ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Reimer_slides.pdf))

[A methodology on natural occurring lines segmentation and generalization based on visual perception principles](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Mitropoulos.pdf)
Vasilis Mitropoulos and Byron Nakos ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Mitropoulos_slides.pdf))

11.00-11.30 Coffee break

11.30-13.00 Session 2: Building Generalization; Quality

[Building Footprint Simplification Based on Hough Transform and Least Squares Adjustment](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Guercke.pdf)
Richard Guercke and Monika Sester ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Guercke_slides.pdf))

[A generic application for building typification](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Oeztug.pdf)
Ibrahim Öztug Bildirici, Serdar Aslan, Özlem Simav and Osman Nuri Çobankaya ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Simav_slides.pdf))

[Identifying map regions that are difficult to read](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Olsson.pdf)
Perola Olsson, Karsten Pippig, Lars Harrie and Hanna Stigmar ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Olsson_slides.pdf))

[Toward the generalisation of cartographic mashups: Taking into account the dependency between the thematic data and the reference data throughout the process of automatic generalisation](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Jaara.pdf)
Kusay Jaara, Cécile Duchêne, Anne Ruas ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Jaara_slides.pdf))

13.00-14.30 Lunch

14.30-15.30 Break-out sessions (and coffee)

15.30-17.00 Session 3: Generalization of topographic features I

[A Comparison of Star and Ladder Generalization Strategies for Intermediate Scale Processing of USGS National Hydrography Dataset](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Buttenfield.pdf)
Barbara Buttenfield, Lawrence Stanislawski and Cynthia Brewer ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Buttenfield_slides.pdf))

[Towards extraction of constraints for integrating environmental spatial data in digital landscape models of lower resolution – a work in progress](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stern.pdf)
Christian Stern and Monika Sester ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stern_slides.pdf))

[Automated generalisation of land cover data in a planar topographic map](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Van_Smaalen.pdf)
John Van Smaalen, Ron Nijhuis and Jantien Stoter ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Van_Smaalen_slides.pdf))

17.00-17.30 Reports from break-out sessions

### Friday, July 1st

08.30-10.00 Session 4: Generalization of topographic features II

[Generalizing the altimetric information of the Topographic Database of Catalonia at 1:5,000: classification and selection of break lines](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Palomar-Vazquez.pdf)
Jesus Palomar-Vazquez, Josep E. Pardo-Pascual, Blanca Baella and Maria Pla ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Palomar-Vazquez_slides.pdf))

[Pruning of Hydrographic Networks: A Comparison of Two Approaches](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stanislawski.pdf)
Lawrence Stanislawski and Sandro Savino ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stanislawski_slides.pdf))

[Combining Varied Federal Data Sources for Multiscale Map Labeling of Populated Places and Airports for The National Map of the United States](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Brewer.pdf)
Cynthia Brewer, James Thatcher and Stephen Butzler ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Brewer_slides.pdf))

10.00-10.30 Coffee break

10.30-12.00 Session 5: Architectures and Vario-Scale

[Towards a true vario-scale structure supporting smooth-zoom](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Van_Oosterom.pdf)
Peter Van Oosterom and Martijn Meijers ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Van_Oosterom_slides.pdf))

[Cache-friendly progressive data streaming with variable-scale data structures](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Meijers.pdf)
Martijn Meijers ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Meijers_slides.pdf))

[Collaborating for better on-demand mapping](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Balley.pdf)
Sandrine Balley and Nicolas Regnauld ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Balley_slides.pdf))

[Scalability of contextual generalization processing using partioning and parallelization](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Briat.pdf)
Marc-Olivier Briat, Jean-Luc Monnot, Edith M. Punt ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Briat_slides.pdf))

12.00-13.30 Lunch

13.30-14.30 Break-out sessions

14.30-15.00 Reports from break-out sessions

15.00-15.30 Coffee break

15.30-17.00 Session 6: Generalization of whole maps

[Method for deriving spatial data of Dresden to smaller scales](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Pippig.pdf)
Karsten Pippig and Dirk Burghardt ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Pippig_slides.pdf))

[Feasibility study on an automated generalisation production line for multiscale topographic products](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stoter.pdf)
Jantien Stoter, Ron Nijhuis, Marc Post, Vincent Van Altena, Jan Bulder, Ben Bruns, John Van Smaalen ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Stoter_slides.pdf))

[Automated generalisation of 1:10k topographic data from municipal data](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Altundag.pdf)
Dogan Altundag and Jantien Stoter ([slides](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/workshop2011/genemr2011_Altundag_slides.pdf))

17.00-17.30 Wrap up, next steps, plans
