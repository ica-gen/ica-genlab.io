---
  layout: page
---

NMA Symposium 2013 - Program and presentations
----------------------------------------------

Day 1
-----

08:30-0­8:45 Arrival of participants

08:45-0­9:00 Introduction session: welcome and introduction of participants

09:00 OSUK [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/OS_Presentation-outline-Barcelona2013.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_OSGB.pdf)
09:25 Swisstopo [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Swisstopo.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_SwissTopo.pdf)
09:50 IGN, France [\[abstract\]]https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Abstract-workshopICA-Barcelona-IGNFrance.pdf) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_IGNFrance.pdf)
10:15 IGN, Belgium [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Belgium_MRDB_Abstract_IGNB.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_IGNBelgium.pdf)

**10:40-­11:10 Coffee break**

11:10 NLS, Finland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/Finland.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_NLSFinland.pdf)
11:35 GST (KMS) Denmark [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_GST.pdf)
12:00 IGN, Madrid [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/IGN_Madrid.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_IGNSpain.ppt)
12:25 Kadaster, NL [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/NL_DLMDCM.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_KadasterNL.pdf)

**12:50-­14:20 Lunch**

14:20 OS, Ireland [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/OSI.pptx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_OSI.pdf)
14:45 AdV, Germany [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/ADV_project.docx) [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_AdV.pdf)
15:10 ICC, Catalonia  [\[slides\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_ICC.pdf)
15:35 JRC, Europe
16:00 USGS, USA [\[abstract\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/ICA_EuroSDR_Workshop_StanislawskiBrewerButtenfield_fin.docx) [\[slides-1\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_USGS_Part1_Main.pdf) [\[slides-2\]](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/symposium2013/presentations/2013_03_21_BarcelonaNMAMeeting_USGS_Part2_RoadThinning.pdf)

**16:00­‐16:20 Coffee break**

16:20­‐17:15 Break out session I

Day 2
-----

09:00-­09:30 Reporting back break out session 1

09:30­‐10:30 Break out session 2

**10:30­‐11:00 Coffee break**

11:00­‐11:30 Reporting back break out session 2

11:30‐12:30 Plenary discussion on common activities
