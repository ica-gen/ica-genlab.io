---
  layout: page
---


24th ICA Workshop on Map Generalisation and Multiple Representation
--------------------------------------------------------------

**You can now find information on the benchmarks presented at the workshop on a dedicated [page](benchmarks.md) of the website!**

Florence, Italy (and online)

13th of December, 2021

## Program

* **9h15-10h** Introduction to open science issues related to the commission, and examples of existing benchmarks (by the commissions chairs) [download the presentation][4]
* **10h-10h20** Coffee break
* **10h20-12h30** Presentation session (15’ presentation + 10’ questions)
  * Karel Stanek, Petr Silhak and Aneta Ryglova. *The benchmark set for slopes generalization*
  * Azelle Courtial, Guillaume Touya and Xiang Zhang. *AlpineBends – A Benchmark for Deep Learning-Based Generalisation*      
  * Guillaume Touya. *L’Alpe d’Huez: A Benchmark for Topographic Map Generalisation*
  * Guillaume Touya and Azelle Courtial. *BasqueRoads: A Benchmark for Road Network Selection*
  * Barry Kronenfeld, Lawrence Stanislawski, Barbara Buttenfield and Ethan Shavers. *Interactive Procedure for Localized Monitoring and Control of Polyline Simplification*  
* **12h30-14h** Lunch
* **14h-16h** Group work sessions [download the report](/downloads/Report_2021_ICA_Workshop.pdf)
* **16h-16h30** Coffee break
* **16h30-16h45** Conclusion of the workshop and presentation of the ICC sessions for the commission

## Registration

Registration is free but mandatory. Please send an email to Guillaume Touya to register either on-site (registration only possible until November 30th), or online.

**Submission deadline: ~~15th September~~ 15 October, 2021**

Rather than a series of talks, this workshop will focus on the creation of a set of benchmarks for map generalisation. Benchmarks are useful to compare map generalisation methods, and also to foster the search for solutions to complex map generalisation problems. A benchmark for map generalisation is composed of:

* A dataset to generalise (e.g., national mapping agency topographic data, OpenStreetMap data, thematic data with a topographic background, images of maps to transform with deep learning, etc.);
* A target map that can take the shape of any of the following: an image of the desired map; a generalised vector dataset; or a set of map specifications (e.g., generalisation constraints);
* An evaluation method to measure how far from the target map a proposed generalisation method is. Depending on the form of the target map, the evaluation method can be very different.

Accepted benchmarks will be presented and discussed during the workshop, in order to improve them before their official release. A journal paper will be prepared after the workshop to present the set of benchmarks and the workshop output, with all the volunteer workshop participants co-authoring.

The event will be hosted as a pre-conference workshop at ICC’21 Florence. Depending on the pandemic situation in the next months, the event will be both live and online, or only online.

## Submissions

You are invited to submit a dataset and a 2-page paper, describing the dataset and its use as a benchmark, following the general guidelines of the [ICA conference abstracts][2].

The proceedings of the workshop will be published as a new volume of the Abstracts of the ICA. Papers should be submitted to [EasyChair][1].


Datasets should be uploaded to [Zenodo.org][3], and the DOI should be mentioned in the paper.

The workshop will be open to researchers and practitioners interested in the benchmark process, regardless of submission or acceptance of a benchmark.

## Important dates

* Submission deadline: 			~~Wednesday, 15th September~~, Friday, 15th October 2021,
submitted to EasyChair.
* Notification of acceptance: 		by Friday, 29st October, 2021
* Workshop:  				Monday, 13th December, 2021 (to be confirmed)

## Organizing & Scientific Committee

* Pia Bereuter, FHNW University of Applied Sciences of Northwestern Switzerland.
* Azelle Courtial, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.
* Paulo Raposo, Faculty ITC, University of Twente, The Netherlands.
* Nicolas Regnauld, Esri, Redlands, CA, USA.
* Guillaume Touya, LASTIG, IGN-ENSG, Univ. Gustave Eiffel, France.
* Xiang Zhang, School of Resource and Environmental Sciences, Wuhan University, Wuhan, China


## Important dates
* Submission deadline: Monday 24th August, 2020, submitted to EasyChair.
* Notification of paper acceptance: by Friday 25th September, 2020
* Workshop: Thursday 5th to Friday 6th of November, 2020


[1]: https://easychair.org/conferences/?conf=icamapgen21
[2]: http://www.icc2019.org/data/ica-abstract_teplate.docx
[3]: https://zenodo.org/
[4]: https://www.slideshare.net/MapMuxing/introduction-of-the-24th-ica-workshop-on-map-generalisation
