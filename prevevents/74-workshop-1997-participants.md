---
  layout: page
---

2nd ICA Workshop - Participants
-------------------------------

![group1997](prevevents/images/images/Events/group1997.jpg)

*(not updated)*

### **Organizing Committee

Geoffrey **Edwards**
Université Laval
Centre de Recherche en Géomatique
Sainte-Foy
Quebec GIK 7P4
Canada
Fax: +1-418-656 36 07

Dietmar **Grünreich**
University of Hannover
Institute of Cartography
Appelstrasse 9 a
D-30167 Hannover
Germany
Fax: +49-511-762 27 80

Christopher B. **Jones**
University of Glamorgan
Department of Computer Studies
Pontypridd
Mid Glamorgan CF37 1DL
Wales, UK
Fax: +44-1443-48 27 15

Liqiu **Meng**
VBB Viak AB
Box 34044
S-10026 Stockholm
Sweden
Fax: +46-8-695 63 10

Dianne E. **Richardson**
Canada Centre for Remote Sensing
Major Projects Office
588 Booth Street
Ottawa, Ontario K1A 0E9
Canada
Fax: +1-613-947 1383

Robert **Weibel**
University of Zurich
Department of Geography
Winterthurerstrasse 190
CH-8057 Zurich
Switzerland
Fax: +41-1-635 6848

### Participants

François **Anton**
Industrial Chair of Geomatics
Centre for research in Geomatics
0722 Pavillon Casault
Universite Laval
Ste. Foy, Quebec, G1K 7P4
Canada.
Fax: (+1 418) 656 7411

Nico J. **Bakker**
Topografische Dienst
Cartographic Department
Postbus 115
Bendienplein 5
NL-7815 SM Emmen
The Netherlands
Fax: +31-591-69 62 96

Anna **Bergmann**
Högskolan Gävle-Sandviken
T-institutionen
Kungsbäcksvägen 47
S-801 76 Gävle
Sweden
Fax: +46 26 648 828

Jan Terje **Bjørke**
University of Trondheim
Dept. of Surveying and Mapping
N-7034 Trondheim
Norway
Fax: +47-73-59 46 21

Frank **Brazile**
University of Zurich
Department of Geography
Winterthurerstrasse 190
CH-8057 Zurich
Switzerland
Fax: +41 1 635 6848

Allan **Brown**
ITC
Dept. of Geoinformatics
P.O. Box 6
NL-7500 AA Enschede
The Netherlands
Fax: +31-534874335

Dirk **Burghardt**
Technische Universität Dresden
Institut für Planetare Geodäsie
Mommsenstrasse 13
D-01069 Dresden
Germany
Fax: +49 351 463 7063

Barbara **Buttenfield**
U.C. Boulder
Department of Geography
Campus Box 260
Boulder, CO 80309
USA
Fax: +1-303-492-7501

Mats **Dunkars**
VBB Viak AB
S-100 26 Stockholm
Sweden

Geoffrey **Dutton**
University of Zurich
Department of Geography
Winterthurerstrasse 190
CH-8057 Zurich
Switzerland
Fax: +41 1 362 5227

Mats **Eriksson**
Stockholm University
Sweden

Lifan **Fei**
University of Hannover
Institute of Cartography
Appelstrasse 9a
D-30167 Hannover
Germany

Agnes **Gryl**
Université Laval
Centre de Recherche en Géomatique
Sainte-Foy
Quebec GIK 7P4
Canada
Fax: +1-418-656 36 07

Lars **Harrie**
Lund University
Sweden

Francis **Harvey**
EPFL
IGEO-SIRS
GR-Ecublens
CH-1015 Lausanne
Switzerland
Fax: +41 21 6935790

Peter **Højholt**
Kort- og Matrikelstyrelsen
Map and Chart Division
Rentemestervej 8
DK-2400 Copenhagen NV
Denmark
Fax: +45-35 87 50 57

Olli **Jaakkola**
Finnish Geodetic Institute
PL 15
Geodeetinrinne 2
FIN-02431 Masala
Finland
Fax: +358 9 29555 200

Ingrid **Jaquemotte**
Fachhochschule Oldenburg
Institut für Angewandte Photogrammetrie und Geoinformatik
Ofener Strasse 16-19
D-26121 Oldenburg
Germany

Jean-Philippe **Lagrange**
ETCA/CTME/CEGN
16bis avenue Prieur de la Côte d'Or
F-94114 Arceuil Cedex
France
Fax: +33-1-42 31 99 77

François **Lecordix**
Institut Géographique National
2, avenue Pasteur
F-94160 Saint-Mandé
France
Fax: +19 1 4398 8171

Dan **Lee**
ESRI, INC.
380 New York Street
Redlands, CA 92373
USA
Fax: +1 909 793 5953

Zhilin **Li**
Hong Kong Polytechnic University
Dept. of Land Surveying and Geo-Informatics
Hong Kong
Fax: +852 2330 2994

William A. **Mackaness**
University of Edinburgh
Department of Geography
Drummond Street
Edinburgh EH8 9XP
Scotland, United Kingdom
Fax: +44-131-650-2524

Robert B. **McMaster**
University of Minnesota
Department of Geography
414 Social Sciences Building
Minneapolis, MN 55455
USA
Fax: +1-612-625 6080

Darka **Mioc**
Université Laval
Centre de Recherche en Géomatique
0722 Pavillon Casault
Sainte-Foy
Quebec G1K 7P4
Canada
Fax: +1 418 656 7411

Byron **Nakos**
National Technical University of Athens
Dept. of Surveying / Carto Lab
Iroon Polytechniou 9
GR - 157 80 Zographos
Greece
Fax: +30 1 7722670

Jan **Neumann**
Zememericky urad
Kostelni 42
CZ-70 00 Prague 7
Czech Republic
Fax: 42 2 382233

María **Pla**
Generalitat de Catalunya
Institut Cartogràfic de Catalunya
Parc de Montjuïc s/n
E-08038 Barcelona
Spain
Fax: +34-3-426 74 42

Corinne **Plazanet**
Institut Géographique National
BP 68
2, avenue Pasteur
F-94160 Saint-Mandé
France
Fax: +33 1 4398 8171

Anne **Ruas**
IGN-France
COGIT Laboratory
BP 68
2, avenue Pasteur
F-94160 Saint-Mandé
France
Fax: +33-1-43 98 81 71

Dietrich **Schürer**
Universität Bonn
Institut für Kartographie
Meckenheimer Allee 172
D-53115 Bonn
Germany
Fax: +49 228 737756

Zhen **Tian**
Dalian Naval Academy
552, Department of Marine Surveying & Mapping
1, Xiao Long Street
Dalian
China (116018)
Fax: +86 411 2678355

John van **Smaalen**
Wageningen Agricultural University
Dept. of Geographic Information
Processing and Remote Sensing
P.O. Box 339
NL-6700 AH Wageningen
The Netherlands
Fax: +31-317-48 46 43

J. Mark **Ware**
University of Glamorgan
Department of Computer Studies
Pontypridd
UK-Mid Glamorgan CF 37 1DL
United Kingdom
Fax: +44 1433 482715

Thomas **Wilke**
University of Hannover
Institute of Cartography
Appelstrasse 9a
D-30167 Hannover
Germany
Fax: +49 511 762 2780
