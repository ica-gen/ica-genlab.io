---
  layout: page
---

15th ICA Workshop - General information
---------------------------------------

[TABLE]

 

 

 

 

 

The chairs of the ICA Commission on Map Generalisation and Multiple Representation and EuroSDR Commission 4 "Data Specifications" warmly encourage the submission of short research papers to the **15th Generalisation Workshop**, which will take place in

*Istanbul, Turkey*, *13-14 September, 2012*

This time the workshop will explore in particular new challenges and solution in the domain of automated generalisation for on demand mapping and the changing context of map use. The presentations and discussion at the workshop will contribute to a book project of the ICA commission on Map Generalisation and Multiple Representation planned for the current working period.
Participation and attendance at the two day workshop requires submission of research papers or position papers. The workshop will assume reading of papers prior to the workshop, comprise short presentations, break out sessions, demonstrations, posters, open problems and discussions. Presentations will be of mixed duration, but relatively brief, with a focus on discussion groups and open problem solving.

**Key dates:**

-   **15 Mai: deadline for submission of short papers** (limit 3000 words)
-   15 June: notification of acceptance of paper for presentation (& invitation)
-   31 August: deadline for submission of revised papers
    (participants will have around two weeks to read papers prior to the workshop)
-   13 and 14 September: Workshop

Submission and review of short papers will be managed through EasyChair (an online conference management system): <https://www.easychair.org/conferences/?conf=genemr2012>

**Place and local organising committee:**
A local organising committee has formed by cartographic institutions from General Comand of Maping (Serdar Aslan, Nuri Cobankaya), İstanbul Technical University (Ozgur Dogru) and Yildiz Technical University (Melih Basaraner, Fatih Gulgen, Türkay Gökgöz). The workshop will be held in Istanbul at Yıldız Technical University, very close to Taksim, center of İstanbul. The meeting room called “Hünkar Salonu” (Sultan’s Hall) is situated in the historical rectorate building.

**Registration fee:**
The registration fee will be 60 € including coffee breaks and lunches for 2 days. The payment will be organised at the Istanbul University hosting the workshop by the local organisers, just prior the beginning of the workshop.

**Travel awards:**
EuroSDR has sponsored two travel awards for PhD students (each 500 €). PhD students have submitted a short paper for the workshop and have problems with financing the participation can apply by e-mail to the commission chairs until the 15th Mai. The students should write shortly why they feel they are worthy recipient and what other sources they have applied to.

**Paper format:**
The length of the submitted papers should not exceed a limit of 8 pages, consisting in a text of up to 3000 words illustrated by figures. Guidelines for the text: 1 column, body in Times New Roman 12 or equivalent, reference list can be decreased to Times New Roman 10 or equivalent. Papers should have page numbers and a header like "15th ICA Generalisation Workshop, Istanbul, Turkey, 2012". In the final version we will accept up to 10 pages, especially to take into account the comments of the reviewers.

A word template is available [IcaGenTemplate.doc](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/icagentemplate.doc)

**Topics of interest:**
Besides submissions related to research interests of NMAs, vendors and practitioners, the reviewers particularly welcome papers that respond to changing contexts of map use, and in response to developing technologies in data capture, and dissemination via the web and through mobile devices. The following headings cover research that the Commission is particularly interested in exploring further:

-   framework(s) for on-demand mapping, and the place of generalisation/multiple representation
-   user requirements and map specifications for adaptive generalisation
-   data structures and multi scale modelling in support of continuous generalisa-tion / visualisation
-   mashups and data integration, generalisation of web 2.0 data sources
-   on-the-fly generalisation
-   web generalisation services
-   generalisation within mobile applications
-   3D generalisation
-   temporal generalisation
-   generalisation and privacy issues

For any of these topics, the Commission also welcomes demonstrations that illustrate the utility of systems, or that demonstrate the handling of large volumes of geographic data.
