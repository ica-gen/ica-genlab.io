---
  layout: page
---

![](/images/logos/ica_logo_commission.png){: .right}

20<sup>th</sup> ICA Workshop on Generalisation and Multiple Representation
==========================================================================


*In the tradition of pre-conference workshops in association with the International Cartographic Conference (ICC) the ICA Commission on Generalisation and Multiple Representation will organise a workshop in*

**Washington, United States, 1 - 2 July, 2017**

The workshop will explore new challenges and solutions in the domain of automated generalisation. A special focus will be set on approaches to generalise location-ba**sed social media data. The first day (1 July 2017) will be carried out as a regular workshop of the ICA Commission on Generalisation and Multiple Representation. For the second day we have the aim to have combined sessions with ICA Commission on Geospatial Analysis and Modeling and ICA Commission on Location-based Services on topics related to "Scalability and Abstraction of Location-based Social Media Data and Tracking Data".

The challenges of research in Generalisation and Multiple Representation draws upon researchers and practitioners alike, working in the fields of on-demand mapping, geovisual analysis, multiple representations, data integration and generalisation of geographic information. Participants of the workshop are invited to submit research papers or positions papers.

[Program](20th-workshop-program.html)

[Paper Download (for participants)](https://cloudstore.zih.tu-dresden.de/index.php/s/dygK4RTjCIis6K7/authenticate)

<!---
### Key dates:

-   1 March extended to 24 March: deadline for submission of short papers (limit 3000 words, and 8 pages including figures)
-   25 April: notification of acceptance of paper for presentation
-   31 May: deadline for submission of revised papers
-   1 - 2 July: Workshop

Submission and review of short papers will be managed through EasyChair
(an online conference management system): <https://www.easychair.org/conferences/?conf=genemr2017>

*A word template is available: [ICA\_Workshop\_Template\_2017.docx](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/workshop2017/ICA_Workshop_Template_2017.docx)*
--->

### Location:
[Washington Marriott Wardman Park, Room: __](http://icc2017.org/hotel-maps-for-icc2017-events/)

### Topics of interest:

-   Place modelling and understanding
-   Scalability issues with social media data and tracking data
-   Development of generalisation algorithms
-   Generalisation process modelling and orchestration, e.g. for on-demand mapping
-   Generalisation web services
-   Data integration and harmonisation of user generated or multi-source spatial content
-   Multi-scale data matching, change detection and updating
-   Graph and network based analysis and visualisation related to social media data
-   3D-Generalisation
-   Continuous generalisation
-   Evaluation and quality in generalisation

##### Scientific workshop organisers & program committee:

Dirk Burghardt, Cecile Duchene

Hongchao Fan,
Lars Harrie,
Jan-Henrik Haunert,
Bin Jiang,
Dan Lee,
William Mackaness,
Martijn Meijers,
Nicolas Regnauld,
Timofey Samsonov,
Monika Sester,
Lawrence Stanislawski,
Stefan Steiniger,
Guillaume Touya,
Vincent van Altena,
Robert Weibel,
Xiang Zhang
