---
  layout: page
---


2nd workshop on CartoAI: AI for cartography
--------------------------------------------------------------

Dresden, Germany (on site and online)

**June 10, 2025**


In recent years, the development of Artificial Intelligence (AI) in the field of cartography has been truly inspiring. AI technology has demonstrated immense potential in areas such as map generalization, automated map-making, map design, user interaction, and many more. With these advancements, cartography is undergoing an unprecedented transformation.

Following a successful CartoAI Workshop in 2023, this workshop seeks to gather the researchers working on AI techniques applied to cartography. For this workshop, contributions on all AI-related techniques are welcome. The workshop will give the participants the opportunity to give short presentations on recent and ongoing research. It also includes a keynote and time for discussions.

The following topics are welcome:

* Novel deep-learning techniques applied to maps or cartographic data
* AI techniques for map generalization, map schematization, text placement
* Pattern recognition in maps
* Cartographic style transfer
* AI techniques for map vectorization and map label interpretation
* Large language model for map interaction
* Evaluation of AI techniques and results
* Review of AI studies in cartography
* Explainable AI (XAI) for AI in Cartography

See more details on the workshop [webpage][1].

## Program

To be announced...


## Important dates

* Submission deadline: 			31 March 2025,
submitted to EasyChair.
* Notification of acceptance: 		27 April 2025
* Workshop:  			10 June 2025

## Organizing Committee

* Yu Feng, Technical University of Munich, Germany
* Güren Tan Dinga, HafenCity University Hamburg, Germany
* Guillaume Touya, IGN, Univ. Gustave Eiffel, France
* Zhiyong Zhou, University of Zurich, Switzerland

[1]: https://cartoai.net/en/events/cartoai25/
