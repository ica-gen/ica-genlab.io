---
  layout: page
---


AAG 2024 session "Multiscale Mapping That Works: Harnessing Intelligent Methods for Cartographic Display and Analysis"
--------------------------------------------------------------

Honolulu, Hawaii (and online)

**Submission deadline: ~~Nov. 16~~ Nov. 30 2023**

**April 16-20,  2024 (more details to come on the date of the session)**

Almost 30 years ago, a book How Maps Work (MacEachren, 1995) generated lively discussion about the nexus between how geospatial information is represented cartographically and how people understand and use mapped representations to learn about the world, facilitate scientific analysis, and support decision making. Since that time, several advances in geospatial technologies have bolstered cartographic representation capabilities that are useful to scientists and scholars, but the methods can be difficult to access, interpret and utilize for other user communities.

This paper session focuses on two such advances, the development of multiscale mapping databases and the application of intelligent methods to capture, curate and integrate geospatial features across spatial and temporal scales and resolutions. Intelligent methods include but are not limited to knowledge-based or artificial intelligence (AI) techniques, such as rule-based systems, neural networks, genetic algorithms, and generative AI. Interest in these methods has been widespread, which raises important questions about how to tailor and evaluate the strategies and resulting multiscale data products for applications in social and physical science domains. What guidelines can be established to ensure strategies are operable, repeatable, and reliable? Can processing and outcomes be reported in ways that are understandable to user communities lacking specialized knowledge about multiscale mapping and data? Are we limited to metrics regarding efficient processing, data volume and accuracy, or can we advance the assessment to consider semantics about representing the world well with multiscale mapping? We invite demonstrations or proposals for implementing or assessing intelligent methods for geospatial data transformations across multiple spatial and/or temporal scales.


## Program

* [Automated Generalization under the Constraints of Similarity, Visual Clarity, and Semantics][2], <span style="color:blue">Pengbo Li</span>,  Lanzhou Jiaotong University
* [The role of the physical-geographical variables in the automatic selection of rivers][3], <span style="color:blue">Iga Ajdacka</span>, University of Warsaw
* [Don’t Hate, Automate! Settlement Selection with the Use of Machine Learning][4]. <span style="color:blue">Izabela Karsznia</span>, University of Warsaw
* [Exact computation of Hausdorff and average distance between polylines to support intelligent multiscale mapping][5], <span style="color:blue">Barry Kronenfeld</span>, Eastern Illinois University
* [Assessment of Density Pattern Retention for Generalized Data][6], <span style="color:blue">Larry Stanislawski</span>, U.S. Geological Survey, Center of Excellence for Geospatial Information Science


## Registration

Registration is done via the AAG conference registration system. Please visit the AAG [website][1].

If you only plan to attend online, there is a special registration plan on the [website][1].

## Submissions

The AAG accepts all submitted abstracts and organized sessions for presentation. *The registration fee must be paid prior to abstract submission. You may only submit one abstract for presentation* and be a panelist in one panel session. If you opt not to submit an abstract, you may be a panelist twice. There is no limit on how many sessions you may organize.

See the details for registration (and thus submission on the AAG [website][1])

## Important dates

* November 16: Abstract Submission deadline
* February 29: Abstract/Session Editing and Presentation Conversion deadlines
* April 16-20: Annual Meeting

## Organizing Committee

* Barry Kronenfeld, Eastern Illinois University (bjkronenfeld{at}}eiu.edu)
* Larry Stanislawski, USGS Center for Excellence in Geospatial Sciences (lstan{at}}usgs.gov)
* Barbara Buttenfield, University of Colorado – Boulder
* Shaowen Wang, University of Illinois Urbana-Champaign
* Ethan Shavers, USGS Center for Excellence in Geospatial Sciences
* Yuhao Kang, University of South Carolina





[1]: https://www.aag.org/events/aag2024/
[2]: https://aag.secure-platform.com/aag2024/solicitations/57/sessiongallery/7318/application/28747
[3]: https://aag.secure-platform.com/aag2024/solicitations/57/sessiongallery/7318/application/32401
[4]: https://aag.secure-platform.com/aag2024/solicitations/57/sessiongallery/7318/application/33009
[5]: https://aag.secure-platform.com/aag2024/solicitations/57/sessiongallery/7318/application/31919
[6]: https://aag.secure-platform.com/aag2024/solicitations/57/sessiongallery/7318/application/29065
