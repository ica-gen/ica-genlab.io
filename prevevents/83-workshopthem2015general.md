---
  layout: page
---

Thematic Workshop - General Information
---------------------------------------

 

![ica logo](/images/ica_logo.png)

*Thematic Workshop on building an Ontology of Generalisation for
On-demand Mapping
*
------------------------------------------------------------------

*Great progress has been made in recent years in automatic generalisation for map production. However, to extend this progress into on-demand and thematic mapping it is necessary to capture the semantics of generalisation.  The representation in an ontology of geographic features and the semantic and spatial relations between them, along with concepts such as generalisation operators and algorithms, will allow for reasoning about the domain and thus support automation.*

The aim of the workshop is to start a collaboration for developing such an ontology. This will be achieved by identifying: the relevant skills in the community, ontology design methodologies, potential tools, and potential use cases. The workshop will also include an OWL (Web Ontology Language) tutorial based on Protégé. By the end of the workshop we hope to have agreed on a method for the development of the ontology, including tools and standards, and to have identified groups to work on particular aspects.

The ICA Commission on Generalisation and Multiple Representation will organise the 1½ day meeting in

* 
*

*Paris, France*, 2*6-27 March, 2015*

 

The workshop will be hosted by Institut National de l’Information Géographique et Forestière (IGN). It will start at 9 am on 26 March and end at 13 am on 27 March.

### Summary of the workshop

The following paper presents a summary of the activities carried out and initiated at the Generalisation Ontology Workshop 2015.

 [![pdf](prevevents/images/icons/pdf.jpg) GeneralisationOntology\_Paris2015.pdf](https://gitlab.com/ica-gen/workshop-proceedings/-/raw/main/ica-gen-downloads/ica-gen/ThemWorkshop/ThematicOntologyOnDemand_Paris2015.pdf)

### Workshop organisers

Nick Gould, Dirk Burghardt, Cécile Duchêne, William Mackaness, Guillaume Touya
