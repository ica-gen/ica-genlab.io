---
  title: Current Achievements
  layout: page
  order: 8
---

Examples of results
===================

The purpose of this section is to provide some result samples of some works on generalisation. These samples concern several different kinds of generalisation (different types of objects, operations, scale changes...). The coordinates of the author(s) are available. If you want the results of your work to be published on this page, contact the chairs.

Multi-Agent based generalisation system (IGN France)
---------------------------------------------------------------

This work deals with the automatic generalisation of topographic maps based on the agent paradigm. The first principles were defined by Anne Ruas in her PhD, and this seminal work was further extended during the E.U. funded AGENT project.

![](images/results/AGENT_results.png){: .left }

Contact: Cécile Duchêne (LASTIG, IGN, France)

Road and river network selection (IGN France)
----------------------------------------

Network selection seeks to choose the features from an initial geo-database that will appear in a generalised geo-database. For road and river network, it is complex process as a good choice should reduce the amount of features but should also preserve the characteristics of the networks (hierarchies, density differences, specific patterns). The automatic processes developed at COGIT are based on a complete enrichment of the initial database by specific structures and patterns of the network (e.g. braided streams for rivers or highway interchanges for roads) to allow a good selection.

![](images/results/river_selection.png){: .left }

![](images/results/road_selection.png){: .left }

Contact: Guillaume Touya (LASTIG, IGN, France)

Label generalization (ICC)
--------------------------

Results of the automated selection and scaling of map names done by ICC software.

Simultaneous graphic generalisation (Lund University, Finnish Geodetic Institute)
---------------------------------------------------------------------------------

![](images/results/lsa.jpg){: .left }

This study deals with an optimization (with continuous variables) approach to graphic generalization (essentially the operators simplification, smoothing, exaggeration and displacement). By solving the whole graphic generalization in a single step, both in terms of operators and all types of objects, we provide a sound compromise between the competing goals in the graphic generalization process. The basic idea behind simultaneous graphic generalization is to formulate the requirements of graphic generalization as analytical constraints and then to use these constraints as observations in a least-squares adjustment.

The figure visualizes the effect of simultaneous graphic generalization in a small area. The original objects are shown in gray and the generalized ones in black. (© The National Land Survey of Finland, all rights reserved.)

Contact: Lars Harrie (Lund University, Sweden)

Multi-scale generalization (IGN France)
----------------------------------------

The results presented in this section are multi-scale maps with three intermediate scales between 1:25k and 1:100k (both scales are existing maps). The Gif animations zoom in and out in the 5 scales from the 1:25k to the 1:100k, through the three intermediate scales obtained by automatic generalization. The work was carried out at LASTIG, IGN France by [Marion Dumont][1] (supervised by Guillaume Touya and Cécile Duchêne)/

Multi-scale map obtained with a combination of AGENT and CartACom.

<![AGENT-CartACom multi-scale map around Ciboure](images/results/Ciboure_AnimationAgent_small.gif) >

<![AGENT-CartACom multi-scale map around Saint-Pée-sur-Nivelle](images/results/AnimationAgent_small.gif) >

Multi-scale map obtained with typification.

<![AGENT-CartACom multi-scale map around Ciboure](images/results/Ciboure_AnimationTypif_small.gif) >

<![AGENT-CartACom multi-scale map around Saint-Pée-sur-Nivelle](images/results/AnimationTypif_small.gif) >

Multi-scale map obtained with a progressive block graying with landmarks preserved.

<![AGENT-CartACom multi-scale map around Ciboure](images/results/Ciboure_AnimationReperesVisuels_small.gif) >

<![AGENT-CartACom multi-scale map around Saint-Pée-sur-Nivelle](images/results/AnimationReperesVisuels_small.gif) >

Multi-scale map obtained with a progressive block graying.

<![AGENT-CartACom multi-scale map around Ciboure](images/results/Ciboure_AnimationAgregation_small.gif) >

<![AGENT-CartACom multi-scale map around Saint-Pée-sur-Nivelle](images/results/AnimationAgreg_small.gif) >

Contact: Guillaume Touya (LASTIG, IGN, France)

[1]: http://recherche.ign.fr/labos/util_basilic/publicDownload.php?id=4130
