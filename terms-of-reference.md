---
  layout: page
---

Articles
========

Terms of reference
------------------

For the 2023-2027 period, the ICA commission on multi-scale cartography has the following objectives:

1. To foster research and practical experience in multi-scale issues and automated map generalisation and to form a network and focal point for researchers and practitioners in this domain.
2. To promote open science in map generalisation by developing open benchmark datasets (i.e. standardised sample test data), best practices, open source platforms, and open source code related to papers published at the commission workshops.
3. To contribute to the effort to contain climate change:
  - initiate research on energy-sober web maps
  - promote new ways to gather the community, more compatible with climate change issues.
4. To maintain a World Wide Web site for electronic dissemination of research on multiscale issues and map generalisation, including:
  - a membership database,
  - a bibliographic database,
  - examples of research projects,
  - links to related websites,
  - promotion of the shared ontology designed in the past by commission members,
  - and an electronic discussion list.
4. To maintain a liaison with related commissions and working groups within the ICA as well as in related national and international organisations (e.g. ISPRS, IGU, EuroSDR).
5. To organise sessions and meetings for exploring the issues named above, including:
  - workshops held every two years in conjunction with International Cartographic Association conferences
  - special sessions and/or panel sessions at International Cartographic Association conferences, in collaboration with local organisers
6. To produce publications for dissemination of the above efforts, including one special issue of a peer-reviewed scientific journal or a book every four years.
