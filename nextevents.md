---
  title: Next Events
  layout: page
  order: 2
---



# 2nd Workshop on CartoAI

Location: Dresden, Germany (prior to AGILE 2025 conference).

Date: **10 June 2025**

More details [here](prevevents/cartoai2025.md).


# ICA Workshop on Multiscale Cartography and Sustainability

Location: Vancouver, Canada (prior to ICC 2025 conference).

Date: **16 August 2025**

More details [here](prevevents/workshop2025.md).


<!---No event planned by the commission in the next months, but there should be a regular workshop next year in Firenze, prior to [ICC'2021][1].


More information coming soon...-->

[1]: https://www.aag.org/events/aag2024/
