---
  title: Benchmarks
  layout: page
  order: 11
---

Map Generalisation Benchmarks
===================

This page lists the existing benchmarks for map generalisation. For now, there are only datasets, but full benchmarks should be built soon upon these datasets.


Alpe d'Huez - a topographic map benchmark
---------------------------------------------------------------
A benchmark for 1:50k topographic map generalisation.

[Find the dataset here](https://zenodo.org/record/5749324)

[Related paper](https://www.abstr-int-cartogr-assoc.net/4/4/2022/)


BasqueRoads - a benchmark for road network selection
----------------------------------------

[Find the dataset here](https://zenodo.org/record/5744120)

[Related paper](https://www.abstr-int-cartogr-assoc.net/4/5/2022/)


AlpineBends - a benchmark for deep learning generalisation
----------------------------------------

[Find the dataset here](https://zenodo.org/record/5257686)

[Related paper](https://www.abstr-int-cartogr-assoc.net/4/1/2022/)



Benchmarks for slope/embankment generalisation
----------------------------------------

[Find the dataset here](https://zenodo.org/record/5745583)

[Related paper](https://www.abstr-int-cartogr-assoc.net/4/3/2022/)
