---
  title: Web Services
  layout: page
  order: 9
---

Generalisation web services
===========================

Projects & platforms
--------------------

The WebGen project (currently hosted at TU Dresden):

-   [WebGen service documentation](https://webgen.geo.tu-dresden.de) and download links for software & test data
-   [WebGen WPS implementation](https://webgen.geo.tu-dresden.de/webgen_wps/)
-   former [WebGen project documentation](https://webgen.geo.tu-dresden.de/webgen_core/index.html)

The 52n WPS framework ([client](http://52north.org/downloads/geoprocessing/clients/udig) and service [applications](http://52north.org/downloads/geoprocessing/wps)).

 

Workshop
--------

The first workshop on generalisation web services took place from november 19th to november 20th 2007, at the Ordnance Survey, UK.

[Workshop summary](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/workshop_summary_2007.pdf "Workshop summary")

![](images/workshop_webservice_2007.jpg) 
