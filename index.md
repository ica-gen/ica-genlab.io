---
  title: Welcome
  layout: page
  order: 1
---

Welcome to the website of the ICA Commission on multi-scale cartography!
===========================================================================================

It is the case that many tasks require us to visualize geographic information at a number of different scales, in a variety of environments, over different devices. Whether it is planning a holiday, or following a route on a smart phone, it is critical to give careful consideration as to how best to display that information, and to interact with the multiple scales. Therefore our research is concerned with automated methods that enable the creation and display of such geographic information at multiple levels of detail, across a range of technologies. The challenges of this research draws upon researchers and practitioners alike, working in the fields of database and ontological modeling, in computational geometry and spatial analytics, informatics and interface design, web services, agent based modeling, spatial cognition and of course cartography! In the context of exploratory data analysis and interaction, our work lies at the heart of geovisualisation.

For these reasons we feel that our research is truly multidisciplinary, and we warmly encourage you to join us, and to contribute to our endeavors.

As a continuation of the longtime commission on Generalisation and Multiple Representation, we seek to support research in the field of map generalization, multiple representation and multi-scale cartography through joint workshops and conference sessions, and publications including books devoted to this topic. We offer tutorials in support of research initiatives, and provide environments where researchers can share ideas, building on the knowledge of practitioners whilst seeking to address the needs of  the cartographic industry.

Our website offers a range of resources and contacts and includes papers from previous conferences and workshops. Membership of our Commission is free and open – if you work in this field, you can consider yourself a member! We are keen to hear about your endeavors. We are keen to know of any proposals or initiatives that you might have, and hope that you will find membership of this group a rewarding and pleasurable experience. Welcome to the Commission on multi-scale cartography!

[Terms of reference](terms-of-reference.md)

[![Watch the presentation video](https://img.youtube.com/vi/o5V_nQfoWgo/default.jpg)](https://youtu.be/o5V_nQfoWgo)
