---
  title: Members
  layout: page
  order: 5
---



Chairs of the commission
========================

Chair: Guillaume Touya, IGN France, Univ. Gustave Eiffel (firstname.lastname(at)ign.fr)

![Guillaume Touya](images/touya.png){: .right }

Guillaume Touya is a senior researcher at IGN France (the French mapping agency) and Univ. Gustave Eiffel, and has been an active member of the Generalisation commission since 2005. He holds a PhD degree and a Habilitation degree in Geographical Information Science from Paris Est University. His research interests include map generalization and automated multi-scale cartography, with a focus on the cartography of volunteered geographic information.

&nbsp;

&nbsp;

![Izabela Karsznia](images/IKarsznia_small.jpg){: .right }

Vice-chair: Izabela Karsznia, University of Warsaw, Poland (i.karsznia(at)uw.edu.pl)

&nbsp;

&nbsp;

&nbsp;    


<!--![Paulo Raposo](images/raposo.jpg){: .right }-->

Vice-chair: Nicolas Regnauld, ESRI (nregnauld(at)esri.com)

&nbsp;

&nbsp;

&nbsp;

<!--![Paulo Raposo](images/raposo.jpg){: .right }-->

Vice-chair: Timofey Samsonov, Lomonosov Moscow State University, Russia (tsamsonov(at)geogr.msu.ru)

&nbsp;

&nbsp;

&nbsp;

![Lawrence V. Stanislawski](images/Lvs_2011_picture.jpg){: .right }

Vice-chair: Lawrence V. Stanislawski, USGS, USA (lstan(at)usgs.gov)
 

Members of the commission
=========================

Members of the commission are those making it active. The current state of the following list is based on the participants of annual workshop of the last two years.

|------------------------|---------------------------------------|-------------|
| Name of the member     | Institution                           | Country     |
|------------------------|---------------------------------------|-------------|
| Meysam Aliakbarian     | Univ. of Zürich                       | Switzerland |
| Vincent van Altena     | Kadaster                              | Netherlands |
| Dogan Altundag         | Kadaster                              | Netherlands |
| Serdar Aslan           | General Command Mapping               | Turkey      |
| Pia Bereuter           | FHNW University                       | Switzerland |
| Jagadish Boodala       | Indian Institute of Technology Kanpur | India       |
| Cynthia Brewer         | Pennsylvania State University         | USA         |
| Marc-Olivier Briat     | ESRI                                  | USA         |
| Dirk Burghardt         | TU Dresden                            | Germany     |
| Barabara Buttenfield   | University of Colorado                | USA         |
| Tobias Dahinden        | University of Hannover                | Germany     |
| Cécile Duchêne         | IGN/LASTIG                            | France      |
| Pieter Erauw           | IGN-NGI                               | Belgium     |
| Hongchao Fan           | Technische Universität München        | Germany     |
| Anne Féchir            | IGN-NGI                               | Belgium     |
| Anna Fiedukowicz       | Warsaw University of Technology       | Poland      |
| Nick Gould             | University of Manchester              | UK          |
| Stefan Hahmann         | Dresden University of Technology      | Germany     |
| Lars Harrie            | University of Lund                    | Sweden      |
| Jan-Henrik Haunert     | University of Würzburg                | Germany     |
| Bin Jiang              | University of Gävle                   | Sweden      |
| Shen Jie               | Nanjing University                    | China       |
| Dominik Käuferle       | SwissTopo                             | Switzerland |
| Pengbo Li       | Lanzhou Jiaotong University                  | China |
| William Mackaness      | University of Edinburgh               | UK          |
| Bo Mao                 | Geoinformatics, KTH                   | Sweden      |
| Martijn Meijers        | TU Delft                              | Netherlands |
| Wouter Meulemans       | TU Eindhoven                          | Netherlands |
| Sébastien Mustière     | IGN/LASTIG                            | France      |
| Byron Nakos            | University of Athens                  | Greece      |
| Ron Nijhuis            | Kadaster                              | Netherlands |
| Robert Olszewski       | Warsaw University of Technology       | Poland      |
| Peter van Oosterom     | TU Delft                              | Netherlands |
| Woojin Park            | Seoul National University             | South Korea |
| Agata Pillich-Kolipińska | Warsaw University of Technology     | Poland      |
| Daniel Pilon           | Natural Resources Canada              | Canada      |
| Karsten Pippig         | TU Dresden                            | Germany     |
| Maria Pla              | Institut Cartogràfic de Catalunya     | Spain       |
| Marc Post              | TU Delft                              | Netherlands |
| Edith Punt             | ESRI                                  | USA         |
| Paolo Raposo           | Pennsylvania State University         | USA         |
| Nico Regnauld          | ESRI                                  | USA/France  |
| Andreas Reimer         | German Research Centre f. Geosciences  | Germany    |
| Timofey Samsonov       | Lomonosov State University            | Russia      |
| Sandro Savino          | University of Padova                  | Italy       |
| Monika Sester          | University of Hannover                | Germany     |
| Özlem Simav            | General Command Mapping               | Turkey      |
| Larry Stanislawski     | USGS-CEGIS                            | USA         |
| Christian Stern        | Karlsruhe University of Applied Sciences | Germany  |
| Jantien Stoter         | Kadaster & TU Delft                   | Netherlands |
| Emmanuel Stefanakis    | University of Calgary                 | Canada      |
| Stanisław Szombara     | AGH University of Science and Technology | Poland   |
| Guillaume Touya        | IGN/LASTIG                            | France      |
| Anna Vetter            | Esri Switzerland Ltd.                 | Switzerland |
| Mark Ware              | University of Glamorgan               | UK          |
| David Watkins          | ESRI                                  | USA         |
| Robert Weibel          | Univ. of Zurich                       | Switzerland |
| Luan Xuechen           | Wuhan University                      | China       |
| Min Yang               | Wuhan University                      | China       |
| Xiang Zhang            | Sun Yat-Sen University                      | China       |
|------------------------|---------------------------------------|-------------|





Member registration and mailing list
====================================

Send an e-mail to Guillaume Touya (firstname.lastname(at)ign.fr)
