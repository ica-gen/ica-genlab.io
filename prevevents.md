---
  title: Previous Events
  layout: page
  order: 3
---

Session at 2024 AAG Annual Meeting
---------------------------------------------------------------

Location: Honolulu, Hawaii (and online).

Date: April 16-20,  2024

[Program, papers and presentations](prevevents/AAGsession2024.md)


1st workshop on CartoAI: AI for cartography
---------------------------------------------------------------

Location: Leeds, United Kingdom (on site and online).

Date: September 12, 2023 (9:00 - 12:30), prior to GIScience 2023

[Program, papers and presentations](prevevents/cartoai2023.md)


25th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

Location: Delft, the Netherlands (on site and online).

Date: 13 June 2023, prior to AGILE 2023

[Program, papers and presentations](prevevents/workshop2023.md)


24th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

Location: Firenze, Italy (on site and online).

Date: 13 December 2021, prior to ICC 2021

[Program, papers and presentations](prevevents/workshop2021.md)


23rd ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

Location: Delft, The Netherlands (online).

Date: 5-6 November 2020, jointly with NCG Symposium

[Program, papers and presentations](prevevents/workshop2020.md)


Workshop on Abstractions, Scales, and Perception, 22nd ICA Workshop
---------------------------------------------------------------

Location: Tokyo, Japan.

Date: 15th July 2019, prior to ICC'19 conference

[Program, papers and presentations](prevevents/workshop2019.md)


21th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2018](images/images/Events/group2018_thumb.jpg)](images/images/Events/group2018.jpg){: .right}

Location: Lund, Sweden.

Date: 12th June 2018, prior to AGILE'18 conference

[Program, papers and presentations](prevevents/workshop2018.md)


20th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2017](images/images/Events/group2017_thumb.jpg)](images/images/Events/group2017.jpg){: .right}

Location: Washington DC, USA.

Date: 1.-2. July 2017

[General information](prevevents/workshop2017.md)
[Program, papers and presentations](prevevents/workshop2017program.md)



19th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2016 19th ICA WS web](images/images/Events/group2016_19th_ICA_WS_web.png)](images/images/Events/group2016_19th_ICA_WS.png){: .right}

Location:  Helsinki, Finland.

Date: 14th June 2016

[General information](prevevents/93-workshop2016general.md)
[Program, papers and presentations](prevevents/95-workshop2016program.md)



2nd NMA Symposium
-----------------

### - jointly organised with EuroSDR Commission on Data Specifications

[![group2015 2nNMA web](images/images/Events/group2015_2nNMA_web.png)](images/images/Events/group2015_2nNMA.png){: .right}

Location: Amsterdam, Netherlands.

Date: 3rd to 4th December, 2015

[General information](prevevents/88-nma-symposium-2015-general.md)
[Program and presentations](prevevents/92-nma-symposium-2015-presentations.md)



18th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2015](images/images/Events/group2015.jpg)](images/images/Events/group2015part.jpg){: .right}

Location:  State University of Rio de Janeiro (near MARACANA), Rio de Janeiro, Brazil.

Date: 21st  August 2015

[General information](prevevents/84-workshop2015general.md)
[Papers, program and presentations](prevevents/90-workshop2015program.md)
[Summary report](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/workshop2015/SummaryReport_ICA_News_Nov2015.pdf)




Thematic Workshop on building an Ontology of Generalisation for On-demand Mapping
--------------------------------------------------------------------------------

[![GenOntoGroup2015](images/images/Events/GenOntoGroup2015.jpg)](images/images/Events/GenOntoGroup2015part.jpg){: .right}

Location: Paris, France.

Date: 26th to 27th March 2015

[General information](prevevents/83-workshopthem2015general.md)
[GeneralisationOntologyParis2015](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/ThemWorkshop/ThematicOntologyOnDemand_Paris2015.pdf)



17th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2014part](images/images/Events/group2014part.jpg)](images/images/Events/group2014.jpg){: .right}

Location: Vienna, Austria.

Date: 23rd September, 2014

[General information](prevevents/81-workshop2014general.md)
[Papers, program and presentations](prevevents/82-workshop2014program.md)




16th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

### - jointly organised with the ICA Commission on Map production and Geo-Business

[![group2013part](images/images/Events/group2013part.jpg)](images/images/Events/group2013.JPG){: .right}

Location: Dresden, Germany.

Date: 23rd to 24th August, 2013

[General information](prevevents/43-workshop2013general.md)
[Papers, program and presentations](prevevents/76-workshop2013papers.md)
[Photos](prevevents/77-workshop2013photos.md)



NMA Symposium 2013
------------------

### - jointly organised with EuroSDR Commission on Data Specifications

[![Photo Institut Cartografic de Catalunya](images/images/Events/Photo_Institut_Cartografic_de_Catalunya.jpg)](http://www.icc.cat/eng){: .right}

Location: Barcelona, Spain.

Date: 21st to 22nd March, 2013

[General information](prevevents/48-nma-symposium-2013-general.md)
[Program and presentations](prevevents/73-nma-symposium-2013-presentations.md)




15th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

### - jointly organised with EuroSDR Commission on Data Specifications

[![group2012](images/images/Events/group2012.jpg)](images/images/Events/group2012part.jpg){: .right}

Location: Istanbul, Turkey.

Date: 13th to 14th September, 2012

[General information](prevevents/60-workshop-2012-general-info.md)
[Papers and program](prevevents/61-workshop-2012-papers-and-program.md)




14th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

### - jointly organised with ISPRS Commission II/2 Working group on Multiscale Representation of Spatial Data

[![group2011](images/images/Events/group2011.jpg)](images/images/Events/group2011part.jpg){: .right}

Location: Paris, France.

Date: 30th June to 1st July 2011

[General information](prevevents/72-workshop-2011-general-information.md)
[Papers and presentations](prevevents/70-workshop-2011-papers-and-presentations.md)
[Participants](prevevents/71-workshop-2011-participants.md)



Tutorial 2011
-------------

[![group2011tut](images/images/Events/group2011tut.jpg)](images/images/Events/tut2011_impressions.gif){: .right}

Location: Paris, France.

Date: 2nd July 2011

Teachers: W. Mackaness, N. Regnauld, L. Harrie, J. Stoter, D. Burghardt, C. Duchêne

[Program, slides and pictures](prevevents/68-tutorial-2011-program-slides-and-pictures.md)




13th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2010](images/images/Events/group2010.jpg)](images/images/Events/group2010part.jpg){: .right}

Location: Zürich, Switzerland.

Date: 12th to 13th September 2010

[General information](prevevents/65-workshop-2010-general-information.md)
[Papers and presentations](prevevents/67-workshop-2010-papers-and-presentations.md)
[Participants](prevevents/66-workshop-2010-participants.md)

Business meeting 2009
---------------------

Location: Santiago, Chile.

Date: 20th November 2009

[Report](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/report2009.pdf)

Seminar 2009
------------

[![group2009](images/images/Events/group2009.JPG)](images/images/Events/group2009part.jpg){: .right}

Location: Dagsthul, Germany.

Date: 14th to 17th April 2009

[Summary report
](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/summary2009.pdf "Summary")Webpage of the seminar in [Dagsthul Center](http://www.dagstuhl.de/de/programm/kalender/semhp/?semnr=09161)

12th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

### - jointly organised with EuroSDR Commission on Data Specifications and the Dutch program RGI

[![group2008](images/images/Events/group2008.jpg)](images/images/Events/group2008part.jpg){: .right}

Location: Montpellier, France.

Date: 20th to 21st June 2008

[General information](prevevents/62-workshop-2008-general-information.md)
[Papers and presentations](prevevents/64-workshop-2008-papers-and-presentations.md)
[Participants](prevevents/63-workshop-2008-participants.md)

11th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2007](images/images/Events/group2007.jpg)](images/images/Events/group2007part.jpg){: .right}

Location: Moscow, Russia.

Date: 2nd and 3rd August 2007

[Papers and program](prevevents/58-workshop-2007-papers-and-program.md)
[Photos](prevevents/69-workshop-2007-photos.md)

Tutorial 2007
-------------

Location: Moscow, Russia.

Date: 4th August 2007

Teachers : D. Burghardt, J. Gaffuri, W. Mackaness, M. Neun, L. Meng, A. Ruas, M. Sester, C. Vangenot

[Program](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/program_tutorial2007.pdf)
[Report](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/report97.pdf)

10th ICA Workshop on Generalisation and Multiple Representation
---------------------------------------------------------------

[![group2006](images/images/Events/group2006.jpg)](images/images/Events/group2006part.jpg){: .right}

Location: Portland, United States.

Date: 25th June 2006

[Papers and program](prevevents/57-workshop-2006-papers-and-program.md)
[Participants](prevevents/56-workshop-2006-participants.md)

9th ICA Workshop on Generalisation and Multiple Representation
--------------------------------------------------------------

[![group2005](images/images/Events/group2005.jpg)](images/images/Events/group2005part.jpg){: .right}

Location: La Coruña, Spain.

Date: 7th and 8th July 2005

[Papers and program](prevevents/75-workshop-2005-paper.md)
[Participants](prevevents/55-workshop-2005-participants.md) 

Tutorial 2005
-------------

Location: La Coruña, Spain.

Date: 10th July 2005

[Program](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/program_tutorial2005.pdf)

8th ICA Workshop on Generalisation and Multiple Representation
--------------------------------------------------------------

### *- jointly organised with EuroSDR Commission on Data Specifications*

[![group2004](images/images/Events/group2004.jpg)](images/images/Events/group2004part.jpg){: .right}

Location: Leicester, United Kingdom.

Date: 20th and 21st August 2004

[Papers and program](prevevents/54-workshop-2004-papers-and-program.md)
[Summary](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/summary2004.pdf)
[Participants](prevevents/53-workshop-2004-participants.md)


7th ICA Workshop on Progress in Automated Map Generalization
------------------------------------------------------------

[![group2003](images/images/Events/group2003.jpg)](images/images/Events/group2003part.jpg){: .right}

Location: Paris, France.

Date: 28th to 30th April 2003

[Program](prevevents/80-workshop2003program.md)
[Papers](prevevents/52-workshop-2003-papers.md)
[Participants](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/participants2003.pdf) 

Business meeting 2003
---------------------

Location: Durban, South Africa.

Date: 14th August 2003

[Report](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/Business2003.pdf)

6th ICA Workshop on Progress in Automated Map Generalization
------------------------------------------------------------

Location: Ottawa, Canada.

Date: 7th and 8th July 2002

[Program](prevevents/50-workshop-2002-program.md)
[Presentation](prevevents/51-workshop-2002-presentations.md)
[Website](http://www.ikg.uni-hannover.de/isprs/wg43-workshop.html)

5th ICA Workshop on Progress in Automated Map Generalization
------------------------------------------------------------

[![group2001](images/images/Events/group2001.jpg)](images/images/Events/group2001part.jpg){: .right}

Location: Beijing, China.

Date: 2nd to 4th August 2001

[Program](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/program2001.pdf)
[Papers](prevevents/49-workshop-2001-papers.md)
[Participants](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/participants2001.pdf)

4th ICA Workshop on Progress in Automated Map Generalization
------------------------------------------------------------

[![group2000](images/images/Events/group2000.jpg)](images/images/Events/group2000part.jpg){: .right}

Location: Barcelona, Spain.

Date: 21st to 23rd September 2000

[Papers](prevevents/41-workshop-2000-papers.md)
[Participants](prevevents/42-workshop-2000-participants.md)

3rd ICA Workshop on Progress and Developments in Automated Map Generalization
-----------------------------------------------------------------------------

[![group1999](images/images/Events/group1999.jpg)](images/images/Events/group1999part.jpg){: .right}

Location: Ottawa, Canada.

Date: 12th to 14th August 1999

[Program](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/program1999.pdf)
[Papers](prevevents/40-workshop-1999-papers.md)

2nd ICA Workshop on Map Generalization
--------------------------------------

[![group1997](images/images/Events/group1997.jpg)](images/images/Events/group1997part.jpg){: .right}

Location: Gävle, Sweden.

Date: 19th to 21st June 1997

[Summary](https://kartographie.geo.tu-dresden.de/downloads/ica-gen/report97.pdf)
[Papers and program](prevevents/11-workshop1997papers.md)
[Participants](prevevents/74-workshop-1997-participants.md)

1st ICA Workshop on Map Generalization
--------------------------------------

Location: Barcelona, Spain.

Date: 1995
