---
  title: Links
  layout: page
  order: 12
---

Links
=====

Reports
-------

[First National Workshop on Generalization and Multiple Representations held in Turkey (2009)](http://aci.ign.fr/Links/ING_IZNIK.PDF)  
[First workshop on generalisation web services (2007)](http://aci.ign.fr/web_service/workshop_summary_2007.pdf)

Organisations
-------------

[The International Cartographic Association (ICA)](http://www.icaci.org/)  
[The International Cartographic Conference 2009 (ICC) in Santiago (Chile)](http://www.icc2009.cl/)  
[EuroSDR](http://www.eurosdr.net/)  
[Commission 4 of the ISPRS](http://www.isprs.org/commission4/)  
[Inspire](http://www.inspire.net/)

Research teams
--------------

[University of Edinburgh (UK)](http://www.geos.ed.ac.uk/geography/research/EMMGIS/)  
[COGIT laboratory, IGN (France)](http://recherche.ign.fr/labos/cogit/english/accueilCOGIT.php)  
[Ordnance Survey (UK)](http://www.ordnancesurvey.co.uk/oswebsite/partnerships/research/research/general.html)  
[IKG, University of Hannover (Germany)](http://www.ikg.uni-hannover.de/forschung/index.en.html)  
[University of Zurich (Switzerland)](http://www.geo.unizh.ch/gis/research/cartgen/)  
[Finnish Geodetic Institute (Finland)](http://www.fgi.fi/osastot/osasto_eng.php?osasto=1)  
[TU Delft (Netherland)](http://www.tudelft.nl/live/pagina.jsp?id=3adbf62d-a63b-4974-80e4-09873ed5a9a6)  
[ITC (Netherland)](http://www.itc.nl/)  
[University of Colorado (USA)](http://www.colorado.edu/geography/)  
[Database laboratory, EPFL (Switzerland)](http://lbd.epfl.ch/e/)  
[University of Glamoran (UK)](http://gis.research.glam.ac.uk/)  
[University of Minnesota (USA)](http://www.geog.umn.edu/clusters/geovisual.html)  
[University of Laval (Québec)](http://www.crg.ulaval.ca/)  
[ICC (Spain)](http://www.icc.cat/web/content/en/common/icc/icc_serveis_pro.html)  
[University of Warsaw (Poland)](http://zk.gik.pw.edu.pl/default.html)  
[University of Lund (Sweden)](http://www.giscentrum.lu.se/english/projectresearch.htm)  
[Technical university of Munich (Germany)](http://www.lrz-muenchen.de/~t583101/WWW/index.html)  
[Sun Yat-sen University (China)](http://www.sysu.edu.cn/en/schools%20and%20departments/geographicalsciencesandplanning.htm)  
[Ecole Navale (France)](http://www.ecole-navale.fr/fr/irenav/groupes/sig/sig-en/index.html)  
[KMS (Danemark)](http://www.kms.dk/)  
SibGI [1](http://www.sibgi.ru/services/gen_big.htm) and [2](http://www.sibgi.ru/services/gen_litle.htm) (Russia)  
[MIIGAiK (Russia)](http://www.miigaik.ru/eng/activit.htm)

Projects
--------

[MapMuxing](http://mapmuxing.ign.fr/)  
[GiMoDig](http://gimodig.fgi.fi/)  
[AGENT (map generalisation by multi agent technology)](http://agent.ign.fr/)  
[WebGen](http://www.ixserve.de/index.php)  
[WebPark](http://www.webparkservices.info/generalization.html)  
[GEMURE](https://gold.geoide.ulaval.ca/internet/u_703_01.asp?id_projet=233)

Softwares
---------

[Radius Clarity](http://www.1spatial.com/products/radius_clarity/index.php?office=scot)  
[Change](http://www.ikg.uni-hannover.de/forschung/change/index.en.html)  
[DynaGEN](http://www.intergraph.com/dynagen/default.asp)  
[ArcGIS](http://www.esri.com/technology_trends/cartography/generalization.html)  
[Axpand](http://www.axes-systems.com/e/axes_3_5a.htm)

Web Services
------------

See [here](web-services.md).

An important link is missing? Thanks for sending it to the webmaster.
