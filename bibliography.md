---
  title: Bibliography
  layout: page
  order: 7
---

Map Generalisation Bibliography
===================

The website demonstrates the depth of the literature on map generalisaiton, multiple representation, and multi-scale cartography. In order to help newcomers, the commission proposes a shared library as a Zotero Group, freely usable [here](https://www.zotero.org/groups/5107803/ica_commission_multi-scale_cartograpy) if you have a Zotero account.

The library is a constant work-in-progress, and the administrators of the group will try to make this library as exhaustive as possible. If you are interested in helping us by becoming administrator of the group, please send an email to the commission chairs.
