---
  title: Webinars
  layout: page
  order: 4
---

Next Webinars
===================

Location: https://univ-eiffel.zoom.us/j/84487762239 (secret code: xwVhr8Eb).

Date: **September 26, 2024, 9:30 CET**

Speaker: Vincent van Altena, Dutch Kadaster

Duration: 1 hour including questions and discussions.

**SUMMARY**
In this webinar, Vincent will talk about the need for maps to be accessible and inclusive. He will discuss the function of maps in understanding the world and the necessity of a multidisciplinary approach to advancing understanding and inclusion.
Besides sharing theoretical and fundamental insights he will use case studies and user stories from the Dutch tactile mapping project to exemplify a practical approach to pursuing a more inclusive world by using cartography.

**BIO**
Vincent van Altena (senior researcher, Kadaster, the Netherlands. Co-chair ICA Working Group on Inclusive Cartography) holds a bachelor’s degree in theology, an MSc in Geographical Information Science, and a PhD in spatial-temporal interpretation of early Christian literature. At Kadaster, Vincent has worked on topographic mapping, automated generalization, and tailored customer solutions. He has participated in international projects like the European Location Framework and chaired Esri’s User Community for Geospatial Authorities Working Group on Map Automation and Generalization. Currently, he leads the Dutch initiative on tactile mapping.

![Vincent van Altena](images/headshotVincentvanAltena.png){: .right }


Past Webinars
===================

March 2024
-------

*Speaker: Azelle Courtial*

**An overview of deep learning for map generalisation**

[![Watch the webinar on Youtube](https://img.youtube.com/vi/GlUkhKY7a2c/default.jpg)](https://youtu.be/GlUkhKY7a2c)
